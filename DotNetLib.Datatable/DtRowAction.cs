﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtRowAction
    {
        public static IEnumerable<DtRowAction> Build(params DtRowAction[] actions)
        {
            return (actions ?? Enumerable.Empty<DtRowAction>()).Where(x => x != null);
        }

        public static DtRowAction Edit(string url)
        {
            return new DtRowAction
            {
                Type = "edit",
                Label = "Edit",
                Title = "Edit",
                Icon = "fa fa-fw fa-edit",
                Url = url
            };
        }

        public static DtRowAction Delete(string url)
        {
            return new DtRowAction
            {
                Type = "delete",
                Label = "Delete",
                Title = "Delete",
                Icon = "fa fa-fw fa-trash",
                Attributes = new Dictionary<string, object>
                {
                    { "class", "text-danger" },
                    { "data-dt-action", "delete" },
                    { "data-dt-action-url", url }
                }
            };
        }

        public string Type { get; set; }
        public string Label { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
        public IDictionary<string, object> Attributes { get; set; }
    }
}
