﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Datatable
{
    class DtOrderColumn
    {
        public DtOrderColumn(string name, bool ascending)
        {
            this.Name = name;
            this.Ascending = ascending;
        }

        public string Name { get; }
        public bool Ascending { get; }
    }
}
