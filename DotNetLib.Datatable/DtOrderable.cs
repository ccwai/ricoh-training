﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtOrderable<TModel> where TModel : class
    {
        private readonly Dictionary<string, Expression<Func<TModel, object>>> _Orders = new Dictionary<string, Expression<Func<TModel, object>>>();
        private static string GetColumnName(string name) => char.ToLower(name[0]) + name.Substring(1);

        internal DtOrderable(DtRequest request, IQueryable<TModel> query)
        {
            this.Request = request;
            this.Query = query;
        }

        public DtRequest Request { get; }
        public IQueryable<TModel> Query { get; }

        public DtOrderable<TModel> OrderDefaults(params string[] propertyNames)
        {
            var names = (propertyNames ?? Enumerable.Empty<string>()).Where(x => x != null).ToArray();
            var props = typeof(TModel).GetProperties()
                .Where(x => x.PropertyType.IsPublic)
                .Where(x => x.PropertyType.IsValueType || x.PropertyType == typeof(string))
                .Where(x => x.CanRead && x.CanWrite)
                .Where(x => DataTable.ExcludedDefaultOrderColumns?.Contains(x.Name) != true)
                .Where(x => !names.Any() || names.Contains(x.Name))
                .ToArray();

            foreach (var p in props)
            {
                var name = p.Name;

                // build the expression
                var parameter = Expression.Parameter(typeof(TModel), "x");
                var member = Expression.Property(parameter, name);
                var conversion = Expression.Convert(member, typeof(object));
                var expression = Expression.Lambda<Func<TModel, object>>(conversion, parameter);

                var col = GetColumnName(name);
                Order(col, expression);
            }

            return this;
        }

        public DtOrderable<TModel> OrderProperties(params Expression<Func<TModel, object>>[] propertySelectors)
        {
            if (propertySelectors?.Any(x => x != null) == true)
            {
                var names = propertySelectors.Where(x => x != null)
                    .Select(x => (x.Body as MemberExpression) ?? ((x.Body as UnaryExpression)?.Operand as MemberExpression))
                    .Select(x => x?.Member?.Name)
                    .Where(x => x != null)
                    .ToArray();
                return OrderDefaults(names);
            }
            return OrderDefaults();
        }

        public DtOrderable<TModel> Order(string name, Expression<Func<TModel, object>> keySelector)
        {
            _Orders[name] = keySelector;
            return this;
        }

        public DtResult<TModel> GetResult(int recordsTotal)
        {
            int recordsFiltered = Query.Count();
            return GetResult(recordsTotal, recordsFiltered);
        }

        public DtResult<TModel> GetResult(int recordsTotal, int recordsFiltered)
        {
            IOrderedQueryable<TModel> query;
            if (Query.Expression.Type == typeof(IOrderedQueryable<TModel>))
                query = (IOrderedQueryable<TModel>)Query;
            else
                query = Query.OrderBy(x => 0);

            var columns = Request.GetOrderColumns().Where(x => _Orders.ContainsKey(x.Name)).ToList();
            foreach (var column in columns)
            {
                var order = _Orders[column.Name];
                query = column.Ascending ? query.ThenBy(order) : query.ThenByDescending(order);
            }

            List<TModel> data;
            if (Request.Start >= 0 && Request.Length >= 0)
            {
                var skip = Request.Start.Value;
                var take = Request.Length.Value;
                data = query.Skip(skip).Take(take).ToList();
            }
            else
                data = query.ToList();

            return new DtResult<TModel>
            {
                Draw = Request.Draw,
                RecordsTotal = recordsTotal,
                RecordsFiltered = recordsFiltered,
                Data = data
            };
        }
    }
}
