﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtResult<TModel> where TModel : class
    {
        internal DtResult()
        {
        }

        public int Draw { get; internal set; }
        public int RecordsTotal { get; internal set; }
        public int RecordsFiltered { get; internal set; }
        public IEnumerable<TModel> Data { get; internal set; }

        public DtResult<TResult> As<TResult>(Func<TModel, TResult> selector) where TResult : class
        {
            return new DtResult<TResult>
            {
                Draw = Draw,
                RecordsTotal = RecordsTotal,
                RecordsFiltered = RecordsFiltered,
                Data = Data.Select(selector)
            };
        }
    }
}
