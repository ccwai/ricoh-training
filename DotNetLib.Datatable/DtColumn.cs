﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtColumn
    {
        public string Data { get; set; }
        public string Name { get; set; }
        public bool Searchable { get; set; }
        public bool Orderable { get; set; }
        public DtSearch Search { get; set; }
    }
}
