﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Datatable
{
    public static class DataTable
    {
        public static string[] ExcludedDefaultOrderColumns = new string[]
        {
            "CreatedDate", "CreatedBy", "UpdatedDate", "UpdatedBy"
        };
    }
}
