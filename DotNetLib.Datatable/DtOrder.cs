﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtOrder
    {
        public int Column { get; set; }
        public string Dir { get; set; }
        public bool Ascending => Dir.EqualsIgnoreCase("asc");
    }
}
