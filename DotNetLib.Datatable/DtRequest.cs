﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtRequest
    {
        public int Draw { get; set; }

        public int? Start { get; set; }

        public int? Length { get; set; }

        public List<DtColumn> Columns { get; set; }

        public List<DtOrder> Order { get; set; }

        public DtSearch Search { get; set; }

        public string SearchValue => Search?.Value;

        public DtOrderable<TModel> GetOrderable<TModel>(IQueryable<TModel> query) where TModel : class
        {
            return new DtOrderable<TModel>(this, query);
        }

        public DtResult<TModel> GetResult<TModel>(IQueryable<TModel> query, int recordsTotal) where TModel : class
        {
            return GetOrderable(query).GetResult(recordsTotal);
        }

        public DtResult<TModel> GetResult<TModel>(IQueryable<TModel> query, int recordsTotal, int recordsFiltered) where TModel : class
        {
            return GetOrderable(query).GetResult(recordsTotal, recordsFiltered);
        }

        internal List<DtOrderColumn> GetOrderColumns()
        {
            List<DtOrderColumn> list = new List<DtOrderColumn>();
            var columns = Columns ?? new List<DtColumn>();
            var orders = Order ?? new List<DtOrder>();
            foreach (var order in orders)
            {
                var index = order.Column;
                if (index >= 0 && index < columns.Count)
                {
                    var column = columns[index];
                    var name = column.Name;
                    if (name == null && column.Data != null && !column.Data.EqualsIgnoreCase("function"))
                        name = column.Data;

                    if (name != null)
                        list.Add(new DtOrderColumn(name, order.Ascending));
                }
            }
            return list;
        }
    }

    public class DtRequest<T> : DtRequest where T : class
    {
        public T Params { get; set; }
    }
}
