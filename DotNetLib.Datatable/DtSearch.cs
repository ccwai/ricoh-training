﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Datatable
{
    public class DtSearch
    {
        public string Value { get; set; }
        public bool Regex { get; set; }
    }
}
