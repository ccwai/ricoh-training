﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class Media
    {
        public Media()
        {
            FaqDocument = new HashSet<FaqDocument>();
            TrainingAgendaMedia = new HashSet<TrainingAgendaMedia>();
        }

        public int Id { get; set; }
        public string FileName { get; set; }
        public string Path { get; set; }
        public string ContentType { get; set; }
        public int ContentLength { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public virtual ICollection<FaqDocument> FaqDocument { get; set; }
        public virtual ICollection<TrainingAgendaMedia> TrainingAgendaMedia { get; set; }
    }
}
