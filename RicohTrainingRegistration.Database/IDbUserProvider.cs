﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public interface IDbUserProvider
    {
        string GetId();
        string GetName();
    }
}
