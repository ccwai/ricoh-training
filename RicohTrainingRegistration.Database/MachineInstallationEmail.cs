﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class MachineInstallationEmail
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string SerialNo { get; set; }
        public DateTime InstallDate { get; set; }
        public string EmailTemplateId { get; set; }
        public DateTime TargetDate { get; set; }
        public int? EmailRequestId { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Language { get; set; }

        public virtual MachineInstallationDate Order { get; set; }
    }
}
