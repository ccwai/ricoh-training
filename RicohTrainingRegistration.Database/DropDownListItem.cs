﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public class DropDownListItem
    {
        public string Slug { get; set; }
        public string NameEng { get; set; }
        public string NameChi { get; set; }
        public string NoteEng { get; set; }
        public string NoteChi { get; set; }
    }
}
