﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public class SystemSettings
    {
        public const string SerialNoHintImagePath = "content/sn-hint.file";

        public IEnumerable<string> CompletedInstallationProblemCodes { get; set; }
        public IEnumerable<string> CompletedTrainingProblemCodes { get; set; }
        public int? OpenedDayForRegistration { get; set; }
        public int? FreeTrainingPeriod { get; set; }
        public int? RegistrationDeadlineDay { get; set; }
        public int? RegistrationDeadlineHour { get; set; }
        public int? RegistrationDeadlineMinute { get; set; }
        public IEnumerable<DropDownListItem> Attendees { get; set; }
        public IEnumerable<DropDownListItem> OperatingSystems { get; set; }
        public IEnumerable<FooterLink> FooterLinks { get; set; }
        public bool? RescheduleHasCutoff { get; set; }
        public string SerialNoHintEng { get; set; }
        public string SerialNoHintChi { get; set; }
    }
}
