﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class EmailTemplate
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string SubjectEng { get; set; }
        public string SubjectChi { get; set; }
        public string ContentEng { get; set; }
        public string ContentChi { get; set; }
        public bool SendToTrainers { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
