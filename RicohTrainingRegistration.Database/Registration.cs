﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class Registration : RegistrationBase
    {
        public Registration()
        {
            ScheduleItemSlot = new HashSet<ScheduleItemSlot>();
        }

        public virtual ICollection<ScheduleItemSlot> ScheduleItemSlot { get; set; }
    }
}
