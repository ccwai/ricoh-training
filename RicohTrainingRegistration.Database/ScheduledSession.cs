﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduledSession
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int MasterId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public DateTime Date { get; set; }
        public string District { get; set; }
        public string Timeslot { get; set; }
        public int Seq { get; set; }
        public int? RegistrationId { get; set; }
        public string SerialNo { get; set; }
        public bool IsCustom { get; set; }
        public bool IsUseCustom { get; set; }
        public string Remark { get; set; }
    }
}
