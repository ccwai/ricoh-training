﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RicohTrainingRegistration.Database
{
    public partial class RicohDB : DbContext
    {
        public RicohDB()
        {
        }

        public RicohDB(DbContextOptions<RicohDB> options)
            : base(options)
        {
        }

        public virtual DbSet<AuditEntry> AuditEntry { get; set; }
        public virtual DbSet<AuditEntryProperty> AuditEntryProperty { get; set; }
        public virtual DbSet<EmailRequest> EmailRequest { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplate { get; set; }
        public virtual DbSet<EnquiryForm> EnquiryForm { get; set; }
        public virtual DbSet<FaqDocument> FaqDocument { get; set; }
        public virtual DbSet<Holiday> Holiday { get; set; }
        public virtual DbSet<JobLog> JobLog { get; set; }
        public virtual DbSet<MachineInstallation> MachineInstallation { get; set; }
        public virtual DbSet<MachineInstallationDate> MachineInstallationDate { get; set; }
        public virtual DbSet<MachineInstallationEmail> MachineInstallationEmail { get; set; }
        public virtual DbSet<Media> Media { get; set; }
        public virtual DbSet<RegistrationMaster> RegistrationMaster { get; set; }
        public virtual DbSet<RegistrationTimeslot> RegistrationTimeslot { get; set; }
        public virtual DbSet<ScheduleDefault> ScheduleDefault { get; set; }
        public virtual DbSet<ScheduleItem> ScheduleItem { get; set; }
        public virtual DbSet<ScheduleItemSlot> ScheduleItemSlot { get; set; }
        public virtual DbSet<ScheduleMaster> ScheduleMaster { get; set; }
        public virtual DbSet<ScheduledSession> ScheduledSession { get; set; }
        public virtual DbSet<SystemEmail> SystemEmail { get; set; }
        public virtual DbSet<SystemSetting> SystemSetting { get; set; }
        public virtual DbSet<TrainingAgenda> TrainingAgenda { get; set; }
        public virtual DbSet<TrainingAgendaMedia> TrainingAgendaMedia { get; set; }
        public virtual DbSet<TrainingModel> TrainingModel { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=www.wise-studio.com,51433;Database=RicohDB;user id=ricoh;password=ricoh");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuditEntry>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EntitySetName).HasMaxLength(255);

                entity.Property(e => e.EntityTypeName).HasMaxLength(255);

                entity.Property(e => e.StateName).HasMaxLength(255);
            });

            modelBuilder.Entity<AuditEntryProperty>(entity =>
            {
                entity.Property(e => e.PropertyName).HasMaxLength(255);

                entity.Property(e => e.RelationName).HasMaxLength(255);

                entity.HasOne(d => d.AuditEntry)
                    .WithMany(p => p.AuditEntryProperty)
                    .HasForeignKey(d => d.AuditEntryId)
                    .HasConstraintName("FK_dbo.AuditEntryProperties_dbo.AuditEntries_AuditEntryId");
            });

            modelBuilder.Entity<EmailRequest>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NextAttempt).HasColumnType("datetime");

                entity.Property(e => e.Receiver)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.RequestTime).HasColumnType("datetime");

                entity.Property(e => e.SentTime).HasColumnType("datetime");

                entity.Property(e => e.TemplateId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<EmailTemplate>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(50);

                entity.Property(e => e.ContentChi).HasColumnType("ntext");

                entity.Property(e => e.ContentEng).HasColumnType("ntext");

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.SubjectChi).HasMaxLength(100);

                entity.Property(e => e.SubjectEng).HasMaxLength(100);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<EnquiryForm>(entity =>
            {
                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.RequestIp)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.RequestTime).HasColumnType("datetime");

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.UpdatedBy).HasMaxLength(200);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UserAgent).HasMaxLength(500);
            });

            modelBuilder.Entity<FaqDocument>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Language)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UploadDate).HasColumnType("datetime");

                entity.HasOne(d => d.Media)
                    .WithMany(p => p.FaqDocument)
                    .HasForeignKey(d => d.MediaId)
                    .HasConstraintName("FK_FaqDocument_Media");
            });

            modelBuilder.Entity<Holiday>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.Type).HasMaxLength(20);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<JobLog>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndTime).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(500);

                entity.Property(e => e.StartTime).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<MachineInstallation>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CustNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Email).HasMaxLength(254);

                entity.Property(e => e.ErpId).HasMaxLength(40);

                entity.Property(e => e.FirstName).HasMaxLength(40);

                entity.Property(e => e.ImportTime).HasColumnType("datetime");

                entity.Property(e => e.InstallAddress)
                    .IsRequired()
                    .HasMaxLength(254);

                entity.Property(e => e.InstallDistrict).HasMaxLength(30);

                entity.Property(e => e.ItemNo)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastChangeTime).HasColumnType("datetime");

                entity.Property(e => e.LastName).HasMaxLength(40);

                entity.Property(e => e.ModelDesc)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.ModelId)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.OpenTime).HasColumnType("datetime");

                entity.Property(e => e.PhoneNo).HasMaxLength(40);

                entity.Property(e => e.ProblemCode)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.RequestId)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.RetrievedDate).HasColumnType("datetime");

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MachineInstallationDate>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FreeTrainingEndDate).HasColumnType("date");

                entity.Property(e => e.InstallDate).HasColumnType("date");

                entity.Property(e => e.ProblemCode)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MachineInstallationEmail>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EmailTemplateId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.InstallDate).HasColumnType("date");

                entity.Property(e => e.Language)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.TargetDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.MachineInstallationEmail)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_MachineInstallationEmail_MachineInstallationDate");
            });

            modelBuilder.Entity<Media>(entity =>
            {
                entity.Property(e => e.ContentType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<RegistrationMaster>(entity =>
            {
                entity.Property(e => e.Attendees)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Discriminator)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OperatingSystem)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Session).HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SubmissionDate).HasColumnType("datetime");

                entity.Property(e => e.TrainingDate).HasColumnType("date");

                entity.Property(e => e.TrainingDateRemarks).HasMaxLength(200);

                entity.Property(e => e.TrainingTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VenueAddress)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<RegistrationTimeslot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("RegistrationTimeslot");

                entity.Property(e => e.Attendees)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ItemNo).HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModelId).HasMaxLength(30);

                entity.Property(e => e.ModelName).HasMaxLength(50);

                entity.Property(e => e.OperatingSystem)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.ScheduleDate).HasColumnType("date");

                entity.Property(e => e.ScheduleDistrict)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduleTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Session).HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SubmissionDate).HasColumnType("datetime");

                entity.Property(e => e.TrainingDate).HasColumnType("date");

                entity.Property(e => e.TrainingDateRemarks).HasMaxLength(200);

                entity.Property(e => e.TrainingTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.VenueAddress)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ScheduleDefault>(entity =>
            {
                entity.HasKey(e => new { e.DayOfWeek, e.Timeslot });

                entity.Property(e => e.Timeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Hk).HasColumnName("HK");

                entity.Property(e => e.Kln).HasColumnName("KLN");

                entity.Property(e => e.Nt).HasColumnName("NT");

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ScheduleItem>(entity =>
            {
                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.ScheduleMaster)
                    .WithMany(p => p.ScheduleItem)
                    .HasForeignKey(d => d.ScheduleMasterId)
                    .HasConstraintName("FK_ScheduleItem_ScheduleMaster");
            });

            modelBuilder.Entity<ScheduleItemSlot>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Timeslot)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.HasOne(d => d.RegistrationMaster)
                    .WithMany(p => p.ScheduleItemSlot)
                    .HasForeignKey(d => d.RegistrationMasterId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_ScheduleItemSlot_RegistrationMaster");

                entity.HasOne(d => d.ScheduleItem)
                    .WithMany(p => p.ScheduleItemSlot)
                    .HasForeignKey(d => d.ScheduleItemId)
                    .HasConstraintName("FK_ScheduleItemSlot_ScheduleItem");
            });

            modelBuilder.Entity<ScheduleMaster>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<ScheduledSession>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("ScheduledSession");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.SerialNo).HasMaxLength(30);

                entity.Property(e => e.Timeslot)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SystemEmail>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DisplayName).HasMaxLength(50);

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SystemSetting>(entity =>
            {
                entity.HasKey(e => e.Name);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Value).HasMaxLength(2000);
            });

            modelBuilder.Entity<TrainingAgenda>(entity =>
            {
                entity.Property(e => e.ContentChi).HasMaxLength(1000);

                entity.Property(e => e.ContentEng).HasMaxLength(1000);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.UrlChi).HasMaxLength(500);

                entity.Property(e => e.UrlEng).HasMaxLength(500);

                entity.Property(e => e.UrlLabelChi).HasMaxLength(500);

                entity.Property(e => e.UrlLabelEng).HasMaxLength(500);
            });

            modelBuilder.Entity<TrainingAgendaMedia>(entity =>
            {
                entity.HasKey(e => new { e.TrainingAgendaId, e.MediaId });

                entity.Property(e => e.Language)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.Media)
                    .WithMany(p => p.TrainingAgendaMedia)
                    .HasForeignKey(d => d.MediaId)
                    .HasConstraintName("FK_TrainingAgendaMedia_Media");

                entity.HasOne(d => d.TrainingAgenda)
                    .WithMany(p => p.TrainingAgendaMedia)
                    .HasForeignKey(d => d.TrainingAgendaId)
                    .HasConstraintName("FK_TrainingAgendaMedia_TrainingAgenda");
            });

            modelBuilder.Entity<TrainingModel>(entity =>
            {
                entity.HasKey(e => e.ModelId);

                entity.Property(e => e.ModelId).HasMaxLength(30);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ImportTime).HasColumnType("datetime");

                entity.Property(e => e.ModelName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RetrievedDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.TrainingAgenda)
                    .WithMany(p => p.TrainingModel)
                    .HasForeignKey(d => d.TrainingAgendaId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_TrainingModel_TrainingAgenda");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
