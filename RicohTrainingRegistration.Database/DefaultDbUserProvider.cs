﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RicohTrainingRegistration.Database
{
    public class DefaultDbUserProvider : IDbUserProvider
    {
        public string GetId()
        {
            return Thread.CurrentPrincipal?.Identity?.Name;
        }

        public string GetName()
        {
            return Thread.CurrentPrincipal?.Identity?.Name;
        }
    }
}
