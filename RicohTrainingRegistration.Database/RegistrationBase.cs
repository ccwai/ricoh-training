﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public abstract class RegistrationBase
    {
        public int Id { get; set; }
        public string SerialNo { get; set; }
        public string VenueAddress { get; set; }
        public string District { get; set; }
        public DateTime? TrainingDate { get; set; }
        public string TrainingTimeslot { get; set; }
        public string Attendees { get; set; }
        public string OperatingSystem { get; set; }
        public bool? TrainingDatePreferable { get; set; }
        public string TrainingDateRemarks { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string OrderId { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string Session { get; set; }
    }
}
