﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RicohTrainingRegistration.Database
{
    public partial class RicohDB
    {
        public virtual DbSet<Registration> Registration { get; set; }
        public virtual DbSet<Reschedule> Reschedule { get; set; }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EmailRequest>(entity =>
            {
                entity.HasOne(d => d.EmailTemplate)
                    .WithMany(p => p.EmailRequests)
                    .HasForeignKey(d => d.TemplateId)
                    .HasPrincipalKey(p => p.Id);
            });

            modelBuilder.Entity<EnquiryForm>(entity =>
            {
                entity.HasOne(d => d.MachineInstallation)
                    .WithMany(p => p.EnquiryForms)
                    .HasForeignKey(d => d.OrderId)
                    .HasPrincipalKey(p => p.OrderId);
            });

            modelBuilder.Entity<MachineInstallation>(entity =>
            {
                entity.HasOne(d => d.TrainingModel)
                    .WithMany(p => p.MachineInstallation)
                    .HasForeignKey(d => d.ModelId)
                    .HasPrincipalKey(p => p.ModelId);
            });

            modelBuilder.Entity<MachineInstallationDate>(entity =>
            {
                entity.HasOne(d => d.EmailRequest)
                    .WithMany(p => p.MachineInstallationDates)
                    .HasForeignKey(d => d.EmailRequestId)
                    .HasPrincipalKey(p => p.Id);

                entity.HasOne(d => d.MachineInstallation)
                    .WithOne(p => p.MachineInstallationDate)
                    .HasForeignKey<MachineInstallationDate>(d => d.OrderId);
            });

            modelBuilder.Entity<MachineInstallationEmail>(entity =>
            {
                entity.HasOne(d => d.EmailRequest)
                    .WithMany(p => p.MachineInstallationEmails)
                    .HasForeignKey(d => d.EmailRequestId)
                    .HasPrincipalKey(p => p.Id);
            });

            modelBuilder.Ignore<RegistrationMaster>();

            modelBuilder.Entity<RegistrationBase>(entity =>
            {
                entity.ToTable("RegistrationMaster")
                       .HasDiscriminator<string>("Discriminator")
                       .HasValue<Registration>(nameof(RicohTrainingRegistration.Database.Registration))
                       .HasValue<Reschedule>(nameof(RicohTrainingRegistration.Database.Reschedule));

                entity.Property(e => e.Attendees)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OperatingSystem)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Session).HasMaxLength(50);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SubmissionDate).HasColumnType("datetime");

                entity.Property(e => e.TrainingDate).HasColumnType("date");

                entity.Property(e => e.TrainingDateRemarks).HasMaxLength(200);

                entity.Property(e => e.TrainingTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy).HasMaxLength(100);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.VenueAddress)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Ignore<RegistrationTimeslot>();

            modelBuilder.Entity<RegistrationTimeslot>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToView("RegistrationTimeslot");

                entity.Property(e => e.Attendees)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(60);

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModelId).HasMaxLength(30);

                entity.Property(e => e.ModelName).HasMaxLength(50);

                entity.Property(e => e.OperatingSystem)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.OrderId).HasMaxLength(40);

                entity.Property(e => e.PhoneNo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Remarks).HasMaxLength(200);

                entity.Property(e => e.ScheduleDate).HasColumnType("date");

                entity.Property(e => e.ScheduleDistrict)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ScheduleTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.SubmissionDate).HasColumnType("datetime");

                entity.Property(e => e.TrainingDate).HasColumnType("date");

                entity.Property(e => e.TrainingDateRemarks).HasMaxLength(200);

                entity.Property(e => e.TrainingTimeslot)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.VenueAddress)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<ScheduleItemSlot>(entity =>
            {
                entity.Ignore(e => e.RegistrationMasterId);

                entity.Ignore(e => e.RegistrationMaster);

                entity.Property(e => e.RegistrationId)
                    .HasColumnName("RegistrationMasterId");

                entity.HasOne(d => d.Registration)
                    .WithMany(p => p.ScheduleItemSlot)
                    .HasForeignKey(d => d.RegistrationId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_ScheduleItemSlot_RegistrationMaster");
            });

            modelBuilder.Ignore<ScheduledSession>();

            modelBuilder.Entity<ScheduledSession>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToView("ScheduledSession");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.SerialNo).HasMaxLength(30);

                entity.Property(e => e.Timeslot)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.HasOne(d => d.ScheduleMaster)
                    .WithMany(p => p.ScheduledSessions)
                    .HasForeignKey(d => d.MasterId);
            });
        }
    }
}
