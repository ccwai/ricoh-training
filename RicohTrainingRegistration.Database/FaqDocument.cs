﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class FaqDocument
    {
        public int Id { get; set; }
        public DateTime UploadDate { get; set; }
        public int MediaId { get; set; }
        public string Language { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public virtual Media Media { get; set; }
    }
}
