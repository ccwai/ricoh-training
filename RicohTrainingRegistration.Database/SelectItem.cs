﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public class SelectItem
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string Note { get; set; }
    }
}
