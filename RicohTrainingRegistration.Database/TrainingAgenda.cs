﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class TrainingAgenda
    {
        public TrainingAgenda()
        {
            TrainingAgendaMedia = new HashSet<TrainingAgendaMedia>();
            TrainingModel = new HashSet<TrainingModel>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string ContentEng { get; set; }
        public string ContentChi { get; set; }
        public string UrlLabelEng { get; set; }
        public string UrlEng { get; set; }
        public string UrlLabelChi { get; set; }
        public string UrlChi { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<TrainingAgendaMedia> TrainingAgendaMedia { get; set; }
        public virtual ICollection<TrainingModel> TrainingModel { get; set; }
    }
}
