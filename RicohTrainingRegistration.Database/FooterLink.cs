﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public class FooterLink
    {
        public string LabelEng { get; set; }
        public string LabelChi { get; set; }
        public string UrlEng { get; set; }
        public string UrlChi { get; set; }
        public bool? Display { get; set; }
    }
}
