﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class EnquiryForm
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string SerialNo { get; set; }
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
        public DateTime RequestTime { get; set; }
        public string RequestIp { get; set; }
        public string UserAgent { get; set; }
        public bool IsRead { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
