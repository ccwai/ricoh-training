﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleDefault
    {
        public int DayOfWeek { get; set; }
        public string Timeslot { get; set; }
        public int Hk { get; set; }
        public int Kln { get; set; }
        public int Nt { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
