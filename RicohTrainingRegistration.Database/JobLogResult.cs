﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    class JobLogResult
    {
        [JsonProperty("entries")]
        public List<JobLogResultEntry> Entries { get; set; }
    }

    class JobLogResultEntry
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("changes")]
        public Dictionary<string, OldNewValue> Changes { get; set; }
    }

    class OldNewValue
    {
        public OldNewValue(object oldValue, object newValue)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }

        [JsonProperty("oldValue")]
        public object OldValue { get; set; }
        
        [JsonProperty("newValue")]
        public object NewValue { get; set; }
    }
}
