﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class SystemSetting
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
