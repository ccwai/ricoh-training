﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class SystemEmail
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string EmailAddress { get; set; }
        public string DisplayName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
