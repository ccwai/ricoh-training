﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleItem
    {
        public ScheduleItem()
        {
            ScheduleItemSlot = new HashSet<ScheduleItemSlot>();
        }

        public int Id { get; set; }
        public int ScheduleMasterId { get; set; }
        public int Day { get; set; }
        public DateTime Date { get; set; }
        public string District { get; set; }
        public bool IsUseCustom { get; set; }
        public string Remark { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ScheduleMaster ScheduleMaster { get; set; }
        public virtual ICollection<ScheduleItemSlot> ScheduleItemSlot { get; set; }
    }
}
