﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleItemSlot
    {
        public int Id { get; set; }
        public int ScheduleItemId { get; set; }
        public string Timeslot { get; set; }
        public int? RegistrationMasterId { get; set; }
        public int Seq { get; set; }
        public bool IsCustom { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public virtual RegistrationMaster RegistrationMaster { get; set; }
        public virtual ScheduleItem ScheduleItem { get; set; }
    }
}
