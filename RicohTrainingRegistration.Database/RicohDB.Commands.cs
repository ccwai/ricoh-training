﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class RicohDB
    {
        public SystemSettings GetDefaultSettings()
        {
            var list = this.SystemSetting.ToList();
            var model = new SystemSettings();

            // problem codes
            model.CompletedInstallationProblemCodes = list.Find(x => x.Name == "CompletedInstallationProblemCodes")?.Value?
                .Split(',')
                .Select(x => x?.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x))
                ?? Enumerable.Empty<string>();
            model.CompletedTrainingProblemCodes = list.Find(x => x.Name == "CompletedTrainingProblemCodes")?.Value?
                .Split(',')
                .Select(x => x?.Trim())
                .Where(x => !string.IsNullOrWhiteSpace(x))
                ?? Enumerable.Empty<string>();

            // deadlines
            int tmp;
            model.OpenedDayForRegistration = int.TryParse(list.Find(x => x.Name == "OpenedDayForRegistration")?.Value, out tmp) ? tmp : 30;
            model.FreeTrainingPeriod = int.TryParse(list.Find(x => x.Name == "FreeTrainingPeriod")?.Value, out tmp) ? tmp : 90;
            model.RegistrationDeadlineDay = int.TryParse(list.Find(x => x.Name == "RegistrationDeadlineDay")?.Value, out tmp) ? tmp : 1;
            model.RegistrationDeadlineHour = int.TryParse(list.Find(x => x.Name == "RegistrationDeadlineHour")?.Value, out tmp) ? tmp : 9;
            model.RegistrationDeadlineMinute = int.TryParse(list.Find(x => x.Name == "RegistrationDeadlineMinute")?.Value, out tmp) ? tmp : 0;
            model.RescheduleHasCutoff = bool.TryParse(list.Find(x => x.Name == "RescheduleHasCutoff")?.Value, out bool b) && b;

            // dropdown lists
            try
            {
                var json = (list.Find(x => x.Name == "Attendees")?.Value) ?? "[]";
                model.Attendees = JsonConvert.DeserializeObject<IEnumerable<DropDownListItem>>(json);
            }
            catch (Exception)
            {
                model.Attendees = Enumerable.Empty<DropDownListItem>();
            }
            try
            {
                var json = (list.Find(x => x.Name == "OperatingSystems")?.Value) ?? "[]";
                model.OperatingSystems = JsonConvert.DeserializeObject<IEnumerable<DropDownListItem>>(json);
            }
            catch (Exception)
            {
                model.OperatingSystems = Enumerable.Empty<DropDownListItem>();
            }

            // messages
            model.SerialNoHintEng = list.Find(x => x.Name == "SerialNoHintEng")?.Value;
            model.SerialNoHintChi = list.Find(x => x.Name == "SerialNoHintChi")?.Value;

            // footer links
            try
            {
                var json = (list.Find(x => x.Name == "FooterLinks")?.Value) ?? "[]";
                model.FooterLinks = JsonConvert.DeserializeObject<IEnumerable<FooterLink>>(json);
            }
            catch (Exception)
            {
                model.FooterLinks = Enumerable.Empty<FooterLink>();
            }

            return model;
        }

        public IEnumerable<SystemSetting> UpdateSystemSettings(SystemSettings settings)
        {
            var list = new List<SystemSetting>();
            list.Add(new SystemSetting { Name = "CompletedInstallationProblemCodes", Value = string.Join(",", settings?.CompletedInstallationProblemCodes?.Where(x => !string.IsNullOrWhiteSpace(x)) ?? Enumerable.Empty<string>()) });
            list.Add(new SystemSetting { Name = "CompletedTrainingProblemCodes", Value = string.Join(",", settings?.CompletedTrainingProblemCodes?.Where(x => !string.IsNullOrWhiteSpace(x)) ?? Enumerable.Empty<string>()) });
            list.Add(new SystemSetting { Name = "RescheduleHasCutoff", Value = settings.RescheduleHasCutoff == true ? bool.TrueString : bool.FalseString });
            list.Add(new SystemSetting { Name = "OpenedDayForRegistration", Value = settings.OpenedDayForRegistration?.ToString() });
            list.Add(new SystemSetting { Name = "RegistrationDeadlineDay", Value = settings.RegistrationDeadlineDay?.ToString() });
            list.Add(new SystemSetting { Name = "RegistrationDeadlineHour", Value = settings.RegistrationDeadlineHour?.ToString() });
            list.Add(new SystemSetting { Name = "RegistrationDeadlineMinute", Value = settings.RegistrationDeadlineMinute?.ToString() });
            list.Add(new SystemSetting { Name = "FreeTrainingPeriod", Value = settings.FreeTrainingPeriod?.ToString() });
            list.Add(new SystemSetting { Name = "Attendees", Value = JsonConvert.SerializeObject(settings.Attendees ?? Enumerable.Empty<DropDownListItem>()) });
            list.Add(new SystemSetting { Name = "OperatingSystems", Value = JsonConvert.SerializeObject(settings.OperatingSystems ?? Enumerable.Empty<DropDownListItem>()) });
            list.Add(new SystemSetting { Name = "FooterLinks", Value = JsonConvert.SerializeObject(settings.FooterLinks ?? Enumerable.Empty<FooterLink>()) });
            list.Add(new SystemSetting { Name = "SerialNoHintEng", Value = settings.SerialNoHintEng });
            list.Add(new SystemSetting { Name = "SerialNoHintChi", Value = settings.SerialNoHintChi });

            var values = this.SystemSetting.ToList();
            foreach (var item in list)
            {
                var value = values.Find(x => x.Name == item.Name) ?? this.Add(item).Entity;
                value.Value = item.Value;
            }
            this.RemoveRange(values.Where(x => !list.Any(i => i.Name == x.Name)).ToList());

            return list;
        }

        public Media GetSerialNoHintMedia()
        {
            var path = SystemSettings.SerialNoHintImagePath;
            return this.Media.SingleOrDefault(x => x.Path == path);
        }

        public IEnumerable<ScheduleItem> GetTodayScheduleItems(string modelId, string district, out DateTime startDate, out DateTime endDate)
        {
            var settings = this.GetDefaultSettings();
            var holidays = this.Holiday.ToList();
            var model = this.TrainingModel.Find(modelId);

            var startTime = DateTime.Today
                .AddHours(settings.RegistrationDeadlineHour ?? 9)
                .AddMinutes(settings.RegistrationDeadlineMinute ?? 0);
            startTime = holidays.GetWorkingDate(startTime, 0);

            var day = settings.RegistrationDeadlineDay ?? 1;
            if (DateTime.Now > startTime)
                day++;

            var date = holidays.GetWorkingDate(startTime.Date, day);

            var st = date;
            var et = st.AddDays(model?.OpenedDays ?? settings.OpenedDayForRegistration ?? 30);

            startDate = st;
            endDate = et;

            return this.ScheduleItem.Include(x => x.ScheduleItemSlot).ThenInclude(x => x.Registration)
                .Where(x => x.Date >= st && x.Date < et && x.District == district)
                .ToList();
        }

        public IEnumerable<SelectItem> GetDistrictList()
        {
            return new List<SelectItem>
            {
                new SelectItem { Id = "Hk", Label = "Hong Kong Island" },
                new SelectItem { Id = "Kln", Label = "Kowloon" },
                new SelectItem { Id = "Nt", Label = "New Territories" }
            };
        }

        public IEnumerable<SelectItem> GetAttendeeList(bool zh = false)
        {
            return this.GetDefaultSettings().Attendees.Select(x => new SelectItem { Id = x.Slug, Label = zh ? x.NameChi : x.NameEng, Note = zh ? x.NoteChi : x.NoteEng });
            /*return new List<SelectItem>
            {
                new SelectItem { Id = "1-10", Label = "1 - 10" },
                new SelectItem { Id = "11+", Label = "11+" }
            };*/
        }

        public IEnumerable<SelectItem> GetOperatingSystemList(bool zh = false)
        {
            return this.GetDefaultSettings().OperatingSystems.Select(x => new SelectItem { Id = x.Slug, Label = zh ? x.NameChi : x.NameEng });
            /*return new List<SelectItem>
            {
                new SelectItem { Id = "windows", Label = "Windows" },
                new SelectItem { Id = "macOs", Label = "macOS" },
                new SelectItem { Id = "windows+macOs", Label = "Windows + macOS" }
            };*/
        }

        public EmailRequest AddEmailRequest(string templateId, object data, string to, string toName = null, string language = null)
        {
            var entry = this.Add(new EmailRequest
            {
                TemplateId = templateId,
                Language = language ?? System.Globalization.CultureInfo.CurrentCulture.Name,
                Data = JsonConvert.SerializeObject(data),
                Receiver = $"{to}:{toName}",
                RequestTime = DateTime.Now,
                Status = (int)EmailRequestStatus.Pending
            });
            return entry.Entity;
        }

        public DateTime GetClosestWorkingDate(DateTime date, int day = 1)
        {
            var holidays = this.Holiday.ToList();
            return holidays.GetWorkingDate(date, -day);
        }

        public DateTime GetRescheduleCutoffDate(DateTime trainingDate)
        {
            var date = GetClosestWorkingDate(trainingDate);
            return date.AddHours(12);
        }

        public MachineInstallationEmail EnqueueTraningReminder(RegistrationTimeslot r, bool fromCms = false)
        {
            var orderId = r.OrderId;
            var templateId = EmailTemplateId.TRAINING_REMIND;
            this.MachineInstallationEmail.Where(x => x.OrderId == orderId && x.EmailTemplateId == templateId).ToList()
                .ForEach(x => x.IsActive = false);

            var date = this.MachineInstallationDate.Find(orderId);

            // CR - Allow to create registration record if problem code is MCT
            if (date == null && fromCms)
            {
                var install = this.MachineInstallation.Find(orderId);
                if (install != null)
                {
                    date = new MachineInstallationDate
                    {
                        OrderId = install.OrderId,
                        SerialNo = install.SerialNo,
                        InstallDate = install.LastChangeTime
                    };
                }
            }

            var entry = this.MachineInstallationEmail.Add(new MachineInstallationEmail
            {
                OrderId = date.OrderId,
                SerialNo = date.SerialNo,
                InstallDate = date.InstallDate,
                EmailTemplateId = templateId,
                TargetDate = GetClosestWorkingDate(r.ScheduleDate.Value),
                IsActive = true,
                Language = System.Globalization.CultureInfo.CurrentCulture.Name
            });
            return entry.Entity;
        }
    }
}
