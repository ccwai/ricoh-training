﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Z.EntityFramework.Plus;

namespace RicohTrainingRegistration.Database
{
    public partial class RicohDB
    {
        static RicohDB()
        {
            AuditManager.DefaultConfiguration.AutoSavePreAction = (context, audit) =>
            {
                var db = (RicohDB)context;
                var entries = audit.Entries.Select(r => new AuditEntry
                {
                    AuditEntryId = r.AuditEntryID,
                    EntitySetName = r.EntitySetName,
                    EntityTypeName = r.EntityTypeName,
                    State = (int)r.State,
                    StateName = r.StateName,
                    CreatedBy = r.CreatedBy,
                    CreatedDate = r.CreatedDate,
                    AuditEntryProperty = r.Properties.Select(s => new AuditEntryProperty
                    {
                        RelationName = s.RelationName,
                        PropertyName = s.PropertyName,
                        OldValue = s.OldValueFormatted,
                        NewValue = s.NewValueFormatted
                    }).ToList()
                });
                db.AuditEntry.AddRange(entries);

                var log = SetJobLog(db.JobLogTitle, audit.Entries);
                if (log != null)
                {
                    log.CreatedBy = db.DbUser?.GetId() ?? "";
                    db.Add(log);
                }
            };
        }

        static JobLog SetJobLog(string title, List<Z.EntityFramework.Plus.AuditEntry> entries)
        {
            if (title == null)
                return null;

            var log = new JobLog
            {
                StartTime = DateTime.Now,
                EndTime = DateTime.Now,
                Title = title,
                CreatedDate = DateTime.Now
            };

            var list = new List<JobLogResultEntry>();
            foreach (var entry in entries)
            {
                var item = new JobLogResultEntry { Name = entry.EntityTypeName };
                switch (entry.State)
                {
                    case AuditEntryState.EntityAdded: item.State = "Insert"; break;
                    case AuditEntryState.EntityModified: item.State = "Update"; break;
                    case AuditEntryState.EntityDeleted: item.State = "Delete"; break;
                    default: item.State = entry.State.ToString(); break;
                }
                var changes = new Dictionary<string, OldNewValue>();

                var properties = entry.Properties;
                foreach (var p in properties)
                {
                    var name = p.PropertyName;
                    var oldValue = p.OldValueFormatted;
                    var newValue = p.NewValueFormatted;
                    if (ShouldLog(item.Name, name) || oldValue != newValue)
                        changes[name] = new OldNewValue(oldValue, newValue);
                }

                item.Changes = changes;
                list.Add(item);
            }

            log.Message = $"{list.Count} record(s) affected.";
            log.Result = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            return log;
        }

        static bool ShouldLog(string table, string column)
        {
            switch (table)
            {
                case "MachineInstallation":
                case "MachineInstallationDate": return column == "OrderId";
                case "SystemSetting": return column == "Name";
                case "TrainingModel": return column == "ModelId";
                default: return column == "Id";
            }
        }
    }
}
