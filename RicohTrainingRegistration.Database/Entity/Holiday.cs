﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class Holiday
    {
        public DateTime LastUpdated => UpdatedDate ?? CreatedDate;
    }

    public static class HolidayType
    {
        public static readonly string HkHoliday = "HK Holiday";
        public static readonly string Manual = "Manual";
    }
}
