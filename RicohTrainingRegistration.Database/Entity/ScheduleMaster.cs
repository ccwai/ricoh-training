﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleMaster
    {
        public virtual ICollection<ScheduledSession> ScheduledSessions { get; set; }
    }
}
