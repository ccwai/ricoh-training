﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class MachineInstallation
    {
        public string ContactName => string.Join(" ", new[] { FirstName?.Trim(), LastName?.Trim() }.Where(x => !string.IsNullOrEmpty(x)));

        public virtual MachineInstallationDate MachineInstallationDate { get; set; }
        public virtual TrainingModel TrainingModel { get; set; }
        public virtual ICollection<EnquiryForm> EnquiryForms { get; set; }
    }
}
