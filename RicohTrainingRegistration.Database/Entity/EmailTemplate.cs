﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class EmailTemplate
    {
        public EmailTemplate()
        {
            EmailRequests = new HashSet<EmailRequest>();
        }

        public virtual ICollection<EmailRequest> EmailRequests { get; set; }

        public string GetSubject(Dictionary<string, object> data, string language = "en")
        {
            var subject = (language.StartsWith("zh", StringComparison.OrdinalIgnoreCase) ? SubjectChi : SubjectEng) ?? SubjectEng ?? "";
            return Substitute(subject, data);
        }

        public string GetContent(Dictionary<string, object> data, string language = "en")
        {
            var content = (language.StartsWith("zh", StringComparison.OrdinalIgnoreCase) ? ContentChi : ContentEng) ?? ContentEng ?? "";
            return Substitute(content, data);
        }

        private static string Substitute(string source, Dictionary<string, object> data)
        {
            var content = source;
            if (data?.Any() == true)
            {
                foreach (var key in data.Keys)
                {
                    var val = Convert.ToString(data[key]);
                    content = content.Replace("{{" + key + "}}", val, StringComparison.Ordinal);
                }
            }
            return content;
        }
    }

    public static class EmailTemplateId
    {
        public const string REG_INVITE = "REG_INVITE";
        public const string REG_REMIND = "REG_REMIND";
        public const string REG_CONFIRM = "REG_CONFIRM";
        public const string TRAINING_REMIND = "TRAINING_REMIND";
        public const string ENQUIRY = "ENQUIRY";
        public const string TRAINING_RESCHEDULE = "TRAINING_RESCHEDULE";
        public const string TRAINING_CANCEL = "TRAINING_CANCEL";
        public const string TRAINING_REQUEST = "TRAINING_REQUEST";
        public const string TRAINING_RELEASE = "TRAINING_RELEASE";
        public const string SYSTEM_FAIL = "SYSTEM_FAIL";
    }
}
