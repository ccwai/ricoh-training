﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public static class RegistrationStatus
    {
        public static readonly string Pending = "Pending";
        public static readonly string Submitted = "Submitted";
        public static readonly string Rescheduled = "Rescheduled";
        public static readonly string Cancelled = "Cancelled";
    }

    public static class RegistrationPassphrase
    {
        public static readonly string Register = "Register";
        public static readonly string Reschedule = "Reschedule";
    }
}
