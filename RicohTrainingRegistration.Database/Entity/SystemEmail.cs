﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class SystemEmail
    {
    }

    public static class SystemEmailType
    {
        public static readonly string Trainer = "Trainer";
        public static readonly string Sender = "Sender";
        public static readonly string ReplyTo = "ReplyTo";
        public static readonly string ReturnPath = "ReturnPath";
    }
}
