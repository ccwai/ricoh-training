﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleItemSlot
    {
        private static readonly string TimeFormat = "HH:mm";
        private static CultureInfo Culture = CultureInfo.InvariantCulture;

        public DateTime? StartTime => ParseTime(Timeslot?.Split(" - ").FirstOrDefault(), ScheduleItem?.Date);
        public DateTime? EndTime => ParseTime(Timeslot?.Split(" - ").Skip(1).FirstOrDefault(), ScheduleItem?.Date);

        public static DateTime? ParseTime(string hhmm, DateTime? date = null)
        {
            if (DateTime.TryParseExact(hhmm, TimeFormat, Culture, DateTimeStyles.None, out DateTime dateTime))
            {
                if (date != null)
                    return new DateTime(date.Value.Year, date.Value.Month, date.Value.Day, dateTime.Hour, dateTime.Minute, dateTime.Second);

                return dateTime;
            }
            return new DateTime?();
        }

        public int? RegistrationId { get; set; }
        public virtual Registration Registration { get; set; }
    }
}
