﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleDefault
    {
        public int GetQuota(string district)
        {
            if (District.Hk.Equals(district, StringComparison.OrdinalIgnoreCase)) return Hk;
            if (District.Kln.Equals(district, StringComparison.OrdinalIgnoreCase)) return Kln;
            if (District.Nt.Equals(district, StringComparison.OrdinalIgnoreCase)) return Nt;
            return 0;
        }
    }
}
