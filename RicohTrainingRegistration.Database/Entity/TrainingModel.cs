﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class TrainingModel
    {
        public TrainingModel()
        {
            MachineInstallation = new HashSet<MachineInstallation>();
        }

        public virtual ICollection<MachineInstallation> MachineInstallation { get; set; }
    }
}
