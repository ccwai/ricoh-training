﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public partial class EmailRequest
    {
        public virtual EmailTemplate EmailTemplate { get; set; }
        public virtual ICollection<MachineInstallationDate> MachineInstallationDates { get; set; }
        public virtual ICollection<MachineInstallationEmail> MachineInstallationEmails { get; set; }
    }

    public enum EmailRequestStatus
    {
        Pending = 0,
        Sent = 1,
        Retry = 2,
        Fail = 3
    }
}
