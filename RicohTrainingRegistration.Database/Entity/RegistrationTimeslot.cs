﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    partial class RegistrationTimeslot
    {
        public string ContactName => string.Join(" ", new[] { FirstName?.Trim(), LastName?.Trim() }.Where(x => !string.IsNullOrEmpty(x)));
    }
}
