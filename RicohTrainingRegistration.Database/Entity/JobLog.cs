﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    partial class JobLog
    {
    }

    public static class JobLogTitle
    {
        public static readonly string RetrieveModel = "Retrieve Model";
        public static readonly string RetrieveInstallation = "Retrieve Installation";
        public static readonly string SendRegistrationEmail = "Send Registration Email";
        public static readonly string SendReminderEmail = "Send Reminder Email";

        public static readonly string SaveDefaultSchedule = "Save Default Weekly Training Schedule";
        public static readonly string SaveCustomTimeslot = "Save Custom Timeslot";
        public static readonly string SaveHoliday = "Save Holiday";
        public static readonly string SaveTrainingAgenda = "Save Training Agenda";
        public static readonly string SaveRegistration = "Save Registration";
        public static readonly string SaveEnquiry = "Save Enquiry";
        public static readonly string SaveFaq = "Save FAQ";
        public static readonly string SaveGeneralSettings = "Save General Settings";
        public static readonly string SaveEmailAddresses = "Save Email Addresses";
        public static readonly string SaveEmailTemplate = "Save Email Template";

        public static readonly string SaveTrainingModel = "Save Training Model";
        public static readonly string DeleteTrainingModel = "Delete Training Model";
    }
}
