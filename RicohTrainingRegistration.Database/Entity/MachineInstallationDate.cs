﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    partial class MachineInstallationDate
    {
        public virtual EmailRequest EmailRequest { get; set; }
        public virtual MachineInstallation MachineInstallation { get; set; }
    }
}
