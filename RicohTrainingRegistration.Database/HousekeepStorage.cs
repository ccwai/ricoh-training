﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetLib.Storage;
using Microsoft.EntityFrameworkCore;

namespace RicohTrainingRegistration.Database
{
    class HousekeepStorage
    {
        private IStorageProvider _storage;
        private readonly List<string> _paths;

        public HousekeepStorage(IStorageProvider storage)
        {
            _storage = storage;
            _paths = new List<string>();
        }

        public void Collect(DbContext db)
        {
            var entries = db.ChangeTracker.Entries<Media>()
                .Where(x => x.State == EntityState.Modified || x.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in entries)
            {
                if (entry.State == EntityState.Deleted)
                {
                    _paths.Add(entry.Entity.Path);
                    continue;
                }

                var oldPath = entry.OriginalValues.GetValue<string>("Path");
                var newPath = entry.CurrentValues.GetValue<string>("Path");
                if (!string.IsNullOrWhiteSpace(oldPath) && !oldPath.Equals(newPath, StringComparison.OrdinalIgnoreCase))
                    _paths.Add(oldPath);
            }
        }

        public void Execute()
        {
            _paths.ForEach(path => _storage.Delete(path));
        }
    }
}
