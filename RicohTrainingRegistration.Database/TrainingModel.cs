﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class TrainingModel
    {
        public string ModelId { get; set; }
        public string ModelName { get; set; }
        public DateTime ImportTime { get; set; }
        public DateTime RetrievedDate { get; set; }
        public bool IsFreeTraining { get; set; }
        public int OpenedDays { get; set; }
        public int? TrainingAgendaId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual TrainingAgenda TrainingAgenda { get; set; }
    }
}
