﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class EmailRequest
    {
        public int Id { get; set; }
        public string TemplateId { get; set; }
        public string Language { get; set; }
        public string Data { get; set; }
        public string Receiver { get; set; }
        public DateTime RequestTime { get; set; }
        public DateTime? SentTime { get; set; }
        public int Attempt { get; set; }
        public DateTime? NextAttempt { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
