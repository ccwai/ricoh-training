﻿using DotNetLib;
using DotNetLib.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Z.EntityFramework.Plus;

namespace RicohTrainingRegistration.Database
{
    public partial class RicohDB
    {
        internal string JobLogTitle { get; private set; }
        
        private readonly IStorageProvider storage;

        public RicohDB(IStorageProvider storage)
        {
            this.storage = storage;
        }

        public RicohDB(DbContextOptions<RicohDB> options, IStorageProvider storage)
            : base(options)
        {
            this.storage = storage;
        }

        public RicohDB(IStorageProvider storage, IDbUserProvider dbUser)
        {
            this.storage = storage;
            this.DbUser = dbUser;
        }

        public RicohDB(DbContextOptions<RicohDB> options, IStorageProvider storage, IDbUserProvider dbUser)
            : base(options)
        {
            this.storage = storage;
            this.DbUser = dbUser;
        }

        public IDbUserProvider DbUser { get; set; }

        public override int SaveChanges()
        {
            return SaveChanges(null);
        }

        public int SaveChanges(string jobLogTitle)
        {
            // CreatedDate, CreatedBy, UpdatedDate, UpdatedBy
            AddTrail();

            // prepare audit
            var audit = new Audit();
            audit.CreatedBy = DbUser?.GetName() ?? audit.CreatedBy ?? "";
            audit.PreSaveChanges(this);

            // add job log
            JobLogTitle = jobLogTitle;

            // housekeep media
            HousekeepStorage housekeep = null;
            if (storage != null)
            {
                housekeep = new HousekeepStorage(storage);
                housekeep.Collect(this);
            }

            // db save
            int changes = base.SaveChanges();

            // execute audit
            audit.PostSaveChanges();
            if (audit.Configuration.AutoSavePreAction != null)
            {
                audit.Configuration.AutoSavePreAction(this, audit);
                base.SaveChanges();
            }

            // execute housekeep (if any)
            housekeep?.Execute();

            // reset job log
            JobLogTitle = null;

            return changes;
        }

        private void AddTrail()
        {
            var entries = this.ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added || x.State == EntityState.Modified || x.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in entries)
            {
                var entity = entry.Entity;
                if (entry.State == EntityState.Added)
                {
                    ObjectHelper.TrySetPropertyValue(entity, "CreatedDate", DateTime.Now);
                    ObjectHelper.TrySetPropertyValue(entity, "CreatedBy", DbUser?.GetId() ?? "");
                }
                else if (entry.State == EntityState.Modified)
                {
                    ObjectHelper.TrySetPropertyValue(entity, "UpdatedDate", DateTime.Now);
                    ObjectHelper.TrySetPropertyValue(entity, "UpdatedBy", DbUser?.GetId() ?? "");
                }
                else
                {
                    if (ObjectHelper.TrySetPropertyValue(entity, "IsDeleted", true))
                    {
                        ObjectHelper.TrySetPropertyValue(entity, "DeletedBy", DbUser?.GetId() ?? "");
                        entry.State = EntityState.Modified;
                    }
                }
            }
        }
    }
}
