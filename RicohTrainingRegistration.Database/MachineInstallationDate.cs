﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class MachineInstallationDate
    {
        public MachineInstallationDate()
        {
            MachineInstallationEmail = new HashSet<MachineInstallationEmail>();
        }

        public string OrderId { get; set; }
        public string SerialNo { get; set; }
        public DateTime InstallDate { get; set; }
        public string ProblemCode { get; set; }
        public int FreeTrainingDay { get; set; }
        public DateTime FreeTrainingEndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int? EmailRequestId { get; set; }
        public bool IsFreeTraining { get; set; }
        public bool WithCutoff { get; set; }

        public virtual ICollection<MachineInstallationEmail> MachineInstallationEmail { get; set; }
    }
}
