﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class TrainingAgendaModel
    {
        public int TrainingAgendaId { get; set; }
        public string TrainingModelId { get; set; }

        public virtual TrainingAgenda TrainingAgenda { get; set; }
        public virtual TrainingModel TrainingModel { get; set; }
    }
}
