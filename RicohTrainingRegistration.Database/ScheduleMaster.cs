﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class ScheduleMaster
    {
        public ScheduleMaster()
        {
            ScheduleItem = new HashSet<ScheduleItem>();
        }

        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public virtual ICollection<ScheduleItem> ScheduleItem { get; set; }
    }
}
