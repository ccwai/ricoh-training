﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Database
{
    public static class Extensions
    {
        public static IEnumerable<ScheduleItemSlot> Defaults(this IEnumerable<ScheduleItemSlot> list)
        {
            return list.Where(x => !x.IsCustom);
        }

        public static IEnumerable<ScheduleItemSlot> Customs(this IEnumerable<ScheduleItemSlot> list)
        {
            return list.Where(x => x.IsCustom);
        }

        public static IEnumerable<ScheduleItem> GenerateScheduleItems(this IEnumerable<ScheduleDefault> defs, ScheduleMaster master, List<Holiday> holidays = null)
        {
            var date = new DateTime(master.Year, master.Month, 1);
            var days = DateTime.DaysInMonth(date.Year, date.Month);
            var dates = Enumerable.Range(0, days).Select(d => date.AddDays(d)).ToList();
            var targets = dates.Where(x => defs.Any(d => d.DayOfWeek == (int)x.DayOfWeek));
            if (holidays?.Any() == true)
                targets = targets.Where(x => !holidays.Any(h => h.StartDate <= x && x <= h.EndDate));

            Func<DateTime, IEnumerable<ScheduleDefault>> predicate = x => defs.Where(d => d.DayOfWeek == (int)x.DayOfWeek);
            return targets.Select(x => GetScheduleItem(master, x, District.Hk, predicate(x)))
                .Concat(targets.Select(x => GetScheduleItem(master, x, District.Kln, predicate(x))))
                .Concat(targets.Select(x => GetScheduleItem(master, x, District.Nt, predicate(x))));
        }

        private static ScheduleItem GetScheduleItem(ScheduleMaster master, DateTime date, string district, IEnumerable<ScheduleDefault> defs)
        {
            var item = new ScheduleItem
            {
                ScheduleMaster = master,
                Date = date,
                Day = date.Day,
                District = district
            };
            item.ScheduleItemSlot = defs.SelectMany(d => Enumerable.Range(1, d.GetQuota(district)).Select(seq => new ScheduleItemSlot { Timeslot = d.Timeslot, Seq = seq }))
                .OrderBy(s => s.Timeslot)
                .ThenBy(s => s.Seq)
                .ToList();

            return item;
        }

        public static MachineInstallation LatestOrDefault(this IQueryable<MachineInstallation> list, string serialNo)
        {
            return list.Where(x => x.SerialNo == serialNo)
                .OrderByDescending(x => x.LastChangeTime)
                .ThenByDescending(x => x.RetrievedDate)
                .FirstOrDefault();
        }

        public static bool IsHoliday(this IEnumerable<Holiday> holidays, DateTime date)
        {
            var d = date.Date;
            return d.DayOfWeek == DayOfWeek.Saturday || d.DayOfWeek == DayOfWeek.Sunday || holidays.Any(x => d >= x.StartDate && d <= x.EndDate);
        }

        public static DateTime GetWorkingDate(this IEnumerable<Holiday> holidays, DateTime date, int day = 1)
        {
            var d = date.Date;
            var dir = day >= 0 ? 1 : -1;
            var count = Math.Abs(day);
            while (true)
            {
                if (holidays.IsHoliday(d))
                {
                    d = d.AddDays(dir);
                    continue;
                }
                if (count <= 0)
                    break;

                d = d.AddDays(dir);
                count--;
            }
            return d.Add(date.TimeOfDay);
        }
    }
}
