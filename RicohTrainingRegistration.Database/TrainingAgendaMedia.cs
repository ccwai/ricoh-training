﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Database
{
    public partial class TrainingAgendaMedia
    {
        public int TrainingAgendaId { get; set; }
        public int MediaId { get; set; }
        public string Language { get; set; }
        public int? DisplayOrder { get; set; }

        public virtual Media Media { get; set; }
        public virtual TrainingAgenda TrainingAgenda { get; set; }
    }
}
