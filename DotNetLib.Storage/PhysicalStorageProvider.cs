﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetLib.Storage
{
    public class PhysicalStorageProvider : IStorageProvider
    {
        public PhysicalStorageProvider(string basePath)
        {
            BasePath = basePath;
        }

        public string BasePath { get; }

        private string FullPath(string path)
        {
            return Path.Combine(BasePath, path).Replace('/', Path.DirectorySeparatorChar);
        }

        public bool Create(string path, Stream inputStream)
        {
            string fullPath = FullPath(path);
            FileInfo fi = new FileInfo(fullPath);
            fi.Directory.Create();
            using (var stream = fi.Create())
            {
                inputStream.CopyTo(stream);
                return true;
            }
        }

        public bool Read(string path, Stream outputStream)
        {
            string fullPath = FullPath(path);
            if (File.Exists(fullPath))
            {
                using (var stream = File.OpenRead(fullPath))
                {
                    stream.CopyTo(outputStream);
                    outputStream.Seek(0, SeekOrigin.Begin);
                    return true;
                }
            }
            return false;
        }

        public bool Delete(string path)
        {
            string fullPath = FullPath(path);
            if (File.Exists(fullPath))
            {
                try
                {
                    File.Delete(fullPath);
                    return true;
                }
                catch (Exception)
                {

                }
            }
            return false;
        }

        public bool DeleteDirectory(string path)
        {
            string fullPath = FullPath(path);
            if (Directory.Exists(fullPath))
            {
                try
                {
                    Directory.Delete(fullPath, true);
                    return true;
                }
                catch (Exception)
                {

                }
            }
            return false;
        }

        public bool Exists(string path)
        {
            string fullPath = FullPath(path);
            return File.Exists(fullPath);
        }
    }
}
