﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetLib.Storage
{
    public class AzureStorageProvider : IStorageProvider
    {
        private readonly ILogger<AzureStorageProvider> logger;
        private readonly string connectionString;

        public AzureStorageProvider(ILogger<AzureStorageProvider> logger, string connectionString, string container)
        {
            if (connectionString == null)
                throw new ArgumentNullException("connectionString");
            
            if (container == null)
                throw new ArgumentNullException("container");

            this.logger = logger;
            this.connectionString = connectionString;
            this.Container = container;
        }

        public string Container { get; private set; }

        private CloudBlobContainer GetContainer()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(Container);
            container.CreateIfNotExists();

            return container;
        }

        private string CloudPath(string path)
        {
            var paths = path.Replace(Path.DirectorySeparatorChar, '/').Split('/');
            return string.Join("/", paths);
        }

        public bool Create(string path, Stream inputStream)
        {
            path = CloudPath(path);
            CloudBlobContainer container = GetContainer();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
            blockBlob.UploadFromStream(inputStream);

            bool result = blockBlob.Exists();
            logger.LogDebug("Create({0})={1}", path, result);
            return true;
        }

        public bool Delete(string path)
        {
            path = CloudPath(path);
            CloudBlobContainer container = GetContainer();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
            bool result = blockBlob.DeleteIfExists();
            
            logger.LogDebug("Delete({0})={1}", path, result);
            return result;
        }

        public bool DeleteDirectory(string path)
        {
            path = CloudPath(path);
            CloudBlobContainer container = GetContainer();
            CloudBlobDirectory dir = container.GetDirectoryReference(path);
            var blobs = dir.ListBlobs(true);

            bool success = false;
            foreach (var blob in blobs)
            {
                if (blob is CloudBlockBlob)
                    success = ((CloudBlockBlob)blob).DeleteIfExists();
            }

            logger.LogDebug("DeleteDirectory({0})={1}", path, success);
            return success;
        }

        public bool Exists(string path)
        {
            path = CloudPath(path);
            CloudBlobContainer container = GetContainer();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
            bool result = blockBlob.Exists();

            logger.LogDebug("Create({0})={1}", path, result);
            return true;
        }

        public bool Read(string path, Stream outputStream)
        {
            path = CloudPath(path);
            CloudBlobContainer container = GetContainer();
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(path);
            bool result = blockBlob.Exists();

            if (result)
            {
                blockBlob.DownloadToStream(outputStream);
                outputStream.Seek(0, SeekOrigin.Begin);
            }

            logger.LogDebug("Read({0})={1}", path, result);
            return true;
        }
    }
}
