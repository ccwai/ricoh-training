﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetLib.Storage
{
    public interface IStorageProvider
    {
        bool Create(string path, Stream inputStream);

        bool Read(string path, Stream outputStream);

        bool Delete(string path);

        bool DeleteDirectory(string path);

        bool Exists(string path);
    }
}
