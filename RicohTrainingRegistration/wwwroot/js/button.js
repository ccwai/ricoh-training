﻿var Button = function () {

    function startLoading(btn) {
        $(btn).children('img, .i-loading').addClass('loading');
    }

    function endLoading(btn) {
        $(btn).children('img, .i-loading').removeClass('loading');
    }

    function listen() {
        $('body').off('click', '[data-href]');
        $('body').on('click', '[data-href]', function () {
            var url = $(this).attr('data-href');
            var type = $(this).attr('data-href-type');
            if (type === 'reload') {
                window.location.reload();
                return;
            }
            if (url != null) {
                if (type === 'replace') {
                    window.location.replace(url);
                    return;
                }
                window.location.assign(url);
            }
        });
    }

    return ({
        startLoading: startLoading,
        endLoading: endLoading,
        listen: listen
    });
}();