﻿var Page = function () {

    var _opts = {
        dateApi: '/api/training/date',
        timeslotApi: '/api/training/timeslot',
        dateCulture: 'en',
        dateFormat: 'DD / MM / YYYY',
        restore: {
            district: null,
            date: null,
            timeslot: null
        }
    };

    var ready = 0;
    var $district, $datepicker, $timepicker, $timeslot;
    var trainingDate, trainingTimeslot;

    function init(opts) {
        if (ready) return;

        opts && $.extend(_opts, opts);

        handleControls();
        handleActions();
        handleSubmission();

        ready = 1;
        initValues();
    }

    var handleControls = function () {
        $district = $('.input-district');
        $datepicker = $('.custom-datepicker input');
        $timepicker = $('.custom-timepicker');
        $timeslot = $('.input-timeslot');

        trainingDate = $datepicker.val();
        trainingTimeslot = $timeslot.val();
    };

    var handleActions = function () {
        // prefer training date
        $('.chk-prefer').on('click', function () {
            if ($(this).is(':checked')) {
                $(this).siblings('textarea').addClass('is-select').prop('disabled', false);
                render.emptyTimeslot();
            } else {
                $(this).siblings('textarea').removeClass('is-select').prop('disabled', true);
                ready && actions.queryTimeslot();
            }
        });

        // drow down
        $('.custom-dropdown').on('click', '.dropdown-menu .dropdown-item', function () {
            var item = $(this);
            var val = item.attr('data-value');
            var dropdown = item.closest('.custom-dropdown');

            var input = dropdown.children('input:hidden');
            var oldVal = input.attr('data-value');
            input.val(val).attr('data-value', val);
            if (oldVal != val) { input.trigger('change'); }

            dropdown.trigger('selected.bs.dropdown', this);

            var feedback = dropdown.siblings('.on-select-feedback');
            var text = item.attr('data-feedback');
            if (text) {
                feedback.text(text).addClass('is-select');
            } else {
                feedback.text('').removeClass('is-select');
            }
        }).on('selected.bs.dropdown', function (item) {
            $(this).children('[data-toggle="dropdown"]').valid();
        });

        // date and timeslot
        $district.on('change', function () {
            ready && actions.queryDate();
        });
        $datepicker.on('changeDate', function () {
            ready && actions.queryTimeslot();
        });

        // timeslot selection
        $timepicker.on('click', '.item', function () {
            $(this).addClass('active').siblings().removeClass('active');
            $timeslot.val($(this).text());
            $('[data-nicescroll]').removeClass('show');
        });

        // reschedule
        $('.click-to-edit').on('click', 'img', function () {
            var div = $(this).closest('.editable');
            $('.click-to-edit', div).remove();
            $(':input', div).val('').prop('required', true).focus();
        });
    };

    var handleSubmission = function () {
        $('#frm1').on('submit', function (e) {
            e.preventDefault();
            var frm = this;

            var action = $(this).attr('data-name');
            grecaptcha.execute(sitekey, { action: action }).then(function (token) {
                document.getElementById('Recaptcha').value = token;

                var offset = 120;
                if (!$(frm).valid()) {
                    $('html, body').animate({
                        scrollTop: $(".is-invalid:first").offset().top - offset
                    }, 500);
                    return;
                }

                $('#noTimeslot').hide();
                if (!$timeslot.val() && !$('.chk-prefer').is(':checked')) {
                    $('#noTimeslot').show();
                    $('html, body').animate({
                        scrollTop: $("#noTimeslot").offset().top - offset
                    }, 500);
                    return;
                }

                var url = frm.action;
                var method = frm.method;
                var btn = $(':submit', $(frm));
                Button.startLoading(btn);

                var data = $(frm).serialize();
                $.ajax({
                    url: url,
                    data: data,
                    method: method,
                    success: function (resp) {
                        if (resp.success) {
                            Ajax.success(resp);
                            return;
                        }
                        bootbox.alert(resp.message || 'Not success');
                        Button.endLoading(btn);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var resp = Ajax.error(jqXHR, textStatus, errorThrown);
                        var message = resp.message || '';
                        bootbox.alert('<h1 class="text-danger"><i class="fas fa-times-circle"></i></h1> ' + message);
                        Button.endLoading(btn);

                        if (resp.data && resp.data.reloadTimeslot) {
                            actions.queryTimeslot();
                        }
                    }
                });
            });
        });
    };

    var initValues = function () {
        if ($('.chk-prefer').is('[data-checked]')) {
            $('.chk-prefer').trigger('click');
        }

        // initialize drop down values
        $('.custom-dropdown [name]').each(function () {
            var val = $(this).val();
            $(this).siblings('.dropdown-menu').children('.dropdown-item[data-value="' + val + '"]').trigger('click');
        });
    };

    var actions = {
        queryDate: function () {
            var district = $district.val();
            if (!district) return;

            var url = _opts.dateApi;
            var data = { district: district };
            $.ajax({
                url: url,
                data: data,
                method: 'get',
                success: function (data) {
                    render.datePicker(data);

                    if ($datepicker.val()) {
                        actions.queryTimeslot();
                    }
                },
                error: function () {
                    alert('Unable to retrieve dates.');
                }
            });
        },
        queryTimeslot: function () {
            var district = $district.val();
            var date = moment($datepicker.val(), _opts.dateFormat);
            if (!district || !date.isValid()) return;

            if (!$datepicker.datepicker('getDate')) {
                $datepicker.datepicker('setDate', date.toDate());
                return;
            }

            var url = _opts.timeslotApi;
            var data = { district: district, date: date.format('YYYY-MM-DD') };
            $.ajax({
                url: url,
                data: data,
                method: 'get',
                success: function (data) {
                    render.timeslot(data);
                },
                error: function () {
                    alert('Unable to retrieve timeslots.');
                }
            });
        }
    };

    var render = {
        datePicker: function (data) {
            if (!data.startDate || !data.endDate) return false;

            $datepicker.datepicker('destroy');
            render.emptyTimeslot();

            var opts = $.extend({
                language: _opts.dateCulture,
                format: {
                    toDisplay: function (date, format, language) {
                        return moment(date).format(_opts.dateFormat);
                    },
                    toValue: function (date, format, language) {
                        return new Date(date);
                    }
                },
                container: '.custom-datepicker',
                autoclose: true
            }, data);

            if (trainingDate) {
                var startDate = moment(opts.startDate);
                var endDate = moment(opts.endDate);
                var date = moment(trainingDate, _opts.dateFormat);
                var day, i;
                if (date.isBefore(startDate)) {
                    day = startDate.diff(date, 'days');
                    for (i = 0; i < day; i++) {
                        var backDate = moment(date).add(i, 'days').format('YYYY-MM-DD');
                        opts.datesDisabled.push(backDate);
                    }
                    opts.startDate = date.format('YYYY-MM-DD');
                } else if (date.isAfter(endDate)) {
                    day = date.diff(endDate, 'days');
                    for (i = 0; i < day; i++) {
                        var futureDate = moment(date).subtract(i, 'days').format('YYYY-MM-DD');
                        opts.datesDisabled.push(futureDate);
                    }
                    opts.endDate = date.format('YYYY-MM-DD');
                }
                trainingDate = null;
            }

            $datepicker.datepicker(opts);
            return true;
        },
        timeslot: function (data) {
            render.emptyTimeslot();
            if ($('.chk-prefer').is(':checked')) return;

            for (var i = 0; i < data.length; i++) {
                var ts = data[i];
                $('<div class="item">' + ts.value + '</div>').appendTo($timepicker);
                if (!ts.isEnabled) {
                    $('.custom-timepicker .item').eq(i).addClass('disabled');
                }
            }
            if (data.length) {
                $timepicker.find('.empty').hide();

                var val = $timeslot.attr('data-value');
                if (val) {
                    $timepicker.find('.item').each(function () {
                        if ($(this).text() == val) {
                            $(this).trigger('click');
                        }
                    });
                }
                if (trainingTimeslot) {
                    var date = moment($datepicker.datepicker('getDate'));
                    var datesDisabled = $datepicker.data('datepicker').o.datesDisabled;
                    var hasDate = false;
                    for (var x = 0; x < datesDisabled.length; x++) {
                        var value = datesDisabled[x];
                        if (date.isSame(value, 'day')) {
                            hasDate = true;
                            break;
                        }
                    }
                    if (hasDate) {
                        $timepicker.find('.item').each(function () {
                            if ($(this).text() == trainingTimeslot) {
                                $(this).addClass('disabled');
                            } else {
                                $(this).hide();
                            }
                        });
                    }
                    trainingTimeslot = null;
                }

                var restore = _opts.restore;
                if (restore.district && restore.date && restore.timeslot) {
                    var district = $district.val();
                    var date = $datepicker.val();
                    if (district == restore.district && date == restore.date) {
                        $timepicker.find('.item').each(function () {
                            if ($(this).text() == restore.timeslot) {
                                $(this).trigger('click');
                            }
                        });
                    }
                }
            }
            $timeslot.removeAttr('data-value');
        },
        emptyTimeslot: function () {
            $timepicker.find('.item').remove();
            $timepicker.find('.empty').show();
            $timeslot.val('');
        }
    };


    return ({
        init: init
    });
}();