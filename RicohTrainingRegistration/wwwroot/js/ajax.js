﻿var Ajax = function () {

    function form(opts) {
        var elem = opts.form;
        if (!elem) return;

        var $form = $(elem);
        if (!elem.length) return;

        $form.on('submit', function (e) {
            e.preventDefault();
            var frm = $(this);
            if ($.fn.validate) {
                if (!frm.valid()) {
                    return;
                }
            }

            var url = frm.attr('action');
            var data = frm.serialize();
            typeof (opts.data) === 'function' && opts.data(data);

            load({ url: url, data: data });
        });
    }

    function load(opts) {
        var settings = {
            method: 'post',
            dataType: 'json',
            success: success,
            error: error
        };
        $.extend(settings, opts);

        if (typeof(Loader) === 'object') Loader.startLoading();
        return $.ajax(settings).always(complete);
    }

    function success(resp) {
        var message = resp.message;
        if (message) {
            if (bootbox) {
                bootbox.alert(message, function () { doRedirect(resp); });
                return;
            }
            alert(message);
        }
        doRedirect(resp);
    }

    function doRedirect(resp) {
        var url = resp.redirect;
        if (typeof (url) === 'string') {
            var type = resp.redirectType;
            if (type == 'replace') { window.location.replace(url); }
            else { window.location.assign(url); }
        }
    }

    function error(jqXHR, textStatus, errorThrown) {
        var data = _error(jqXHR, textStatus, errorThrown);
        var message = data.message;
        if (bootbox) {
            message = '<h1 class="text-danger"><i class="fas fa-times-circle"></i></h1> ' + message;
            bootbox.alert({
                message: message,
                callback: function () {
                    doRedirect(data);
                }
            });
        } else {
            alert(message);
            doRedirect(resp);
        }
        return data;
    }

    function _error(jqXHR, textStatus, errorThrown) {
        var data = {};
        var message = '';
        var text = jqXHR.responseText;
        var type = jqXHR.getResponseHeader('content-type');
        if (type == null) {
            message = 'Server unavailable';
        } else {
            if (type.indexOf('application/json') > -1) {
                var resp = JSON.parse(text);
                $.extend(data, resp);
                message = resp.message;
            }
            if (type.indexOf('html') > -1) {
                console.debug('ajaxError', jqXHR);
                if (status >= 400) {
                    message = jqXHR.statusText;
                } else {
                    message = 'Unexpected response';
                }
            }
        }
        message = message || 'Unexpected error';
        data.message = message;
        return data;
    }

    function complete(jqXHR, textStatus) {
        if (typeof (Loader) === 'object') Loader.endLoading();
    }

    return ({
        form: form,
        load: load,
        success: success,
        error: _error
    });
}();