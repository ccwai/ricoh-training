$(document).ready(function (e) {
	//set styleElement
	let heightHeader = $('.header-bar').outerHeight();

	$('.page-home, .page-maintenance, .container-wrapper, .page-banner').css('padding-top', `${heightHeader}px`);
	$(window).resize(function () {
		heightHeader = $('.header-bar').outerHeight();
	});

	//page-home custom-popover
	$('.custom-popover-wrapper .icon').click(function() {
		$(this).siblings('.custom-popover').toggle();
		console.log('toggle');
	});

	//page-home bg
	function bgResize() {
		if($(window).width() > 1200 && $(window).height() < 766) {
			let bgWidth = 766 / $(window).height();
			$('.page-home .bg').css({
				"background-size": 125 * bgWidth + "%",
				"height": 100 + "%"
			});
		} else {
			$('.page-home .bg').css({
				"background-size": "cover",
				"height": 100 + "vh"
			});
			$('.page-home.--reschedule .bg').css({
				"background-size": 110 * (1400 / $(window).width()) + "% auto"
			});
		}
	}
	bgResize();
	$(window).resize(function () {
		bgResize();
	});

	//collapse button toggle
	$('.custom-toggle-btn').click(function() {
		$(this).toggleClass('toggle');
	});
	
	//show on scroll
	function setScrollStyle() {
		var value = $(window).scrollTop();

		if ( value > 0 ) {
			$('.header-bar').addClass('scrolling');
			$('.scroll-top').addClass('scrolling');
		} else {
			$('.header-bar').removeClass('scrolling');
			$('.scroll-top').removeClass('scrolling');
		}
	}
	setScrollStyle();
	$(window).scroll(function() {
		setScrollStyle();
	});

	//back to top
	$('.scroll-top').click(function() {
        $('html,body').animate({
            scrollTop: 0
        }, 700);
	});
	
	//agenda hide extra text
	var agenda = $('.agenda');
	if (agenda.length) {
		setTimeout(function() {
			var height = document.querySelector('.agenda').clientHeight,
			scrollHeight = document.querySelector('.agenda').scrollHeight;
	
			if(scrollHeight-1 > height) {
				$('.agenda span').css('display', 'block');
				// console.log(scrollHeight, height)
			} else {
				$('.agenda span').css('display', 'none');
				// console.log(scrollHeight, height)
			}
		}, 1000)
	}

	//custom-dropdown
	$('.custom-dropdown .dropdown-menu a').click(function () {
		$(this).parent().siblings('input').val($(this).text());
	});

	//custome-timepicker
	if($('.custom-timepicker').attr('data-disabled')) {
        $('.custom-timepicker .item').removeClass('active');
    }
	$('.custom-dropdown[data-toggle] .dropdown-menu a').click(function(){
		$('[data-nicescroll]').removeClass('show');
		$(this).parent().siblings('input').val($(this).text());
		// if($(this).html() == "11+"){
		// 	$('.custom-timepicker, .nicescroll-rails, [data-nicescroll]').attr('data-disabled', false);
		// } else {
		// 	$('.custom-timepicker, .nicescroll-rails, [data-nicescroll]').attr('data-disabled', true);
		// 	$('.custom-timepicker .item').removeClass('active');
		// 	$('[data-nicescroll] input').val('');
		// }
	});
	$('.custom-dropdown[data-toggle] input').change(function() {
		// if($(this).val() == "11+") {
		// 	$('.custom-timepicker, .nicescroll-rails, [data-nicescroll]').attr('data-disabled', false);
		// } else {
		// 	$('.custom-timepicker, .nicescroll-rails, [data-nicescroll]').attr('data-disabled', true);
		// 	$('.custom-timepicker .item').removeClass('active');
		// 	$('[data-nicescroll] input').val('');
		// }
	});
	$('[data-nicescroll] input').click(function () {
		$(this).parent().toggleClass('show');
	});
	$(document).mouseup(function(e) {
	    var container = $('[data-nicescroll]');
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        container.removeClass('show');
	    }
	});

	//add scrollbar
	//https://github.com/inuyaksa/jquery.nicescroll
	$("[data-niceScroll]").niceScroll({
		smoothscroll: true,
		cursorwidth: "4px",
		disableoutline: true,
		horizrailenabled: true,
	});

});

