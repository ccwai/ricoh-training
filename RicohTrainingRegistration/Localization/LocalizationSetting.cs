﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public static class LocalizationSetting
    {
        public static readonly CultureInfo[] DefaultSupportedCultures = new[]
        {
            new CultureInfo("zh"),
            new CultureInfo("en")
        };

        public static readonly string RouteDataName = "culture";
    }
}
