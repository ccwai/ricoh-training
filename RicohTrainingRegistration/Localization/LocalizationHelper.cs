﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public static class LocalizationHelper
    {
        public static CultureInfo GetCulture(this HttpContext httpContext)
        {
            return httpContext.Features?.Get<IRequestCultureFeature>()?.RequestCulture?.Culture;
        }

        public static string ChangeCulture(this HttpContext httpContext, Uri uri, string culture, bool setCookie = true)
        {
            var request = httpContext.Request;
            var pathBase = request.PathBase.Value;
            var supportedCultures = httpContext.GetSupportedCultures();
            var builder = uri != null ? new UriBuilder(uri) : new UriBuilder() { Path = pathBase };
            var localUrl = builder.Uri.PathAndQuery.Substring(pathBase.Length);
            var supportedCulture = supportedCultures.FirstOrDefault(x => x.Name.Equals(culture, StringComparison.OrdinalIgnoreCase)
                || culture.StartsWith(x.TwoLetterISOLanguageName, StringComparison.OrdinalIgnoreCase)
            );

            if (supportedCulture != null)
            {
                culture = supportedCulture.Name.ToLowerInvariant();
                var paths = localUrl.Split('/').ToList();
                var path = paths.Skip(1).FirstOrDefault();
                if (supportedCultures.Any(c => c.Name.Equals(path, StringComparison.OrdinalIgnoreCase)))
                {
                    paths[1] = culture;
                }
                else
                {
                    if (!request.RouteValues.ContainsKey(LocalizationSetting.RouteDataName))
                        paths.Insert(1, culture);

                }

                localUrl = pathBase + string.Join("/", paths);

                if (setCookie)
                    httpContext.SetCultureCookie(culture);
            }
            return localUrl;
        }

        public static void SetCultureCookie(this HttpContext httpContext, string culture)
        {
            httpContext.Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(
                    new RequestCulture(culture)),
                    new CookieOptions
                    {
                        Expires = DateTimeOffset.UtcNow.AddYears(1),
                        HttpOnly = true,
                        Secure = httpContext.Request.IsHttps
                    }
            );
        }

        public static IList<CultureInfo> GetSupportedCultures(this HttpContext httpContext)
        {
            return httpContext.RequestServices.GetRequiredService<IOptions<RequestLocalizationOptions>>()?.Value.SupportedCultures.ToList();
        }

        public static IMvcBuilder ConfigureLocalization(this IMvcBuilder builder)
        {
            return builder.AddLocalization(null);
        }

        public static IMvcBuilder AddLocalization(this IMvcBuilder builder, IConfiguration configuration)
        {
            var defaultCultures = LocalizationSetting.DefaultSupportedCultures.Select(c => c.Name).ToArray();
            var cultures = configuration?.GetSection("SupportedCultures").Get<string[]>() ?? defaultCultures;
            var supportedCultures = cultures.Select(c => new CultureInfo(c)).ToArray();
            if (supportedCultures.Any())
            {
                //builder.Services.AddLocalization();

                builder.Services.Configure<RequestLocalizationOptions>(options =>
                {
                    options.DefaultRequestCulture = new RequestCulture(cultures.First());
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;

                    var providers = options.RequestCultureProviders;
                    var index = providers.ToList().FindIndex(x => x is AcceptLanguageHeaderRequestCultureProvider);
                    if (index >= 0)
                        providers[index] = new CustomAcceptLanguageHeaderRequestCultureProvider();

                    providers.Insert(0, new RouteValueRequestCultureProvider(supportedCultures));

                });

                builder = builder.AddRazorPagesOptions(options =>
                {
                    options.Conventions.Add(new LocalizedPageRouteModelConvention(supportedCultures));
                });

                builder.AddViewLocalization();

                builder.Services.AddSingleton<IUrlHelperFactory, LocalizationUrlHelperFactory>();
            }
            return builder;
        }

        public static IApplicationBuilder UseConfiguredRequestLocalization(this IApplicationBuilder app)
        {
            var option = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            return app.UseRequestLocalization(option.Value);
        }
    }
}
