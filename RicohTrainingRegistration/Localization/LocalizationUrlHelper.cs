﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public class LocalizationUrlHelper : IUrlHelper
    {
        private readonly IUrlHelper defaultUrlHelper;
        
        public LocalizationUrlHelper(ActionContext actionContext, IUrlHelper defaultUrlHelper)
        {
            this.ActionContext = actionContext;
            this.defaultUrlHelper = defaultUrlHelper;
        }

        public ActionContext ActionContext { get; }

        public string Action(UrlActionContext actionContext) => defaultUrlHelper.Action(actionContext);

        public string Content(string contentPath) => defaultUrlHelper.Content(contentPath);

        public bool IsLocalUrl(string url) => defaultUrlHelper.IsLocalUrl(url);

        public string Link(string routeName, object values) => defaultUrlHelper.Link(routeName, values);

        public string RouteUrl(UrlRouteContext routeContext)
        {
            var routeValues = new RouteValueDictionary(routeContext.Values);
            var name = LocalizationSetting.RouteDataName;
            if (!routeValues.ContainsKey(name))
            {
                var culture = ActionContext.HttpContext.GetCulture() ?? System.Globalization.CultureInfo.CurrentCulture;
                routeValues[name] = culture.Name;
            }
            routeContext.Values = routeValues;
            return defaultUrlHelper.RouteUrl(routeContext);
        }
    }

    public class LocalizationUrlHelperFactory : IUrlHelperFactory
    {
        public IUrlHelper GetUrlHelper(ActionContext context)
        {
            var originalUrlHelperFactory = new UrlHelperFactory();
            var originalUrlHelper = originalUrlHelperFactory.GetUrlHelper(context);
            return new LocalizationUrlHelper(context, originalUrlHelper);
        }
    }
}
