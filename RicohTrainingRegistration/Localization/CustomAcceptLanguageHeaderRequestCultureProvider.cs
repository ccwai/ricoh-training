﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public class CustomAcceptLanguageHeaderRequestCultureProvider : AcceptLanguageHeaderRequestCultureProvider
    {
        async public override Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            var result = await base.DetermineProviderCultureResult(httpContext);

            if (result != null)
            {

            }

            return result;
        }
    }
}
