﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public class LocalizedPageRouteModelConvention : IPageRouteModelConvention
    {
        private readonly CultureInfo[] supportedCultures;
        private string culturePattern;

        public LocalizedPageRouteModelConvention(CultureInfo[] supportedCultures)
        {
            this.supportedCultures = supportedCultures;
            culturePattern = $@"^({string.Join("|", supportedCultures.Select(c => c.Name))})$";
        }

        public void Apply(PageRouteModel model)
        {
            var count = model.Selectors.Count;
            var removeSelectors = new List<SelectorModel>();

            for (int i = 0; i < count; i++)
            {
                var selector = model.Selectors[i];
                var template = selector.AttributeRouteModel.Template;
                var cultureSelector = new SelectorModel
                {
                    AttributeRouteModel = new AttributeRouteModel
                    {
                        Order = template.Split('/').Last().EndsWith("index", StringComparison.OrdinalIgnoreCase) ? 0 : -1,
                        Template = AttributeRouteModel.CombineTemplates($"{{{LocalizationSetting.RouteDataName}:regex({culturePattern})}}", template)
                    }
                };

                if (template == "")
                    removeSelectors.Add(selector);

                model.Selectors[i] = cultureSelector;
            }

            foreach (var selector in removeSelectors)
                model.Selectors.Remove(selector);
        }
    }
}
