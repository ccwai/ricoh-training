﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Localization
{
    public class RouteValueRequestCultureProvider : IRequestCultureProvider
    {
        private readonly CultureInfo[] supportedCultures;

        public RouteValueRequestCultureProvider(CultureInfo[] supportedCultures)
        {
            this.supportedCultures = supportedCultures;
        }

        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            var request = httpContext.Request;
            var path = request.Path.Value;
            var value = path.Split('/').Skip(1).FirstOrDefault();
            var culture = supportedCultures.FirstOrDefault(x => x.Name.Equals(value, StringComparison.OrdinalIgnoreCase))?.Name;

            if (culture == null)
            {
                // find cookie
                var cookie = request.Cookies[CookieRequestCultureProvider.DefaultCookieName];
                var result = CookieRequestCultureProvider.ParseCookieValue(cookie);
                if (result != null)
                    return Task.FromResult(result);

                // check route values
                var name = LocalizationSetting.RouteDataName;
                if (request.RouteValues.ContainsKey(name))
                    culture = request.RouteValues[name]?.ToString();
            }

            if (string.IsNullOrWhiteSpace(culture))
                //culture = LocalizationSetting.DefaultCulture.Name;
                return Task.FromResult((ProviderCultureResult)null);

            return Task.FromResult(new ProviderCultureResult(culture));
        }
    }
}
