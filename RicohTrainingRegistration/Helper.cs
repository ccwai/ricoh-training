﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RicohTrainingRegistration
{
    public static class Helper
    {
        public static string OrderId(this ClaimsPrincipal user) => user?.FindFirstValue(ClaimTypes.NameIdentifier);
        public static string SerialNo(this ClaimsPrincipal user) => user?.FindFirstValue(ClaimTypes.Name);

        public static string MaskPhoneNo(this string phoneNo)
        {
            return phoneNo.Substring(0, 3) + "****" + phoneNo.Substring(7);
        }

        public static string MaskEmail(this string email)
        {
            var arr = email.Split("@");
            var left = arr[0].ToCharArray();
            for (int i = 1; i < left.Length - 1; i++)
            {
                left[i] = '*';
            }
            return new string(left) + "@" + arr[1];
        }

        public static IHtmlContent Paragraph(this IHtmlHelper htmlHelper, string content)
        {
            content = (content ?? "").Replace(Environment.NewLine, "<br />");
            return htmlHelper.Raw(content);
        }

        public static string ContentLink(this IUrlHelper urlHelper, string contentPath)
        {
            var request = urlHelper.ActionContext.HttpContext.Request;
            var host = request.Host;
            var builder = host.Port.HasValue ? new UriBuilder(request.Scheme, host.Host, host.Port.Value) : new UriBuilder(request.Scheme, host.Host);
            builder.Path = urlHelper.Content(contentPath); ;
            return builder.Uri.AbsoluteUri;
        }
    }
}
