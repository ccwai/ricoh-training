﻿using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class ApiController : ControllerBase
    {
        private readonly ILogger<ApiController> logger;
        private readonly IStringLocalizer<Resource> localizer;
        private readonly RicohDB db;
        private readonly SiteSettings settings;

        public ApiController(ILogger<ApiController> logger, IStringLocalizer<Resource> localizer, RicohDB db, SiteSettings settings)
        {
            this.logger = logger;
            this.localizer = localizer;
            this.db = db;
            this.settings = settings;
        }

        private string Localize(string name, params object[] arguments)
        {
            if (name != null)
                return localizer.GetString(name, arguments);
            return name;
        }

        [Route("api/training/date")]
        public IActionResult TrainingDate([FromQuery]string district)
        {
            if (!new[] { "hk", "kln", "nt" }.Contains(district?.ToLower()))
                return new JsonResult(new { });

            var orderId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var expiryDate = db.MachineInstallationDate.Find(orderId)?.FreeTrainingEndDate;

            var modelId = User.FindFirst("ModelId")?.Value;

            DateTime startDate, endDate;
            var items = db.GetTodayScheduleItems(modelId, district, out startDate, out endDate);
            var totalDays = (int)(endDate - startDate).TotalDays;
            var dates = Enumerable.Range(0, totalDays).Select(x => startDate.AddDays(x)).ToList();
            var datesDisabled = dates.Where(x => !items.Any(d => d.Date == x && d.ScheduleItemSlot.Any(s => d.IsUseCustom == s.IsCustom)) || x > expiryDate).ToList();
            startDate = dates.Any(x => !datesDisabled.Contains(x)) ? dates.First(x => !datesDisabled.Contains(x)) : startDate;

            if (items.Any())
            {
                var maxDate = items.Max(x => x.Date);
                endDate = endDate > maxDate ? maxDate : endDate;
                datesDisabled = datesDisabled.Where(x => x < maxDate).ToList();
            }

            return new JsonResult(new
            {
                startDate = startDate.ToString("yyyy-MM-dd"),
                endDate = endDate.AddDays(-1).ToString("yyyy-MM-dd"),
                datesDisabled = datesDisabled.Select(x => x.ToString("yyyy-MM-dd"))
            });
        }

        [Route("api/training/timeslot")]
        public IActionResult TrainingTimeslot([FromQuery]string district, [FromQuery]string date)
        {
            if (!new[] { "hk", "kln", "nt" }.Contains(district?.ToLower())
                || !DateTime.TryParseExact(date, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime trainingDate))
                return new JsonResult(Enumerable.Empty<object>());

            var orderId = User.OrderId();
            var reg = db.RegistrationTimeslot.FirstOrDefault(x => x.OrderId == orderId);

            var slots = db.ScheduleItemSlot
                .Where(x => x.ScheduleItem.District == district && x.ScheduleItem.Date == trainingDate)
                .Where(x => x.IsCustom == x.ScheduleItem.IsUseCustom)
                .ToList();
            var data = slots.GroupBy(x => x.Timeslot).Select(x => new
            {
                value = x.Key,
                isEnabled = x.Any(s => s.RegistrationId == null || s.RegistrationId == reg?.Id)
            }).OrderBy(x => x.value).ToList();

            return new JsonResult(data);
        }

        [HttpPost]
        [Route("api/training/cancel")]
        public IActionResult CancelTraining()
        {
            var orderId = User.OrderId();
            var serialNo = User.SerialNo();
            logger.LogDebug("Request cancellation of training for serial no. {0} ({1})", serialNo, orderId);

            var registration = db.Registration.SingleOrDefault(x => x.OrderId == orderId);
            if (registration == null)
            {
                logger.LogInformation("Registration not found for serial no. {0} ({1})", serialNo, orderId);
                return BadRequest(new { message = Localize("Registration could not be found.") });
            }

            if (registration.Status != RegistrationStatus.Submitted && registration.Status != RegistrationStatus.Rescheduled)
            {
                logger.LogInformation("Invalid registration status for cancellation for serial no. {0} ({1}) - {2}", serialNo, orderId, registration.Status);
                return BadRequest(new { message = Localize("Registration could not be found."), redirect = Url.Page("/Logout"), redirectType = "replace" });
            }

            db.ScheduleItemSlot.Where(x => x.Registration.OrderId == orderId)
                .ToList()
                .ForEach(x => x.RegistrationId = null);

            registration.Status = RegistrationStatus.Cancelled;
            db.SaveChanges();

            // send email
            var id = registration.Id;
            var r = db.RegistrationTimeslot.Find(id);
            var sn = SecurityHelper.Encrypt(RegistrationPassphrase.Register, r.SerialNo);
            var regDate = db.MachineInstallationDate.FirstOrDefault(x => x.OrderId == orderId);
            var zh = System.Globalization.CultureInfo.CurrentCulture.Name.StartsWith("zh", StringComparison.OrdinalIgnoreCase);
            db.AddEmailRequest(
                EmailTemplateId.TRAINING_CANCEL,
                new
                {
                    CUSTOMER_NAME = r.CompanyName,
                    CONTACT_NAME = r.ContactName,
                    PHONE_NO = r.PhoneNo,
                    MODEL_NO = r.ModelId,
                    SERIAL_NO = r.SerialNo,
                    TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", r.ScheduleDate ?? r.TrainingDate),
                    TRAINING_TIMESLOT = r.ScheduleTimeslot ?? r.TrainingTimeslot,
                    VENUE_ADDRESS = r.VenueAddress?.Trim(),
                    LOGIN_URL = Url.Action("Invite", "Home", new { sn }, Request.Scheme, Request.Host.Value),
                    REG_DATE = string.Format("{0:yyyy/MM/dd}", regDate?.FreeTrainingEndDate),
                    DISTRICT = Localize(db.GetDistrictList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.ScheduleDistrict ?? r.District))?.Label),
                    NO_SUITABLE_CHOICE = r.TrainingDatePreferable == true ? "Y" : "N",
                    NO_SUITABLE_CHOICE_REMARK = string.IsNullOrWhiteSpace(r.TrainingDateRemarks) ? Localize("-") : r.TrainingDateRemarks,
                    ATTENDEES = db.GetAttendeeList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Label,
                    ATTENDEES_NOTE = db.GetAttendeeList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Note,
                    OS = db.GetOperatingSystemList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.OperatingSystem))?.Label,
                    COMPANY_NAME = r.CompanyName,
                    EMAIL = r.Email,
                    REMARKS = string.IsNullOrWhiteSpace(r.Remarks) ? Localize("-") : r.Remarks
                },
                r.Email,
                r.ContactName
            );
            db.SaveChanges();

            // clear remind email
            var templateId = EmailTemplateId.TRAINING_REMIND;
            db.MachineInstallationEmail.Where(x => x.OrderId == orderId && x.EmailTemplateId == templateId).ToList()
                .ForEach(x => x.IsActive = false);
            db.SaveChanges();

            return Ok(new
            {
                success = true,
                redirect = Url.Page("/Reschedule/Cancel")
            });
        }
    }
}
