﻿using Localization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        private readonly IStringLocalizer<Resource> localizer;
        private readonly RicohDB db;
        private readonly SiteSettings settings;
        private readonly Authorization auth;

        public HomeController(ILogger<HomeController> logger, IStringLocalizer<Resource> localizer, RicohDB db, SiteSettings settings, Authorization auth)
        {
            this.logger = logger;
            this.localizer = localizer;
            this.db = db;
            this.settings = settings;
            this.auth = auth;
        }

        [Route("")]
        public IActionResult Index()
        {
            var culture = HttpContext.GetCulture()?.Name ?? "en";
            var url = Request.GetDisplayUrl();
            var localUrl = HttpContext.ChangeCulture(new Uri(url), culture, false);
            return LocalRedirect(localUrl);
        }

        [Route("login")]
        public IActionResult Login(string redirect = null)
        {
            var path = Url.Page("/Index");
            if (redirect != null && redirect.StartsWith($"{Url.Page("/Reschedule")}", StringComparison.OrdinalIgnoreCase))
                path = Url.Page("/Reschedule");
            return LocalRedirect(path);
        }

        [Route("set-lang")]
        public IActionResult SetLang(string culture)
        {
            var referer = Request.GetTypedHeaders()?.Referer;
            if (referer != null && referer.Authority != new Uri(Url.PageLink("/Index")).Authority)
                referer = null;

            var localUrl = HttpContext.ChangeCulture(referer, culture);
            return LocalRedirect(localUrl);
        }

        [Route("invite")]
        async public Task<IActionResult> Invite(string sn)
        {
            try
            {
                var serialNo = DotNetLib.SecurityHelper.Decrypt(RegistrationPassphrase.Register, sn);
                var install = db.MachineInstallation.Include(x => x.MachineInstallationDate).LatestOrDefault(serialNo);

                // 1. sn is valid
                if (install == null)
                    return EnquiryResult(serialNo, Message.SerialNoNotFound);

                // 2. model is entitled
                if (install.MachineInstallationDate?.IsFreeTraining != true)
                    return EnquiryResult(serialNo, Message.ModelIsNotEntitled);

                // 3. sn is registered
                var orderId = install.OrderId;
                var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
                var reg = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId && status.Contains(x.Status));
                if (reg != null)
                    return EnquiryResult(serialNo, Message.TrainingRegistered, string.Format("{0:yyyy/MM/dd}", reg.SubmissionDate));

                // 3. sn is provided
                var provided = settings.CompletedTrainingProblemCodes.ToList();
                if (provided.Contains(install.ProblemCode))
                    return EnquiryResult(serialNo, Message.TrainingProvided, string.Format("{0:yyyy/MM/dd}", install.LastChangeTime));

                // 4. within free training period
                var installDate = install.MachineInstallationDate;
                var today = DateTime.Today;
                if (!(installDate?.FreeTrainingEndDate >= today))
                    return EnquiryResult(serialNo, Message.NotInRegistrationPeriod);

                // signin system
                await auth.SignInAsync(install);

                return RedirectToPage("/Registration/Index");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Unable to decrypt serial no.");
            }

            return RedirectToPage("/Index");
        }

        [Route("invite/reschedule")]
        async public Task<IActionResult> InviteReschedule(string sn)
        {
            try
            {
                var serialNo = DotNetLib.SecurityHelper.Decrypt(RegistrationPassphrase.Reschedule, sn);
                var install = db.MachineInstallation.Include(x => x.MachineInstallationDate).LatestOrDefault(serialNo);

                // 1. sn is valid
                if (install == null)
                    return RescheduleFailResult(serialNo, Message.SerialNoNotFound);

                // 1. sn is registed
                var orderId = install.OrderId;
                var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
                var reg = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId && status.Contains(x.Status));
                if (reg == null)
                    return RescheduleFailResult(serialNo, Message.TrainingNotRegistered);

                // 1. sn is provided
                var provided = settings.CompletedTrainingProblemCodes.ToList();
                if (provided.Contains(install.ProblemCode))
                    return RescheduleFailResult(serialNo, Message.TrainingProvidedShort, string.Format("{0:yyyy/MM/dd}", install.LastChangeTime));

                // training time
                var timeslot = reg.ScheduleTimeslot?.Split('-').FirstOrDefault().Trim();
                var trainingTime = string.Format("{0:yyyy-MM-dd} {1}", reg.ScheduleDate, timeslot);
                if (!DateTime.TryParseExact(trainingTime, "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime date))
                    return RescheduleFailResult(serialNo, "Training timeslot is not found.");

                // 2. with/without cutoff
                var now = DateTime.Now;
                if (settings.RescheduleHasCutoff)
                {
                    var cutoff = db.GetRescheduleCutoffDate(date.Date);
                    if (now >= cutoff && now <= date)
                    {
                        var qs = System.Web.HttpUtility.ParseQueryString("");
                        qs["OrderId"] = install.OrderId;
                        qs["SerialNo"] = install.SerialNo;
                        var q = DotNetLib.SecurityHelper.Encrypt("RescheduleRequest", qs.ToString());
                        return RedirectToPage("/Reschedule/Request/Index", new { q });
                    }
                }
                else
                {
                    if (now > date)
                        return RescheduleFailResult(serialNo, "Training is completed");
                }

                // signin system
                await auth.SignInAsync(install);

                return RedirectToPage("/Reschedule/Preview");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Unable to decrypt serial no.");
            }

            return RedirectToPage("/Reschedule");
        }

        private IActionResult EnquiryResult(string id, string message, params object[] arguments)
        {
            var m = Guid.NewGuid().ToString("N");
            TempData[m] = Newtonsoft.Json.JsonConvert.SerializeObject(new { message, arguments });
            //return RedirectToPage("/Enquiry/Index", new { id, m });

            ViewBag.Url = Url.Page("/Enquiry/Index", new { id, m });
            return View("~/Pages/Invite.cshtml");
        }

        private IActionResult RescheduleFailResult(string id, string message, params object[] arguments)
        {
            var m = Guid.NewGuid().ToString("N");
            TempData[m] = localizer[message, arguments]?.Value;
            return RedirectToPage("/Reschedule/Fail", new { id, m });
        }
    }
}
