﻿using DotNetLib.Storage;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Controllers
{
    public class DownloadController : ControllerBase
    {
        private readonly RicohDB db;
        private readonly IStorageProvider storage;

        public DownloadController(RicohDB db, IStorageProvider storage)
        {
            this.db = db;
            this.storage = storage;
        }

        [Route("download")]
        public IActionResult Index([FromQuery]int fileId, [FromQuery]int dl = 0)
        {
            var media = db.Media.Find(fileId);
            if (media != null)
            {
                var stream = new MemoryStream();
                if (storage.Read(media.Path, stream))
                {
                    var content = stream.ToArray();
                    if (dl == 1)
                        return File(content, media.ContentType, media.FileName);
                    return File(content, media.ContentType);
                }
            }
            return NotFound();
        }

        [AllowAnonymous]
        [Route("download/faq")]
        public IActionResult Faq(string lang = null)
        {
            CultureInfo culture = null;
            try { culture = CultureInfo.GetCultureInfo(lang); }
            catch { }
            finally { culture = culture ?? CultureInfo.CurrentCulture; }

            lang = culture.TwoLetterISOLanguageName.StartsWith("zh", StringComparison.OrdinalIgnoreCase) ? "zh" : "en";
            var docs = db.FaqDocument.Where(x => x.Language.StartsWith(lang)).ToList();
            if (docs.Any())
            {
                var doc = docs.OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                return Index(doc.MediaId);
            }
            return NotFound();
        }
    }
}
