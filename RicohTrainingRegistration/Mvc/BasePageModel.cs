﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Mvc
{
    public abstract class BasePageModel : PageModel
    {
        static readonly string RecaptchaSiteVerifyApi = "https://www.google.com/recaptcha/api/siteverify";

        private ILogger<BasePageModel> _logger => HttpContext.RequestServices.GetRequiredService<ILogger<BasePageModel>>();

        private decimal RecaptchaThresohold => HttpContext.RequestServices.GetRequiredService<IConfiguration>().GetValue<decimal>("GoogleRecaptcha:Threshold", 0.5m);

        protected IStringLocalizer<Resource> Localizer => HttpContext.RequestServices.GetRequiredService<IStringLocalizer<Resource>>();

        protected string Localize(string name, params object[] arguments) => Localizer.GetString(name, arguments);

        protected string ModelStateError => ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).FirstOrDefault(x => x != null);

        [Display(Name = "Recaptcha")]
        [BindProperty]
        public string Recaptcha { get; set; }

        protected IActionResult Json(object value, int status = 200)
        {
            var result = new JsonResult(value);
            result.StatusCode = status;
            return result;
        }

        protected IActionResult Ok(object data = null, string redirect = null, string redirectType = null, bool success = true, string message = null, int status = 200)
        {
            return Json(new { success, message, data, redirect, redirectType }, status);
        }

        protected IActionResult Fail(string message = null, object data = null, string redirect = null, string redirectType = null, int status = 400)
        {
            _logger.LogInformation(message);
            return Ok(data, redirect, redirectType, false, message, status);
        }

        async protected Task<bool> ValidateRecaptchaAsync()
        {
            var config = HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            var secret = config.GetSection("GoogleRecaptcha:SecretKey")?.Value;
            var response = Recaptcha;

            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "secret", secret },
                    { "response", response },
                    { "remoteip", HttpContext.Connection.RemoteIpAddress.ToString() }
                });
                var resp = await client.PostAsync(RecaptchaSiteVerifyApi, content);
                if (resp.IsSuccessStatusCode)
                {
                    var json = await resp.Content.ReadAsStringAsync();
                    var result = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(json, new
                    {
                        success = true,
                        score = 0.5m,
                        action = "",
                        challenge_ts = new DateTime?(),
                        hostname = "",
                        error_codes = Enumerable.Empty<string>()
                    });
                    if (result.score >= RecaptchaThresohold)
                        return result.success;
                }
            }
            return false;
        }
    }
}
