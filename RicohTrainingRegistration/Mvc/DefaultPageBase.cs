﻿using Localization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration
{
    public abstract class DefaultPageBase : PageBase
    {
        public string Culture => HttpContext.GetCulture()?.Name;
        public bool IsZh => Culture?.StartsWith("zh", StringComparison.OrdinalIgnoreCase) == true;

        public LocalizedString Localize(string name, params object[] arguments)
        {
            var localizer = HttpContext.RequestServices.GetRequiredService<IStringLocalizer<Resources.Resource>>();
            return localizer[name, arguments];
        }

        public LocalizedHtmlString LocalizeHtml(string name, params object[] arguments)
        {
            var localizer = HttpContext.RequestServices.GetRequiredService<IHtmlLocalizer<Resources.Resource>>();
            return localizer[name, arguments];
        }
    }
}
