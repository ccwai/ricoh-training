﻿using Localization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RicohTrainingRegistration
{
    public class Authorization
    {
        public Authorization(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            HttpContext = httpContextAccessor.HttpContext;
            Configuration = configuration;
        }

        public HttpContext HttpContext { get; }
        public IConfiguration Configuration { get; }

        public async Task SignInAsync(MachineInstallation machine)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, machine.OrderId),
                new Claim(ClaimTypes.Name, machine.SerialNo),
                new Claim("CompanyName", machine.CompanyName),
                new Claim("ModelId", machine.ModelId)
            };
            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var expiry = Configuration.GetValue<int>("SessionExpiry", 1200);
            var authProperties = new AuthenticationProperties
            {
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddSeconds(expiry)
            };

            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(claimsIdentity),
                authProperties
            );

            HttpContext.SetCultureCookie(HttpContext.GetCulture().Name);
        }

        public async Task SignOutAsync()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
