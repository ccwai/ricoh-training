﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Models;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages.Registration
{
    public class IndexModel : BasePageModel
    {
        protected readonly ILogger<IndexModel> _logger;
        protected readonly RicohDB db;

        public IndexModel(ILogger<IndexModel> logger, RicohDB db)
        {
            _logger = logger;
            this.db = db;
        }

        [BindProperty]
        public RegisterModel Register { get; set; }

        [BindProperty]
        public string Session { get; set; }

        public MachineInstallation Install { get; private set; }
        public TrainingModel Training => Install?.TrainingModel;
        public TrainingAgenda Agenda => Training?.TrainingAgenda;

        public IReadOnlyDictionary<string, string> DistrictList { get; protected set; } = new Dictionary<string, string>
        {
            { "Hk", "Hong Kong Island" },
            { "Kln", "Kowloon" },
            { "Nt", "New Territories" }
        };

        public IReadOnlyDictionary<string, string> AttendeeList { get; protected set; } = new Dictionary<string, string>
        {
            { "1-10", "1 - 10" },
            { "11+", "11+" }
        };

        public IReadOnlyDictionary<string, string> AttendeeFeedbackList { get; protected set; } = new Dictionary<string, string>
        {
            { "11+", "Please be reminded to leave enough space around the machine for training." }
        };

        public IReadOnlyDictionary<string, string> OperatingSystemList { get; protected set; } = new Dictionary<string, string>
        {
            { "windows", "Windows" },
            { "macOs", "macOS" },
            { "windows+macOS", "Windows + macOS" }
        };

        public string DateFormat => "dd / MM / yyyy";

        public virtual IActionResult OnGet()
        {
            var orderId = User.OrderId();
            Install = db.MachineInstallation
                .Include(x => x.TrainingModel)
                .ThenInclude(x => x.TrainingAgenda)
                .ThenInclude(x => x.TrainingAgendaMedia)
                .ThenInclude(x => x.Media)
                .SingleOrDefault(x => x.OrderId == orderId);

            if (Install == null)
            {
                var serialNo = User.SerialNo();
                _logger.LogInformation("Installation not found for serial no. {0} ({1})", serialNo, orderId);
                return NotFound();
            }

            // select list
            var zh = System.Globalization.CultureInfo.CurrentCulture.Name.StartsWith("zh", StringComparison.OrdinalIgnoreCase);
            DistrictList = db.GetDistrictList().ToDictionary(x => x.Id, x => x.Label);
            AttendeeList = db.GetAttendeeList(zh).ToDictionary(x => x.Id, x => x.Label);
            AttendeeFeedbackList = db.GetAttendeeList(zh).ToDictionary(x => x.Id, x => x.Note);
            OperatingSystemList = db.GetOperatingSystemList(zh).ToDictionary(x => x.Id, x => x.Label);

            var registration = db.Registration
                .Include(x => x.ScheduleItemSlot)
                .ThenInclude(x => x.ScheduleItem)
                .SingleOrDefault(x => x.OrderId == orderId);

            Register = new RegisterModel();
            Register.CompanyName = Install.CompanyName;

            if (registration != null)
            {
                if (registration.Status == RegistrationStatus.Submitted || registration.Status == RegistrationStatus.Rescheduled)
                    return RedirectToPage("/Reschedule/Preview");

                if (registration.Status == RegistrationStatus.Pending)
                {
                    registration.PopulateExcept(Register, "TrainingDate");

                    Register.TrainingDate = registration.TrainingDate?.ToString(DateFormat);

                    var slot = registration?.ScheduleItemSlot?.SingleOrDefault();
                    if (slot != null)
                    {
                        Register.District = slot.ScheduleItem.District;
                        Register.TrainingDate = slot.ScheduleItem.Date.ToString(DateFormat);
                        Register.TrainingTimeslot = slot.Timeslot;
                    }
                }
                else if (registration.Status == RegistrationStatus.Cancelled)
                {
                    Register.VenueAddress = Install.InstallAddress;
                    Register.District = DistrictList.Keys.FirstOrDefault(x => x.EqualsIgnoreCase(Install.InstallDistrict));
                    Register.CompanyName = Install.CompanyName;
                }
            }
            else
            {
                Register.VenueAddress = Install.InstallAddress;
                Register.District = DistrictList.Keys.FirstOrDefault(x => x.EqualsIgnoreCase(Install.InstallDistrict));
                Register.CompanyName = Install.CompanyName;
                //Register.FirstName = Install.FirstName;
                //Register.LastName = Install.LastName;
                //Register.PhoneNo = Install.PhoneNo;
                //Register.Email = Install.Email;
            }

            // value display
            Register.DistrictDisplay = Register.District != null && DistrictList.ContainsKey(Register.District) ? Localize(DistrictList[Register.District]) : null;
            Register.AttendeesDisplay = Register.Attendees != null && AttendeeList.ContainsKey(Register.Attendees) ? Localize(AttendeeList[Register.Attendees]) : null;
            Register.OperatingSystemDisplay = Register.OperatingSystem != null && OperatingSystemList.ContainsKey(Register.OperatingSystem) ? Localize(OperatingSystemList[Register.OperatingSystem]) : null;

            Session = registration?.Session;
            return Page();
        }

        async public virtual Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
                return SubmissionFail(ModelStateError);

            var success = await ValidateRecaptchaAsync();
            if (!success)
                return Fail(Localize(Resources.Message.InvalidRecaptcha));

            DateTime? trainingDate = null;
            if (!string.IsNullOrEmpty(Register.TrainingDate))
            {
                if (!DateTime.TryParseExact(Register.TrainingDate, DateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime d))
                    return SubmissionFail(Localize("Invalid {0}.", Localize("Training Date")));

                trainingDate = d;
            }

            if (string.IsNullOrEmpty(Register.TrainingTimeslot))
            {
                if (Register.TrainingDatePreferable != true || string.IsNullOrWhiteSpace(Register.TrainingDateRemarks))
                    return SubmissionFail(Localize(Resources.Message.RegistrationInputPreferredTimeslot));
            }

            var orderId = User.OrderId();
            var install = db.MachineInstallation.Include(x => x.TrainingModel).SingleOrDefault(x => x.OrderId == orderId);
            var modelId = install?.ModelId;
            var items = db.GetTodayScheduleItems(modelId, Register.District, out DateTime startDate, out DateTime endDate);

            if (!string.IsNullOrEmpty(Register.TrainingTimeslot))
            {
                var slots = items
                    .Where(x => trainingDate == x.Date)
                    .SelectMany(x => x.ScheduleItemSlot.Where(s => s.IsCustom == x.IsUseCustom))
                    .Where(x => x.Timeslot == Register.TrainingTimeslot)
                    .ToList();

                if (!slots.Any())
                    return SubmissionFail(Localize(Resources.Message.RegistrationTimeslotNotExist), new { reloadTimeslot = true });

                if (!slots.Any(s => s.RegistrationId == null || s.Registration.OrderId == orderId))
                    return SubmissionFail(Localize(Resources.Message.RegistrationTimeslotNotAvailable), new { reloadTimeslot = true });
            }

            var registration = db.Registration.SingleOrDefault(x => x.OrderId == orderId);
            if (registration == null)
            {
                registration = new Database.Registration
                {
                    OrderId = orderId,
                    SerialNo = User.SerialNo(),
                    Status = RegistrationStatus.Pending
                };
                db.Registration.Add(registration);
            }

            Register.PopulateExcept(registration, "TrainingDate");
            registration.TrainingDate = trainingDate;
            registration.Status = RegistrationStatus.Pending;
            registration.Session = Guid.NewGuid().ToString();
            db.SaveChanges();
            return Ok(redirect: Url.Page("/Registration/Preview"));
        }

        protected IActionResult SubmissionFail(string message, object data = null)
        {
            _logger.LogInformation("Registration - Submission fail - #{0}: {1}", User.SerialNo(), message);
            /*return Ok(
                redirect: Url.Page("/Registration/Fail"),
                redirectType: "replace"
            );*/
            return Fail(
                message: message,
                data: data
            );
        }
    }
}