﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.Pages.Registration
{
    public class SuccessModel : PageModel
    {
        private readonly RicohDB db;
        private readonly Authorization auth;

        public SuccessModel(RicohDB db, Authorization auth)
        {
            this.db = db;
            this.auth = auth;
        }

        public string Email { get; private set; }
        public bool HasTimeslot { get; private set; }

        public IActionResult OnGet()
        {
            var orderId = User.OrderId();
            var registration = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId);
            if (registration?.Status != RegistrationStatus.Submitted)
                return RedirectToPage("/Registration/Index");

            Email = registration?.Email;
            HasTimeslot = registration?.ScheduleTimeslot != null;
            return Page();
        }

        async public Task<IActionResult> OnPostAsync()
        {
            await auth.SignOutAsync();
            return RedirectToPage("/Index");
        }
    }
}