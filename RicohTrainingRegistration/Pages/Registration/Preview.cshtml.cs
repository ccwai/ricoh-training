﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.Pages.Registration
{
    public class PreviewModel : IndexModel
    {
        public PreviewModel(ILogger<PreviewModel> logger, RicohDB db) : base(logger, db)
        {
        }

        async public override Task<IActionResult> OnPost()
        {
            var index = Url.Page("/Registration/Index");
            var replace = "replace";

            var orderId = User.OrderId();
            var registration = await db.Registration.SingleOrDefaultAsync(x => x.OrderId == orderId);

            // check session
            if (registration?.Session != null && registration.Session != Session)
                return Fail(
                    message: Localize("The submission is expired. Please reload the page.")
                );

            if (registration?.Status == RegistrationStatus.Pending || registration?.Status == RegistrationStatus.Cancelled)
            {
                // check timeslot
                int? slotId = null;
                if (string.IsNullOrEmpty(registration.TrainingTimeslot))
                {
                    if (registration.TrainingDatePreferable != true || string.IsNullOrWhiteSpace(registration.TrainingDateRemarks))
                        return Fail(
                            message: Localize(Resources.Message.RegistrationInputPreferredTimeslot),
                            redirect: index,
                            redirectType: replace
                        );
                }
                else
                {
                    // set timeslot
                    var district = registration.District;
                    var date = registration.TrainingDate;
                    var timeslot = registration.TrainingTimeslot;
                    var slots = db.ScheduleItemSlot
                        .Where(x => x.Timeslot == timeslot && x.ScheduleItem.Date == date && x.ScheduleItem.District == district)
                        .Where(x => x.IsCustom == x.ScheduleItem.IsUseCustom)
                        .OrderBy(x => x.Seq)
                        .ToList();

                    var regId = registration.Id;
                    Func<ScheduleItemSlot, bool> condition = x => x.RegistrationId == null || x.RegistrationId == regId;
                    if (!slots.Any(condition))
                        return Fail(
                            message: Localize(Resources.Message.RegistrationTimeslotNotAvailable),
                            redirect: index,
                            redirectType: replace
                        );

                    // occupy the timeslot
                    var slot = slots.FirstOrDefault(condition);
                    slot.RegistrationId = regId;
                    slotId = slot.Id;
                }

                // release slots
                db.ScheduleItemSlot.RemoveRange(
                    db.ScheduleItemSlot.Where(x => x.Registration.OrderId == orderId && x.Id != slotId).ToList()
                );

                registration.SubmissionDate = DateTime.Now;
                registration.Status = RegistrationStatus.Submitted;

                db.SaveChanges();

                // send email
                var id = registration.Id;
                var r = db.RegistrationTimeslot.Find(id);
                var sn = SecurityHelper.Encrypt(RegistrationPassphrase.Reschedule, r.SerialNo);
                var zh = System.Globalization.CultureInfo.CurrentCulture.Name.StartsWith("zh", StringComparison.OrdinalIgnoreCase);
                var skip = r.TrainingDatePreferable == true && r.ScheduleTimeslot == null;
                db.AddEmailRequest(
                    EmailTemplateId.REG_CONFIRM,
                    new
                    {
                        CUSTOMER_NAME = r.CompanyName,
                        CONTACT_NAME = r.ContactName,
                        PHONE_NO = r.PhoneNo,
                        MODEL_NO = r.ModelId,
                        SERIAL_NO = r.SerialNo,
                        TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", r.ScheduleDate ?? r.TrainingDate),
                        TRAINING_TIMESLOT = r.ScheduleTimeslot ?? r.TrainingTimeslot,
                        VENUE_ADDRESS = r.VenueAddress?.Trim(),
                        LOGIN_URL = Url.Action("InviteReschedule", "Home", new { sn }, Request.Scheme, Request.Host.Value),
                        DISTRICT = Localize(db.GetDistrictList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.ScheduleDistrict ?? r.District))?.Label),
                        NO_SUITABLE_CHOICE = r.TrainingDatePreferable == true ? "Y" : "N",
                        NO_SUITABLE_CHOICE_REMARK = string.IsNullOrWhiteSpace(r.TrainingDateRemarks) ? Localize("-") : r.TrainingDateRemarks,
                        ATTENDEES = db.GetAttendeeList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Label,
                        ATTENDEES_NOTE = db.GetAttendeeList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Note,
                        OS = db.GetOperatingSystemList(zh).FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.OperatingSystem))?.Label,
                        COMPANY_NAME = r.CompanyName,
                        EMAIL = r.Email,
                        REMARKS = string.IsNullOrWhiteSpace(r.Remarks) ? Localize("-") : r.Remarks
                    },
                    skip ? null : r.Email,
                    skip ? null : r.ContactName
                );
                db.SaveChanges();

                // enqueue reminder email
                if (!skip)
                {
                    db.EnqueueTraningReminder(r);
                    db.SaveChanges();
                }

                return Ok(redirect: Url.Page("/Registration/Success"));
            }

            return Fail(redirect: index, redirectType: replace);
        }
    }
}