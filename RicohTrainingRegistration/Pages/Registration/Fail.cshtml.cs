﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RicohTrainingRegistration.Pages.Registration
{
    public class FailModel : PageModel
    {
        public string Description { get; private set; } = "Sorry! System busy. Please try again later.";

        public void OnGet(int? reason = null)
        {
            if (reason.HasValue)
            {
                switch (reason.Value)
                {
                    case 1: Description = Resources.Message.RegistrationInputPreferredTimeslot; break;
                    case 2: Description = Resources.Message.RegistrationTimeslotNotAvailable; break;
                }
            }
        }
    }
}