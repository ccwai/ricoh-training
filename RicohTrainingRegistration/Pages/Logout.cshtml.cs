﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RicohTrainingRegistration.Pages
{
    [AllowAnonymous]
    public class LogoutModel : PageModel
    {
        private readonly Authorization auth;

        public LogoutModel(Authorization auth)
        {
            this.auth = auth;
        }

        public Task OnGet()
        {
            return auth.SignOutAsync();
        }
    }
}