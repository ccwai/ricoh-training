﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages.Reschedule
{
    public class CancelModel : BasePageModel
    {
        private readonly ILogger<CancelModel> logger;
        private readonly RicohDB db;
        private readonly Authorization auth;

        public CancelModel(ILogger<CancelModel> logger, RicohDB db, Authorization auth)
        {
            this.logger = logger;
            this.db = db;
            this.auth = auth;
        }

        public string Email { get; private set; }

        public IActionResult OnGet()
        {
            var orderId = User.OrderId();
            var registration = db.Registration.SingleOrDefault(x => x.OrderId == orderId);
            if (registration?.Status != RegistrationStatus.Cancelled)
                return RedirectToPage("/Registration/Index");

            Email = registration?.Email;
            return Page();
        }

        async public Task<IActionResult> OnPostAsync()
        {
            await auth.SignOutAsync();
            return RedirectToPage("/Index");
        }
    }
}