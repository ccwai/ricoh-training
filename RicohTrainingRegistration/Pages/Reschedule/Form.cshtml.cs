﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Models;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages.Reschedule
{
    public class FormModel : BasePageModel
    {
        protected readonly ILogger<FormModel> _logger;
        protected readonly RicohDB db;

        public FormModel(ILogger<FormModel> logger, RicohDB db)
        {
            _logger = logger;
            this.db = db;
        }

        [BindProperty]
        public Models.RescheduleModel Register { get; set; }

        [BindProperty]
        public string Session { get; set; }

        public MachineInstallation Install { get; private set; }
        public TrainingModel Training => Install?.TrainingModel;
        public TrainingAgenda Agenda => Training?.TrainingAgenda;


        public bool IsLanding { get; private set; } = true;
        public string OriginalPhoneNo { get; private set; }
        public string OriginalEmail { get; private set; }


        public IReadOnlyDictionary<string, string> DistrictList { get; protected set; } = new Dictionary<string, string>
        {
            { "Hk", "Hong Kong Island" },
            { "Kln", "Kowloon" },
            { "Nt", "New Territories" }
        };

        public IReadOnlyDictionary<string, string> AttendeeList { get; protected set; } = new Dictionary<string, string>
        {
            { "1-10", "1 - 10" },
            { "11+", "11+" }
        };

        public IReadOnlyDictionary<string, string> AttendeeFeedbackList { get; protected set; } = new Dictionary<string, string>
        {
            { "11+", "Please be reminded to leave enough space around the machine for training." }
        };

        public IReadOnlyDictionary<string, string> OperatingSystemList { get; protected set; } = new Dictionary<string, string>
        {
            { "windows", "Windows" },
            { "macOs", "macOS" },
            { "windows+macOS", "Windows + macOS" }
        };

        public string DateFormat => "dd / MM / yyyy";

        protected IActionResult PageLoad(string orderId, string serialNo)
        {
            Install = db.MachineInstallation
                .Include(x => x.TrainingModel)
                .ThenInclude(x => x.TrainingAgenda)
                .ThenInclude(x => x.TrainingAgendaMedia)
                .ThenInclude(x => x.Media)
                .SingleOrDefault(x => x.OrderId == orderId);

            if (Install == null)
            {
                _logger.LogInformation("Installation not found for serial no. {0} ({1})", serialNo, orderId);
                return NotFound();
            }

            var registration = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId);
            if (registration == null)
            {
                _logger.LogInformation("Registration not found for serial no. {0} ({1})", serialNo, orderId);
                return NotFound();
            }

            Register = new Models.RescheduleModel();

            if (registration.Status != RegistrationStatus.Submitted && registration.Status != RegistrationStatus.Rescheduled)
                return RedirectToPage("/Registration/Index");

            // default load registration values
            registration.PopulateExcept(Register, "District", "TrainingDate", "TrainingTimeslot");
            Register.District = registration.ScheduleDistrict ?? registration.District;
            Register.TrainingDate = (registration.ScheduleDate ?? registration.TrainingDate)?.ToString(DateFormat);
            Register.TrainingTimeslot = registration.ScheduleTimeslot ?? registration.TrainingTimeslot;
            OriginalPhoneNo = registration.PhoneNo;
            OriginalEmail = registration.Email;

            // load reschedule values (if have saved before)
            if (!IsLanding)
            {
                var reschedule = db.Reschedule.SingleOrDefault(x => x.OrderId == orderId);
                if (reschedule != null)
                {
                    reschedule.PopulateExcept(Register, "TrainingDate");
                    Register.TrainingDate = reschedule.TrainingDate?.ToString(DateFormat);
                    Session = reschedule.Session;
                }
                else
                    return RedirectToPage("/Reschedule/Preview", new { display = "" });
            }

            var zh = System.Globalization.CultureInfo.CurrentCulture.Name.StartsWith("zh", StringComparison.OrdinalIgnoreCase);
            DistrictList = db.GetDistrictList().ToDictionary(x => x.Id, x => x.Label);
            AttendeeList = db.GetAttendeeList(zh).ToDictionary(x => x.Id, x => x.Label);
            AttendeeFeedbackList = db.GetAttendeeList(zh).ToDictionary(x => x.Id, x => x.Note);
            OperatingSystemList = db.GetOperatingSystemList(zh).ToDictionary(x => x.Id, x => x.Label);

            // value display
            Register.DistrictDisplay = Register.District != null && DistrictList.ContainsKey(Register.District) ? Localize(DistrictList[Register.District]) : null;
            Register.AttendeesDisplay = Register.Attendees != null && AttendeeList.ContainsKey(Register.Attendees) ? Localize(AttendeeList[Register.Attendees]) : null;
            Register.OperatingSystemDisplay = Register.OperatingSystem != null && OperatingSystemList.ContainsKey(Register.OperatingSystem) ? Localize(OperatingSystemList[Register.OperatingSystem]) : null;

            return Page();
        }

        public virtual IActionResult OnGet(string display = "")
        {
            IsLanding = !display.EqualsIgnoreCase("revise");

            var orderId = User.OrderId();
            var serialNo = User.SerialNo();
            return PageLoad(orderId, serialNo);
        }

        async public virtual Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
                return Fail(ModelStateError);

            var success = await ValidateRecaptchaAsync();
            if (!success)
                return Fail(Resources.Message.InvalidRecaptcha);

            DateTime? trainingDate = null;
            if (!string.IsNullOrEmpty(Register.TrainingDate))
            {
                if (!DateTime.TryParseExact(Register.TrainingDate, DateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime d))
                    return Fail(Localize("Invalid {0}.", Localize("Training Date")));

                trainingDate = d;
            }

            if (string.IsNullOrEmpty(Register.TrainingTimeslot))
            {
                if (Register.TrainingDatePreferable != true || string.IsNullOrWhiteSpace(Register.TrainingDateRemarks))
                    return Fail(Localize(Resources.Message.RegistrationInputPreferredTimeslot));
            }

            var orderId = User.OrderId();
            var install = db.MachineInstallation.Include(x => x.TrainingModel).SingleOrDefault(x => x.OrderId == orderId);
            var modelId = install?.ModelId;

            var items = db.GetTodayScheduleItems(modelId, Register.District, out DateTime startDate, out DateTime endDate);

            if (!string.IsNullOrEmpty(Register.TrainingTimeslot))
            {
                var slots = items.Where(x => trainingDate == x.Date)
                    .SelectMany(x => x.ScheduleItemSlot.Where(s => s.IsCustom == x.IsUseCustom))
                    .Where(x => x.Timeslot == Register.TrainingTimeslot)
                    .ToList();

                if (!slots.Any())
                    return Fail(Localize(Resources.Message.RegistrationTimeslotNotExist), new { reloadTimeslot = true });

                if (!slots.Any(s => s.RegistrationId == null || s.Registration.OrderId == orderId))
                    return Fail(Localize(Resources.Message.RegistrationTimeslotNotAvailable), new { reloadTimeslot = true });
            }

            var registration = db.Registration.SingleOrDefault(x => x.OrderId == orderId);
            if (registration == null)
                return Fail(redirect: Url.Page("/Registration/Index"));

            var reschedule = db.Reschedule.SingleOrDefault(x => x.OrderId == orderId);
            if (reschedule == null)
            {
                reschedule = new Database.Reschedule
                {
                    OrderId = orderId,
                    SerialNo = User.SerialNo(),
                    Status = RegistrationStatus.Pending
                };
                db.Reschedule.Add(reschedule);
            }

            Register.PopulateExcept(reschedule, "TrainingDate");
            reschedule.TrainingDate = trainingDate;
            if (string.IsNullOrWhiteSpace(reschedule.PhoneNo))
                reschedule.PhoneNo = registration.PhoneNo;
            if (string.IsNullOrWhiteSpace(reschedule.Email))
                reschedule.Email = registration.Email;

            reschedule.Session = Guid.NewGuid().ToString();
            db.SaveChanges();
            return Ok(redirect: Url.Page("/Reschedule/Preview", new { display = "revise" }));
        }
    }
}