﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages.Reschedule
{
    [AllowAnonymous]
    public class FailModel : BasePageModel
    {
        protected readonly ILogger<IndexModel> _logger;
        protected readonly RicohDB db;

        public FailModel(ILogger<IndexModel> logger, RicohDB db)
        {
            _logger = logger;
            this.db = db;
        }

        public string Description { get; private set; }

        public IActionResult OnGet(string id, [FromQuery]string m = null)
        {
            Description = Localize("Sorry! System busy. Please try again later.");

            var install = db.MachineInstallation.LatestOrDefault(id);

            if (install == null)
                return NotFound();

            if (m != null && TempData[m] != null)
            {
                Description = (string)TempData[m];
                TempData[m] = Description;
            }

            return Page();
        }
    }
}