﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.Pages.Reschedule
{
    public class SuccessModel : PageModel
    {
        private readonly RicohDB db;
        private readonly Authorization auth;

        public SuccessModel(RicohDB db, Authorization auth)
        {
            this.db = db;
            this.auth = auth;
        }

        public string Email { get; private set; }

        public IActionResult OnGet()
        {
            var orderId = User.OrderId();
            var registration = db.Registration.SingleOrDefault(x => x.OrderId == orderId);
            if (registration?.Status != RegistrationStatus.Rescheduled)
                return RedirectToPage("/Reschedule/Preview");

            Email = registration?.Email;
            return Page();
        }

        async public Task<IActionResult> OnPostAsync()
        {
            await auth.SignOutAsync();
            return RedirectToPage("/Index");
        }
    }
}