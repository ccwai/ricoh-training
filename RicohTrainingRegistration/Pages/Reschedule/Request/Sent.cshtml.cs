﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.Pages.Reschedule.Request
{
    [AllowAnonymous]
    public class SentModel : PageModel
    {
        private readonly ILogger<SentModel> logger;
        private readonly RicohDB db;
        private readonly Authorization auth;

        public SentModel(ILogger<SentModel> logger, RicohDB db, Authorization auth)
        {
            this.logger = logger;
            this.db = db;
            this.auth = auth;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        async public Task<IActionResult> OnPostAsync()
        {
            await auth.SignOutAsync();
            return RedirectToPage("/Index");
        }
    }
}