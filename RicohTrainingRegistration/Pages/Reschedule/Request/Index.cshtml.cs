﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.Pages.Reschedule.Request
{
    [AllowAnonymous]
    public class IndexModel : PreviewModel
    {
        public IndexModel(ILogger<IndexModel> logger, RicohDB db) : base(logger, db)
        {
        }

        [BindProperty]
        public string Message { get; set; }

        public override IActionResult OnGet(string display = "")
        {
            try
            {
                var q = Request.Query["q"].ToString();
                var qs = System.Web.HttpUtility.ParseQueryString(SecurityHelper.Decrypt("RescheduleRequest", q));
                var orderId = qs["OrderId"];
                var serialNo = qs["serialNo"];

                IsRequest = true;
                return PageLoad(orderId, serialNo);
            }
            catch (Exception)
            {
                return RedirectToPage("/Reschedule");
            }
        }

        async public override Task<IActionResult> OnPost()
        {
            if (string.IsNullOrWhiteSpace(Message))
                return Fail();

            try
            {
                var q = Request.Query["q"].ToString();
                var qs = System.Web.HttpUtility.ParseQueryString(SecurityHelper.Decrypt("RescheduleRequest", q));
                var orderId = qs["OrderId"];
                var model = await db.RegistrationTimeslot.SingleAsync(x => x.OrderId == orderId);

                // send email
                db.AddEmailRequest(
                    EmailTemplateId.TRAINING_REQUEST,
                    new
                    {
                        CUSTOMER_NAME = model.CompanyName,
                        CONTACT_NAME = model.ContactName,
                        PHONE_NO = model.PhoneNo,
                        EMAIL = model.Email,
                        MODEL_NO = model.ModelId,
                        SERIAL_NO = model.SerialNo,
                        TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", model.ScheduleDate ?? model.TrainingDate),
                        TRAINING_TIMESLOT = model.ScheduleTimeslot ?? model.TrainingTimeslot,
                        VENUE_ADDRESS = model.VenueAddress,
                        MESSAGE = Message
                    },
                    null
                );
                db.SaveChanges();

                return Ok(
                    redirect: Url.Page("/Reschedule/Request/Sent"),
                    redirectType: "replace"
                );
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to request reschedule");
                return Fail(ex.Message);
            }
        }
    }
}