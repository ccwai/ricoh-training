﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Models;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages.Enquiry
{
    [AllowAnonymous]
    public class IndexModel : BasePageModel
    {
        protected readonly ILogger<IndexModel> _logger;
        protected readonly RicohDB db;
        private readonly IConfiguration config;

        public IndexModel(ILogger<IndexModel> logger, RicohDB db, IConfiguration config)
        {
            _logger = logger;
            this.db = db;
            this.config = config;
        }

        [BindProperty]
        public EnquiryModel Enquiry { get; set; }

        public MachineInstallation Install { get; private set; }
        public string Message { get; private set; }

        public IActionResult OnGet(string id, [FromQuery]string m = null)
        {
            var acceptedPaths = new List<string>
            {
                Url.Page("/Index"),
                Url.Page("/Reschedule"),
                Url.Page("/Enquiry/Index")
            };

            var cultures = config.GetSection("SupportedCultures").Get<string[]>() ?? Localization.LocalizationSetting.DefaultSupportedCultures.Select(x => x.Name).ToArray();
            foreach (var culture in cultures)
            {
                var values = new RouteValueDictionary(Request.RouteValues);
                values[Localization.LocalizationSetting.RouteDataName] = culture;
                acceptedPaths.Add(Url.Page("/Enquiry/Index", values));
            }

            var referrer = Request.GetTypedHeaders().Referer;
            var path = referrer?.LocalPath.TrimEnd('/');
            if (!acceptedPaths.Any(x => x.EqualsIgnoreCase(path)) && path?.StartsWith(Url.Action("Invite", "Home"), StringComparison.OrdinalIgnoreCase) != true)
                return RedirectToPage("/Index");

            Install = db.MachineInstallation.LatestOrDefault(id);

            if (Install == null)
                return NotFound();

            var sn = Install.SerialNo;
            var itemNo = Install.ItemNo;
            var reg = db.RegistrationTimeslot.FirstOrDefault(x => x.SerialNo == sn && x.ItemNo == itemNo);
            if (reg != null)
                Enquiry = reg.Populate(new EnquiryModel(), "CompanyName");
            else
                Enquiry = Install.Populate(new EnquiryModel(), "CompanyName");

            if (m != null && TempData[m] != null)
            {
                var json = (string)TempData[m];
                var obj = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(json, new
                {
                    message = "",
                    arguments = new object[] { }
                });
                Message = Localize(obj.message, obj.arguments);
                TempData[m] = json;
            }

            return Page();
        }

        public IActionResult OnPost(string id)
        {
            if (!ModelState.IsValid)
                return Fail(message: Localize(ModelStateError));

            Install = db.MachineInstallation.LatestOrDefault(id);
            if (Install == null)
                return Fail(Localize("The serial number entered could not be found"));

            var model = Enquiry.Populate(new EnquiryForm());
            model.OrderId = Install.OrderId;
            model.SerialNo = Install.SerialNo;
            model.RequestTime = DateTime.Now;
            model.RequestIp = HttpContext.Connection.RemoteIpAddress.ToString();
            if (Request.Headers.TryGetValue("User-Agent", out StringValues ua))
                model.UserAgent = ua;

            db.EnquiryForm.Add(model);
            db.SaveChanges();

            // send email
            db.AddEmailRequest(
                EmailTemplateId.ENQUIRY,
                new
                {
                    SERIAL_NO = model.SerialNo,
                    MODEL_NO = Install.ModelId,
                    COMPANY_NAME = model.CompanyName,
                    FIRST_NAME= model.FirstName,
                    LAST_NAME = model.LastName,
                    PHONE_NO = model.PhoneNo,
                    EMAIL = model.Email,
                    REMARK = model.Remarks
                },
                null
            );
            db.SaveChanges();

            TempData.Clear();
            return Ok(redirect: Url.Page("/Enquiry/Success"));
        }
    }
}