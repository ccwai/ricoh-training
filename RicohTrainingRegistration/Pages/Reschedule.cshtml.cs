﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages
{
    [AllowAnonymous]
    public class RescheduleModel : BasePageModel
    {
        private readonly ILogger<RescheduleModel> _logger;
        private readonly RicohDB db;
        private readonly SiteSettings settings;
        private readonly Authorization auth;

        public RescheduleModel(ILogger<RescheduleModel> logger, RicohDB db, SiteSettings settings, Authorization auth)
        {
            _logger = logger;
            this.db = db;
            this.settings = settings;
            this.auth = auth;
        }

        [Display(Name = "Serial no.")]
        [Required(ErrorMessage = "Please enter a valid serial number")]
        [BindProperty]
        public string SerialNo { get; set; }

        [Display(Name = "Phone no. / email")]
        [BindProperty]
        public string PhoneNoEmail { get; set; }

        [BindProperty]
        public string Forgot { get; set; }

        public string SerialNoHint { get; internal set; }
        public string SerialNoHintImage { get; internal set; }

        public IActionResult OnGet()
        {
            /*if (User.Identity.IsAuthenticated)
                return RedirectToPage("/Reschedule/Preview");*/

            SerialNoHint = HttpContext.GetCulture().TwoLetterISOLanguageName.StartsWith("zh", StringComparison.OrdinalIgnoreCase) ? settings.SerialNoHintChi : settings.SerialNoHintEng;
            SerialNoHint = SerialNoHint ?? Localize("You can find the Serial Number on the label");
            var hintMedia = db.GetSerialNoHintMedia();
            SerialNoHintImage = hintMedia != null ? Url.Action("Index", "Download", new { fileId = hintMedia.Id }) : Url.Content("~/assets/images/sn-info.png");

            return Page();
        }

        async public Task<IActionResult> OnPost()
        {
            _logger.LogDebug("Entered serial number: {0} | phone no. / email: {1}", SerialNo, PhoneNoEmail);

            if (!ModelState.IsValid)
                return Fail(
                    message: Localize(ModelStateError),
                    redirect: Url.Page("/Index")
                );

            // CR: check phone no. / email
            PhoneNoEmail = PhoneNoEmail ?? "";
            var isForgot = Forgot == "1";
            var isPhoneNo = IsPhoneNo(PhoneNoEmail);
            var isEmail = IsEmail(PhoneNoEmail);
            if (!isForgot)
            {
                if (string.IsNullOrEmpty(PhoneNoEmail))
                    return Fail(
                        message: Localize("Please enter your registered phone no. / email address")
                    );
                if (!isPhoneNo && !isEmail)
                    return Fail(
                        message: Localize("Please enter a correct phone number or email address")
                    );
            }

            var success = await ValidateRecaptchaAsync();
            if (!success)
                return Fail(
                    message: Localize(Resources.Message.InvalidRecaptcha)
                );

            var install = db.MachineInstallation.Include(x => x.MachineInstallationDate).LatestOrDefault(SerialNo);

            // 1. sn is valid
            if (install == null)
                return Fail(Localize("Please enter a valid serial number"));

            // 1. sn is registed
            var orderId = install.OrderId;
            var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
            var reg = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId && status.Contains(x.Status));
            if (reg == null)
                return Fail(Localize(Resources.Message.TrainingNotRegistered));

            // 1. sn is provided
            var provided = settings.CompletedTrainingProblemCodes.ToList();
            if (provided.Contains(install.ProblemCode))
                return Fail(Localize(Resources.Message.TrainingProvidedShort, string.Format("{0:yyyy/MM/dd}", install.LastChangeTime)));

            // CR: forgot phone no. / email
            if (isForgot)
                return Ok(redirect: Url.Page("/Enquiry/Index", new { id = reg.SerialNo }));

            // CR: check phone no. / email
            if (!PhoneNoEmail.Equals(reg.PhoneNo, StringComparison.OrdinalIgnoreCase) && !PhoneNoEmail.Equals(reg.Email, StringComparison.OrdinalIgnoreCase))
            {
                var message = isPhoneNo ? "Registered phone no. incorrect. Please try again." : "Registered email incorrect. Please try again.";
                return Fail(
                    message: Localize(message)
                );
            }

            // training time
            var timeslot = reg.ScheduleTimeslot?.Split('-').FirstOrDefault().Trim();
            var trainingTime = string.Format("{0:yyyy-MM-dd} {1}", reg.ScheduleDate, timeslot);
            if (!DateTime.TryParseExact(trainingTime, "yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime date))
                return Fail(Localize("Training timeslot is not found."));

            // 2. with/without cutoff
            var now = DateTime.Now;
            if (now > date)
                return Fail(Localize("Training is completed"));

            if (settings.RescheduleHasCutoff)
            {
                var cutoff = db.GetRescheduleCutoffDate(date.Date);
                if (now >= cutoff)
                {
                    var qs = System.Web.HttpUtility.ParseQueryString("");
                    qs["OrderId"] = install.OrderId;
                    qs["SerialNo"] = install.SerialNo;
                    var q = DotNetLib.SecurityHelper.Encrypt("RescheduleRequest", qs.ToString());
                    return Ok(
                        redirect: Url.Page("/Reschedule/Request/Index", new { q })
                    );
                }
            }

            // signin system
            await auth.SignInAsync(install);

            return Ok(redirect: Url.Page("/Reschedule/Preview"));
        }

        static bool IsPhoneNo(string phoneNo)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(phoneNo, @"^\d{8,}$");
        }

        static bool IsEmail(string email)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email) != null;
            }
            catch (Exception)
            {

            }
            return false;
        }
    }
}
