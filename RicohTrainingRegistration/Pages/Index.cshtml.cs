﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Mvc;

namespace RicohTrainingRegistration.Pages
{
    [AllowAnonymous]
    public class IndexModel : BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly RicohDB db;
        private readonly SiteSettings settings;
        private readonly Authorization auth;

        public IndexModel(ILogger<IndexModel> logger, RicohDB db, SiteSettings settings, Authorization auth)
        {
            _logger = logger;
            this.db = db;
            this.settings = settings;
            this.auth = auth;
        }

        [Display(Name = "Serial no.")]
        [Required]
        [BindProperty]
        public string SerialNo { get; set; }

        public string SerialNoHint { get; internal set; }
        public string SerialNoHintImage { get; internal set; }

        public IActionResult OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                /*var orderId = User.OrderId();
                var reg = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId);
                if (reg != null)
                {
                    if (reg.Status == RegistrationStatus.Pending)
                        return RedirectToPage("/Registration/Preview");
                    if (reg.Status == RegistrationStatus.Submitted)
                        return RedirectToPage("/Registration/Success");
                    if (reg.Status == RegistrationStatus.Rescheduled)
                        return RedirectToPage("/Reschedule/Preview");
                    if (reg.Status == RegistrationStatus.Cancelled)
                        return RedirectToPage("/Registration/Index");
                }*/
            }

            SerialNoHint = HttpContext.GetCulture().TwoLetterISOLanguageName.StartsWith("zh", StringComparison.OrdinalIgnoreCase) ? settings.SerialNoHintChi : settings.SerialNoHintEng;
            SerialNoHint = SerialNoHint ?? Localize("You can find the Serial Number on the label");
            var hintMedia = db.GetSerialNoHintMedia();
            SerialNoHintImage = hintMedia != null ? Url.Action("Index", "Download", new { fileId = hintMedia.Id }) : Url.Content("~/assets/images/sn-info.png");

            return Page();
        }

        async public Task<IActionResult> OnPost()
        {
            _logger.LogDebug("Entered serial number: {0}", SerialNo);

            if (!ModelState.IsValid)
                return Fail(
                    message: Localize(ModelStateError),
                    redirect: Url.Page("/Index")
                );

            var success = await ValidateRecaptchaAsync();
            if (!success)
                return Fail(
                    message: Localize(Resources.Message.InvalidRecaptcha)
                );

            var install = db.MachineInstallation.Include(x => x.MachineInstallationDate).LatestOrDefault(SerialNo);

            // 1. sn is valid
            if (install == null)
                return Fail(Localize(Resources.Message.SerialNoNotFound));

            // 2. model is entitled
            if (install.MachineInstallationDate?.IsFreeTraining != true)
                return EnquiryResult(Resources.Message.ModelIsNotEntitled);

            // 3. sn is registered
            var orderId = install.OrderId;
            var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
            var reg = db.RegistrationTimeslot.SingleOrDefault(x => x.OrderId == orderId && status.Contains(x.Status));
            if (reg != null)
                return EnquiryResult(Resources.Message.TrainingRegistered, string.Format("{0:yyyy/MM/dd}", reg.SubmissionDate));

            // 3. sn is provided
            var provided = settings.CompletedTrainingProblemCodes.ToList();
            if (provided.Contains(install.ProblemCode))
                return EnquiryResult(Resources.Message.TrainingProvided, string.Format("{0:yyyy/MM/dd}", install.LastChangeTime));

            // 4. within free training period
            var installDate = install.MachineInstallationDate;
            var today = DateTime.Today;
            if (!(installDate?.FreeTrainingEndDate >= today))
                return EnquiryResult(Resources.Message.NotInRegistrationPeriod);

            // signin system
            await auth.SignInAsync(install);

            return Ok(redirect: Url.Page("/Registration/Index"));
        }

        private IActionResult EnquiryResult(string message, params object[] arguments)
        {
            var m = Guid.NewGuid().ToString("N");
            TempData[m] = Newtonsoft.Json.JsonConvert.SerializeObject(new { message, arguments });
            return Ok(redirect: Url.Page("/Enquiry/Index", new { id = SerialNo, m }));
        }
    }
}
