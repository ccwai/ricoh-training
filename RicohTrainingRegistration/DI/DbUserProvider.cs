﻿using Microsoft.AspNetCore.Http;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.DI
{
    public class DbUserProvider : IDbUserProvider
    {
        public DbUserProvider(IHttpContextAccessor httpContextAccessor)
        {
            HttpContext = httpContextAccessor.HttpContext;
        }
        public HttpContext HttpContext { get; }

        public string GetId()
        {
            return HttpContext?.User?.SerialNo();
        }

        public string GetName()
        {
            return HttpContext?.User?.SerialNo();
        }
    }
}
