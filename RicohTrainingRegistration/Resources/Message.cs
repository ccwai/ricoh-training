﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Resources
{
    public static class Message
    {
        public static readonly string SerialNoNotFound = "The serial number entered could not be found";
        public static readonly string ModelIsNotEntitled = "Thank you for your enquiry. Please be informed that a quotation will be provided for the machine. Please leave your contact. We will contact you shortly.";
        public static readonly string TrainingRegistered = "Thank you for your enquiry. This serial number has been registered for the training on {0}. If you need additional training, please leave your contact and we will contact you for the arrangement soon.";
        public static readonly string TrainingProvided = "Thank you for your enquiry. This serial number has been provided for the training on {0}. If you need additional training, please leave your contact and we will contact you for the arrangement soon.";
        public static readonly string NotInRegistrationPeriod = "Thank you for your enquiry. Please be informed that a quotation will be provided for the machine. Please leave your contact. We will contact you shortly.";
        public static readonly string TrainingNotRegistered = "Registration could not be found.";
        public static readonly string TrainingProvidedShort = "Training is already provided on {0}.";

        public static readonly string RegistrationInputPreferredTimeslot = "Please write down your preferred timeslot if none of the choice is suitable.";
        public static readonly string RegistrationTimeslotNotExist = "The selected timeslot does not exist.";
        public static readonly string RegistrationTimeslotNotAvailable = "The selected session is full, please select another session.";

        public const string InvalidRecaptcha = "Google reCAPTCHA verification failed";
    }
}
