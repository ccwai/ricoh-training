﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration
{
    public class SiteSettings
    {
        private readonly IServiceProvider services;
        private SystemSettings settings;

        public SiteSettings(IServiceProvider services)
        {
            this.services = services;
        }

        private SystemSettings GetSystemSettings()
        {
            if (settings == null)
            {
                var db = services.GetRequiredService<RicohDB>();
                settings = db.GetDefaultSettings();
            }

            return settings;
        }

        public IEnumerable<string> CompletedInstallationProblemCodes => GetSystemSettings().CompletedInstallationProblemCodes;
        public IEnumerable<string> CompletedTrainingProblemCodes => GetSystemSettings().CompletedTrainingProblemCodes;
        public int OpenedDayForRegistration => GetSystemSettings().OpenedDayForRegistration ?? 0;
        public int FreeTrainingPeriod => GetSystemSettings().FreeTrainingPeriod ?? 0;
        public int RegistrationDeadlineDay => GetSystemSettings().RegistrationDeadlineDay ?? 0;
        public int RegistrationDeadlineHour => GetSystemSettings().RegistrationDeadlineHour ?? 0;
        public int RegistrationDeadlineMinute => GetSystemSettings().RegistrationDeadlineMinute ?? 0;
        public bool RescheduleHasCutoff => GetSystemSettings().RescheduleHasCutoff == true;
        public List<FooterLink> FooterLinks => GetSystemSettings().FooterLinks.ToList();
        public string SerialNoHintEng => GetSystemSettings().SerialNoHintEng;
        public string SerialNoHintChi => GetSystemSettings().SerialNoHintChi;
    }
}
