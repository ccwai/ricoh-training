﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Models
{
    public class RescheduleModel : RegisterModel
    {
        [Display(Name = "Phone No.")]
        [Phone(ErrorMessage = "Invalid {0}.")]
        [RegularExpression(@"^[\d]{8}$", ErrorMessage = "Invalid {0}.")]
        new public string PhoneNo { get; set; }

        [Display(Name = "Email Address")]
        [EmailAddress(ErrorMessage = "Invalid {0}.")]
        [StringLength(200)]
        new public string Email { get; set; }
    }
}
