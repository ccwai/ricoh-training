﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Models
{
    public class RegisterModel
    {
        [Display(Name = "Training Venue")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [StringLength(200)]
        public string VenueAddress { get; set; }

        [Display(Name = "District")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string DistrictDisplay { get; set; }

        [Display(Name = "District")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string District { get; set; }

        [Display(Name = "Training Date")]
        //[Required(ErrorMessage = "Please enter {0}.")]
        public string TrainingDate { get; set; }

        [Display(Name = "Preferred Timeslot")]
        public string TrainingTimeslot { get; set; }

        public bool? TrainingDatePreferable { get; set; }

        [Display(Name = "Preferred timeslot")]
        [StringLength(200)]
        public string TrainingDateRemarks { get; set; }

        [Display(Name = "No. of Attendees")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string AttendeesDisplay { get; set; }

        [Display(Name = "No. of Attendees")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string Attendees { get; set; }

        [Display(Name = "Operating System")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string OperatingSystemDisplay { get; set; }

        [Display(Name = "Operating System")]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string OperatingSystem { get; set; }

        [Display(Name = "Company Name")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [StringLength(100)]
        public string CompanyName { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Display(Name = "Phone No.")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [Phone(ErrorMessage = "Invalid {0}.")]
        [RegularExpression(@"^[\d]{8}$", ErrorMessage = "Invalid {0}.")]
        public string PhoneNo { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [EmailAddress(ErrorMessage = "Invalid {0}.")]
        [StringLength(200)]
        public string Email { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }
    }
}
