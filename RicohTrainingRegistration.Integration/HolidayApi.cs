﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RicohTrainingRegistration.Integration.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.Integration
{
    public class HolidayApi
    {
        private readonly string HkHolidayApi;

        public HolidayApi(IConfiguration configuration)
        {
            Configuration = configuration;
            HkHolidayApi = Configuration.GetValue("AppSettings:HkHolidayApi", "http://www.1823.gov.hk/common/ical/en.json");
        }

        public IConfiguration Configuration { get; }

        public async Task<List<HkHolidayModel>> GetHkHolidays()
        {
            using (var client = new HttpClient())
            {
                var res = await client.GetAsync(HkHolidayApi);
                res.EnsureSuccessStatusCode();
                
                var json = await res.Content.ReadAsStringAsync();
                var data = JToken.Parse(json);
                var evts = data["vcalendar"][0]["vevent"].AsEnumerable().ToList();
                var culture = CultureInfo.GetCultureInfo("en-US");
                var list = new List<HkHolidayModel>();
                foreach (var evt in evts)
                {
                    DateTime dtStart, dtEnd;
                    if (DateTime.TryParseExact((string)evt["dtstart"][0], "yyyyMMdd", culture, DateTimeStyles.None, out dtStart)
                        && DateTime.TryParseExact((string)evt["dtend"][0], "yyyyMMdd", culture, DateTimeStyles.None, out dtEnd))
                    {
                        var item = new HkHolidayModel();
                        item.StartDate = dtStart;
                        item.EndDate = dtEnd.AddDays(-1);
                        item.Name = (string)evt["summary"];
                        list.Add(item);
                    }
                }
                return list;
            }
        }
    }
}
