﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RicohTrainingRegistration.Integration.Models
{
    public class HkHolidayModel
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
