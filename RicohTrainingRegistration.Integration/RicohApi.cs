﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using RicohTrainingRegistration.Integration.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RicohTrainingRegistration.Integration
{
    public class RicohApi
    {
        private readonly IConfiguration configuration;
        private readonly MachineTrainingInfo db;

        public RicohApi(IConfiguration configuration)
        {
            this.configuration = configuration;

            var conn = configuration.GetConnectionString("MachineTrainingInfoContext");
            this.db = new MachineTrainingInfo(conn);
        }

        public List<ModelData> GetModels()
        {
            return db.ModelData.ToList();
        }

        public List<ServiceCall> GetServiceCalls()
        {
            return GetServiceCalls(null, null);
        }

        public List<ServiceCall> GetServiceCalls(DateTime? from, DateTime? to)
        {
            var query = db.ServiceCall.AsQueryable();
            if (from != null)
                query = query.Where(x => EF.Functions.DateDiffDay(from, x.ImportTime) >= 0);
            if (to != null)
                query = query.Where(x => EF.Functions.DateDiffDay(x.ImportTime, to) >= 0);

            return query.ToList();
        }
    }
}
