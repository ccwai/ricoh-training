﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace RicohTrainingRegistration.Integration.Data
{
    public partial class MachineTrainingInfo : DbContext
    {
        private readonly string connectionString;

        public MachineTrainingInfo()
        {
        }

        public MachineTrainingInfo(DbContextOptions<MachineTrainingInfo> options)
            : base(options)
        {
        }

        public MachineTrainingInfo(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public virtual DbSet<ModelData> ModelData { get; set; }
        public virtual DbSet<ServiceCall> ServiceCall { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(this.connectionString ?? "Server=www.wise-studio.com,51433;Database=MachineTrainingInfo;user id=ricoh;password=ricoh;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ModelData>(entity =>
            {
                entity.HasKey(e => e.ModelId);

                entity.ToTable("model_data");

                entity.Property(e => e.ModelId)
                    .HasColumnName("model_id")
                    .HasMaxLength(30);

                entity.Property(e => e.ImportTime)
                    .HasColumnName("import_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModelName)
                    .IsRequired()
                    .HasColumnName("model_name")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<ServiceCall>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.ToTable("service_call");

                entity.Property(e => e.OrderId)
                    .HasColumnName("order_id")
                    .HasMaxLength(40);

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasColumnName("company_name")
                    .HasMaxLength(60);

                entity.Property(e => e.CustNo)
                    .IsRequired()
                    .HasColumnName("cust_no")
                    .HasMaxLength(30);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(254);

                entity.Property(e => e.ErpId)
                    .HasColumnName("erp_id")
                    .HasMaxLength(40);

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(40);

                entity.Property(e => e.ImportTime)
                    .HasColumnName("import_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.InstallAddress)
                    .IsRequired()
                    .HasColumnName("install_address")
                    .HasMaxLength(254);

                entity.Property(e => e.InstallDistrict)
                    .HasColumnName("install_district")
                    .HasMaxLength(30);

                entity.Property(e => e.ItemNo)
                    .IsRequired()
                    .HasColumnName("item_no")
                    .HasMaxLength(50);

                entity.Property(e => e.LastChangeTime)
                    .HasColumnName("last_change_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(40);

                entity.Property(e => e.ModelDesc)
                    .IsRequired()
                    .HasColumnName("model_desc")
                    .HasMaxLength(60);

                entity.Property(e => e.ModelId)
                    .IsRequired()
                    .HasColumnName("model_id")
                    .HasMaxLength(30);

                entity.Property(e => e.OpenTime)
                    .HasColumnName("open_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderStatId).HasColumnName("order_stat_id");

                entity.Property(e => e.PhoneNo)
                    .HasColumnName("phone")
                    .HasMaxLength(40);

                entity.Property(e => e.ProblemCode)
                    .IsRequired()
                    .HasColumnName("problem_code")
                    .HasMaxLength(30);

                entity.Property(e => e.RequestId)
                    .IsRequired()
                    .HasColumnName("request_id")
                    .HasMaxLength(30);

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasColumnName("serial_no")
                    .HasMaxLength(30);

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.ServiceCall)
                    .HasForeignKey(d => d.ModelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_service_call_model_data");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
