﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Integration.Data
{
    public partial class ServiceCall
    {
        public string OrderId { get; set; }
        public string SerialNo { get; set; }
        public string ItemNo { get; set; }
        public string ProblemCode { get; set; }
        public string Email { get; set; }
        public string ErpId { get; set; }
        public string RequestId { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime LastChangeTime { get; set; }
        public int OrderStatId { get; set; }
        public string ModelId { get; set; }
        public string ModelDesc { get; set; }
        public string CustNo { get; set; }
        public string CompanyName { get; set; }
        public string InstallAddress { get; set; }
        public string InstallDistrict { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public DateTime ImportTime { get; set; }

        public virtual ModelData Model { get; set; }
    }
}
