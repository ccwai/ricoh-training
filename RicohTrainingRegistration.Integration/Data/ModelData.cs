﻿using System;
using System.Collections.Generic;

namespace RicohTrainingRegistration.Integration.Data
{
    public partial class ModelData
    {
        public ModelData()
        {
            ServiceCall = new HashSet<ServiceCall>();
        }

        public string ModelId { get; set; }
        public string ModelName { get; set; }
        public DateTime ImportTime { get; set; }

        public virtual ICollection<ServiceCall> ServiceCall { get; set; }
    }
}
