﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetLib.Email
{
    public class EmailClient : IEmailClient
    {
        private readonly EmailClientConfig config;

        public EmailClient(EmailClientConfig config)
        {
            this.config = config;
        }

        async public Task<SendEmailResult> SendAsync(EmailEnvelope data)
        {
            try
            {
                if (data == null)
                    throw new ArgumentNullException("data");

                if (data.From?.Any() != true)
                    throw new Exception("Email sender not provided.");

                if (data.To?.Any() != true)
                    throw new Exception("Email recipient not provided.");

                var message = new MimeMessage();

                // from
                foreach (var fr in data.From)
                {
                    var addr = new MailboxAddress(fr.Address) { Name = fr.Name };
                    message.From.Add(addr);
                }

                // to
                foreach (var to in data.To)
                {
                    var addr = new MailboxAddress(to.Address) { Name = to.Name };
                    var list = message.To;
                    if (to is Recipient)
                    {
                        var type = ((Recipient)to).Type;
                        if (type == RecipientType.Cc)
                            list = message.Cc;
                        else if (type == RecipientType.Bcc)
                            list = message.Bcc;
                    }
                    list.Add(addr);
                }

                // reply to
                if (data.ReplyTo?.Any() == true)
                {
                    foreach (var to in data.ReplyTo)
                    {
                        var addr = new MailboxAddress(to.Address) { Name = to.Name };
                        var list = message.ReplyTo;
                        list.Add(addr);
                    }
                }

                // return path header
                if (data.ReturnPath?.Address != null)
                    message.Headers.Add(HeaderId.ReturnPath, data.ReturnPath.Address);

                // subject
                message.Subject = data.Subject;

                // body
                var bb = new BodyBuilder();
                bb.HtmlBody = data.Body;
                bb.TextBody = data.Text;
                message.Body = bb.ToMessageBody();

                // send
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    client.Connect(config.Host, config.Port, config.UseSsl);

                    if (!string.IsNullOrEmpty(config.UserName))
                        client.Authenticate(config.UserName, config.Password);

                    await client.SendAsync(message);

                    client.Disconnect(true);
                }

                return new SendEmailResult
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                return new SendEmailResult
                {
                    Success = false,
                    Error = ex
                };
            }
        }
    }
}
