﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Email
{
    public class EmailClientConfig
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool UseSsl { get; set; }
    }
}
