﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib.Email
{
    public class EmailEnvelope
    {
        public IEnumerable<EmailAddress> From { get; set; }
        public IEnumerable<EmailAddress> To { get; set; }
        public IEnumerable<EmailAddress> ReplyTo { get; set; }
        public EmailAddress ReturnPath { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Text { get; set; }
    }

    public enum RecipientType { Default, Cc, Bcc }

    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }

    public class Recipient : EmailAddress
    {
        public RecipientType Type { get; set; }
    }

    public class SendEmailResult
    {
        public bool Success { get; set; }
        public object Data { get; set; }
        public Exception Error { get; set; }
    }
}
