﻿using System;
using System.Threading.Tasks;

namespace DotNetLib.Email
{
    public interface IEmailClient
    {
        Task<SendEmailResult> SendAsync(EmailEnvelope data);
    }
}
