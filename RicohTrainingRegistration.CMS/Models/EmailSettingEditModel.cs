﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class EmailSettingEditModel
    {
        public List<string> TrainerAddresses { get; set; }

        [Display(Name = "From (Display)")]
        public string From { get; set; }
        
        [Display(Name = "From Address")]
        [EmailAddress]
        public string FromAddress { get; set; }

        public List<string> ReplyToAddresses { get; set; }

        [Display(Name = "Return Path")]
        [EmailAddress]
        public string ReturnAddress { get; set; }
    }
}
