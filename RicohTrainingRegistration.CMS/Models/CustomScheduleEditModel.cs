﻿using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class CustomScheduleEditModel
    {
        public int Id { get; set; }

        [Display(Name = "Date")]
        [Required]
        public DateTime? Date { get; set; }

        [Display(Name = "District")]
        [Required]
        public string District { get; set; }

        [Display(Name = "Default Timeslots")]
        public List<CustomScheduleSlotEditModel> DefaultTimeslots { get; internal set; }

        [Display(Name = "Custom Timeslots")]
        public List<CustomScheduleSlotEditModel> CustomTimeslots { get; set; }

        public bool? IsUseCustom { get; set; }

        [StringLength(200)]
        public string Remark { get; set; }
    }

    public class CustomScheduleSlotEditModel
    {
        [Required]
        public string Timeslot { get; set; }

        [Required]
        [Range(0, 99, ErrorMessage = "Invalid quota value")]
        public int Quota { get; set; }

        public List<DotNetLib.Datatable.DtRowAction> Registrations { get; set; }

        public string StartTime => string.Format("{0:HH:mm}", ScheduleItemSlot.ParseTime(Timeslot?.Split(" - ").FirstOrDefault()));
        public string EndTime => string.Format("{0:HH:mm}", ScheduleItemSlot.ParseTime(Timeslot?.Split(" - ").Skip(1).FirstOrDefault()));
    }
}
