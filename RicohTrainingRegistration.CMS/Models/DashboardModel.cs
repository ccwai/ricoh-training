﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class DashboardModel
    {
        public DateTime Date { get; set; }
        public int AvailableTimeslot { get; set; }
        public int InstallationEntitled { get; set; }
        public int SentInvitation { get; set; }

        public int RegistrationPreferred { get; set; }
        public int RegistrationScheduled { get; set; }
        public int Registration => RegistrationPreferred + RegistrationScheduled;

        public int TrainingProvided { get; set; }
        public int NotYetRegistered { get; set; }
        public int AvailableTimeslotLeft { get; set; }
    }
}
