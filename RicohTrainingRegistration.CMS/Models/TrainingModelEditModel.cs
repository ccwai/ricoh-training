﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class TrainingModelEditModel
    {
        public bool? IsFree { get; set; }

        [Display(Name = "Opened Days")]
        [Required]
        [Range(0, 730)]
        public int? OpenedDays { get; set; }
    }
}
