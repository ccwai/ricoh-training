﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class EmailTemplateEditModel
    {
        public string Id { get; set; }
        
        public string Name { get; set; }
        
        [Display(Name = "Subject (Eng)")]
        [StringLength(100)]
        public string SubjectEng { get; set; }
        
        [Display(Name = "Subject (Chi)")]
        [StringLength(100)]
        public string SubjectChi { get; set; }
        
        [Display(Name = "Content (Eng)")]
        [StringLength(int.MaxValue)]
        public string ContentEng { get; set; }

        [Display(Name = "Content (Chi)")]
        [StringLength(int.MaxValue)]
        public string ContentChi { get; set; }
        
        public bool? SendToTrainers { get; set; }
    }
}
