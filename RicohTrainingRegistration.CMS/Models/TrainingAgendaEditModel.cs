﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class TrainingAgendaEditModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        
        [Display(Name = "Content (Eng)")]
        [StringLength(1000)]
        public string ContentEng { get; set; }
        
        [Display(Name = "Content (Chi)")]
        [StringLength(1000)]
        public string ContentChi { get; set; }
        
        [Display(Name = "Url Name (Eng)")]
        [StringLength(500)]
        public string UrlLabelEng { get; set; }
        
        [Display(Name = "Url (Eng)")]
        [StringLength(500)]
        [Url]
        public string UrlEng { get; set; }

        [Display(Name = "Url Name (Chi)")]
        [StringLength(500)]
        public string UrlLabelChi { get; set; }
        
        [Display(Name = "Url (Chi)")]
        [StringLength(500)]
        [Url]
        public string UrlChi { get; set; }

        public List<string> Models { get; set; }

        public List<IFormFile> AttachmentsEng { get; set; }
        
        public List<IFormFile> AttachmentsChi { get; set; }

        public List<int> RemoveAttachments { get; set; }
    }
}
