﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class RegistrationFilter
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string VenueAddress { get; set; }
        public string District { get; set; }
        public string SerialNo { get; set; }
        public string ModelNo { get; set; }
        public string Attendees { get; set; }
        public string OperatingSystem { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string[] Status { get; set; }
    }
}
