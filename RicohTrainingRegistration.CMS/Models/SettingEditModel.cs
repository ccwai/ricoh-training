﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class SettingEditModel
    {
        public List<string> CompletedInstallationProblemCodes { get; set; }
        public List<string> CompletedTrainingProblemCodes { get; set; }

        public int? OpenedDayForRegistration { get; set; }
        public int? FreeTrainingPeriod { get; set; }
        public int? RegistrationDeadlineDay { get; set; }
        public int? RegistrationDeadlineHour { get; set; }
        public int? RegistrationDeadlineMinute { get; set; }
        public bool RescheduleHasCutoff { get; set; }

        public List<Database.DropDownListItem> Attendees { get; set; }
        public List<Database.DropDownListItem> OperatingSystems { get; set; }

        public string SerialNoHintEng { get; set; }
        public string SerialNoHintChi { get; set; }

        public List<Database.FooterLink> FooterLinks { get; set; }
    }
}
