﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class EnquiryFormFilter
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string SerialNo { get; set; }
        public string ModelNo { get; set; }
        public string Company { get; set; }
        public string Name { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string[] IsRead { get; set; }
    }
}
