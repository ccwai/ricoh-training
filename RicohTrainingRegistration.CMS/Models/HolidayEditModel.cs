﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class HolidayEditModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Display(Name = "Start Date")]
        [Required]
        public DateTime? StartDate { get; set; }
        
        [Display(Name = "End Date")]
        [Required]
        public DateTime? EndDate { get; set; }

        public string Type { get; internal set; } = Database.HolidayType.Manual;
    }
}
