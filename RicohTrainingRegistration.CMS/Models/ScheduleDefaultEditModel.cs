﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class ScheduleDefaultEditModel
    {
        public int DayOfWeek { get; set; }
        public string District { get; set; }
        public int? Quota { get; set; }
    }
}
