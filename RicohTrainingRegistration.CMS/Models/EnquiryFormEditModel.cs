﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Models
{
    public class EnquiryFormEditModel
    {
        [Display(Name = "Company Name")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [StringLength(100)]
        public string CompanyName { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50)]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50)]
        [Required(ErrorMessage = "Please enter {0}.")]
        public string LastName { get; set; }

        [Display(Name = "Phone No.")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [Phone]
        [RegularExpression(@"^[\d]{8}$", ErrorMessage = "Invalid {0}.")]
        public string PhoneNo { get; set; }

        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Please enter {0}.")]
        [EmailAddress(ErrorMessage = "Invalid {0}.")]
        [StringLength(200)]
        public string Email { get; set; }

        [Display(Name = "Remarks")]
        [StringLength(200)]
        public string Remarks { get; set; }

        public bool? IsRead { get; set; }
    }
}
