﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS
{
    public class Breadcrumb
    {
        public Breadcrumb(string label)
        {
            Label = label;
        }

        public string Id { get; set; }
        public string Label { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Url { get; set; }
    }
}
