﻿using DotNetLib.Email;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Services
{
    class EmailRequestHandler
    {
        private readonly ILogger<EmailRequestHandler> logger;
        private readonly AppSettings appSettings;
        private readonly RicohDB db;
        private readonly IEmailClient client;

        public EmailRequestHandler(ILogger<EmailRequestHandler> logger, AppSettings appSettings, RicohDB db, IEmailClient client)
        {
            this.logger = logger;
            this.appSettings = appSettings;
            this.db = db;
            this.client = client;
        }

        async public Task ExecuteAsync()
        {
            var pending = (int)EmailRequestStatus.Pending;
            var retry = (int)EmailRequestStatus.Retry;
            var now = DateTime.Now;

            // find all unhandled requests
            Func<EmailRequest, bool> conditions = x => now >= x.RequestTime && (x.Status == pending || (x.Status == retry && now > x.NextAttempt));
            var requests = db.EmailRequest.Include(x => x.EmailTemplate).Where(conditions).ToList();
            logger.LogTrace("{0} email request(s) to be handled.", requests.Count);

            if (requests.Any())
            {
                var addresses = db.SystemEmail.ToList();
                var sender = addresses.Where(x => x.Type == SystemEmailType.Sender).ToList();
                var trainers = addresses.Where(x => x.Type == SystemEmailType.Trainer).ToList();
                var replyTo = addresses.Where(x => x.Type == SystemEmailType.ReplyTo).ToList();
                var returnPath = addresses.SingleOrDefault(x => x.Type == SystemEmailType.ReturnPath)?.EmailAddress;

                foreach (var req in requests)
                {
                    req.Attempt++;

                    try
                    {
                        logger.LogInformation("Handle EmailRequest[{0}]: @{1},{2},{3}", req.Id, req.Attempt, req.TemplateId, req.Receiver);

                        var template = req.EmailTemplate;
                        if (template == null)
                            throw new Exception($"Unknown EmailTemplate: {req.TemplateId}");

                        EmailEnvelope email = new EmailEnvelope();

                        // sender
                        email.From = sender.Select(x => new EmailAddress { Address = x.EmailAddress, Name = x.DisplayName }).ToList();

                        // recipients
                        var recipients = new List<EmailAddress>();
                        recipients.AddRange(
                            req.Receiver.Split(';')
                                .Select(x => x.Split(':'))
                                .Where(x => !string.IsNullOrWhiteSpace(x.FirstOrDefault()))
                                .Select(x => new EmailAddress { Address = x.First(), Name = x.Skip(1).FirstOrDefault() })
                        );

                        if (template.SendToTrainers)
                            recipients.AddRange(trainers.Select(x => new Recipient { Type = RecipientType.Bcc, Address = x.EmailAddress, Name = x.DisplayName }));

                        if (!recipients.Any())
                            throw new Exception("No recipients specified.");

                        email.To = recipients;

                        // reply to
                        email.ReplyTo = replyTo.Select(x => new EmailAddress { Address = x.EmailAddress, Name = x.DisplayName }).ToList();

                        // subject
                        email.Subject = template.GetSubject(null, req.Language);

                        // body
                        var bodyData = JsonConvert.DeserializeObject<Dictionary<string, object>>(req.Data);
                        email.Body = template.GetContent(bodyData, req.Language);

                        // return path
                        if (returnPath != null)
                            email.ReturnPath = new EmailAddress { Address = returnPath };

                        // send
                        var result = await client.SendAsync(email);
                        if (result.Success)
                        {
                            req.SentTime = DateTime.Now;
                            req.Status = (int)EmailRequestStatus.Sent;
                            logger.LogInformation("EmailRequest[{0}] handled.", req.Id);
                        }
                        else
                            throw result.Error ?? new Exception("Send fail.");
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, "Handle EmailRequest[{0}] fail.", req.Id);

                        if (req.Attempt < appSettings.SendEmailRetry)
                        {
                            req.Status = retry;
                            req.NextAttempt = DateTime.Now.AddSeconds(appSettings.SendEmailRetryInterval);
                        }
                        else
                        {
                            req.Status = (int)EmailRequestStatus.Fail;
                        }
                    }
                    finally
                    {
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}
