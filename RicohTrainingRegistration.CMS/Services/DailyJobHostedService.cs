﻿using DotNetLib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace RicohTrainingRegistration.CMS.Services
{
    public class DailyJobHostedService : IHostedService, IDisposable
    {
        private readonly IServiceProvider _services;
        private readonly ILogger<DailyJobHostedService> _logger;
        private readonly AppSettings _appSettings;

        private Timer _timer1;
        private int _tick1 = 5;
        private TimeSpan executeTime;
        private DateTime nextExecuteTime;
        private bool executing;
        private int attempt = 0;
        private int progress = 0;

        private Timer _timer2;
        private int _tick2 = 10;
        private bool sending;

        public DailyJobHostedService(IServiceProvider services, ILogger<DailyJobHostedService> logger, AppSettings appSettings)
        {
            _services = services;
            _logger = logger;
            _appSettings = appSettings;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Daily Job is running.");

            // set execute time
            if (DateTime.TryParseExact(_appSettings.DailyJobTime, "HH:mm", Thread.CurrentThread.CurrentCulture, System.Globalization.DateTimeStyles.None, out DateTime time))
                executeTime = new TimeSpan(time.Hour, time.Minute, 0);
            else
                executeTime = new TimeSpan(4, 0, 0);
            _logger.LogInformation("Execute time of daily job is {0}", executeTime.ToString("hh\\:mm"));

            // set next execute time
            attempt = 0;
            nextExecuteTime = DateTime.Today.Add(executeTime);
            if (DateTime.Now > nextExecuteTime)
                nextExecuteTime = nextExecuteTime.AddDays(1);
            _logger.LogInformation("Daily Job next execute time: {0:yyyy-MM-dd HH:mm}", nextExecuteTime);

            // timer
            _timer1 = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(_tick1));
            _timer2 = new Timer(HandleEmailRequests, null, TimeSpan.Zero, TimeSpan.FromSeconds(_tick2));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Daily Job is stopping.");

            _timer1?.Change(Timeout.Infinite, 0);
            _timer2?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer1?.Dispose();
            _timer2?.Dispose();
        }

        private void DoWork(object state)
        {
            if (executing)
                return;

            var now = DateTime.Now;
            if (now >= nextExecuteTime)
            {
                executing = true;
                _logger.LogInformation("Daily Job is working.");

                attempt++;
                try
                {
                    using (var scope = _services.CreateScope())
                    {
                        ProceedDailyJob(scope);
                        ResetExecution();
                    }
                    _logger.LogInformation("Daily Job is executed successfully.");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error occur while Daily Job is working.");

                    if (attempt < _appSettings.DailyJobRetry)
                    {
                        _logger.LogInformation("Pending to retry ({0})", attempt);
                        nextExecuteTime = DateTime.Now.AddSeconds(_appSettings.DailyJobRetryInterval);
                    }
                    else
                    {
                        _logger.LogInformation("Retry overflow.");
                        SendFailEmail(ex);
                        ResetExecution();
                    }
                }
                finally
                {
                    _logger.LogInformation("Daily Job next execute time: {0:yyyy-MM-dd HH:mm:ss}", nextExecuteTime);
                    executing = false;
                }
            }
        }

        private void ProceedDailyJob(IServiceScope scope)
        {
            var api = scope.ServiceProvider.GetRequiredService<RicohApi>();
            var db = scope.ServiceProvider.GetRequiredService<RicohDB>();
            var strategy = db.Database.CreateExecutionStrategy();
            strategy.Execute(() =>
            {
                if (progress == 0)
                {
                    var tran = db.Database.BeginTransaction();
                    RetrieveModels(api, db);
                    RetrieveInstallations(api, db);
                    tran.Commit();
                    _logger.LogInformation("Data retrieved successfully.");

                    progress++;
                }

                if (progress == 1)
                {
                    var tran = db.Database.BeginTransaction();
                    SendRegistrationEmails(db);
                    tran.Commit();

                    progress++;
                }

                if (progress == 2)
                {
                    var tran = db.Database.BeginTransaction();
                    SendReminderEmails(db);
                    tran.Commit();

                    progress++;
                }
            });
        }

        private void RetrieveModels(RicohApi api, RicohDB db)
        {
            _logger.LogInformation("Executing: RetrieveModels");
            var log = new JobLog { Title = JobLogTitle.RetrieveModel, StartTime = DateTime.Now };

            var settings = db.GetDefaultSettings();
            var openedDays = settings.OpenedDayForRegistration ?? 0;
            var retrieveTime = DateTime.Now;
            var models = api.GetModels();
            var trainingModels = db.TrainingModel.ToList();
            foreach (var model in models)
            {
                var tm = trainingModels.Find(x => x.ModelId == model.ModelId);
                if (tm == null)
                    tm = db.TrainingModel.Add(new TrainingModel
                    {
                        ModelId = model.ModelId,
                        OpenedDays = openedDays,
                        IsFreeTraining = false,
                        RetrievedDate = retrieveTime
                    }).Entity;

                tm.ModelName = model.ModelName;
                tm.ImportTime = model.ImportTime;
            }
            var changes = db.SaveChanges();

            log.EndTime = DateTime.Now;
            log.Message = string.Join("; ", $"{models.Count} models retrieved", $"{changes} record(s) affected");
            db.JobLog.Add(log);

            db.SaveChanges();

            _logger.LogInformation("Executed. ({0})", log.Message);
        }

        private void RetrieveInstallations(RicohApi api, RicohDB db)
        {
            _logger.LogInformation("Executing: RetrieveInstallations");
            var log = new JobLog { Title = JobLogTitle.RetrieveInstallation, StartTime = DateTime.Now };

            var retrieveTime = DateTime.Now;
            var calls = api.GetServiceCalls();
            var installations = db.MachineInstallation.ToList();
            var dates = db.MachineInstallationDate.ToList();
            var models = db.TrainingModel.ToList();
            var settings = db.GetDefaultSettings();
            var freePeriod = settings.FreeTrainingPeriod ?? 0;
            if (freePeriod <= 0)
                _logger.LogWarning("Free training period ({0} days) seems not valid.", freePeriod);

            foreach (var call in calls)
            {
                var inst = installations.Find(x => x.OrderId == call.OrderId);
                if (inst == null)
                    inst = db.MachineInstallation.Add(new MachineInstallation
                    {
                        OrderId = call.OrderId,
                        RetrievedDate = retrieveTime,
                        ModelId = call.ModelId
                    }).Entity;

                call.Populate(inst);

                var date = dates.Find(x => x.OrderId == inst.OrderId);
                if (settings.CompletedInstallationProblemCodes.Contains(inst.ProblemCode))
                {
                    if (date == null)
                    {
                        var model = models.Find(x => x.ModelId.EqualsIgnoreCase(inst.ModelId));
                        
                        date = new MachineInstallationDate();
                        date.OrderId = inst.OrderId;
                        date.SerialNo = inst.SerialNo;
                        date.InstallDate = inst.LastChangeTime;
                        date.ProblemCode = inst.ProblemCode;
                        date.FreeTrainingDay = freePeriod;
                        date.FreeTrainingEndDate = date.InstallDate.Date.AddDays(date.FreeTrainingDay);
                        date.IsFreeTraining = model?.IsFreeTraining == true;
                        date.WithCutoff = settings?.RescheduleHasCutoff == true;
                        db.MachineInstallationDate.Add(date);
                        dates.Add(date);

                        if (date.IsFreeTraining)
                            AddInstallationEmails(date);

                        _logger.LogInformation("Added #{0} - {1} - {2} => {3:yyyy-MM-dd}", date.SerialNo, inst.OrderId, date.ProblemCode, date.FreeTrainingEndDate);
                    }
                    else
                        _logger.LogInformation("Skipped #{0} - {1} - {2}", date.SerialNo, date.OrderId, inst.ProblemCode);
                }
            }
            var changes = db.SaveChanges();

            log.EndTime = DateTime.Now;
            log.Message = string.Join("; ", $"{calls.Count} service_calls retrieved", $"{changes} record(s) affected");
            db.JobLog.Add(log);

            db.SaveChanges();

            _logger.LogInformation("Executed. ({0})", log.Message);
        }

        private void AddInstallationEmails(MachineInstallationDate date)
        {
            var t1 = new TimeSpan(9, 0, 0);
            if (DateTime.TryParseExact(_appSettings.ExpireReminderEmailTime, "HH:mm", Thread.CurrentThread.CurrentCulture, System.Globalization.DateTimeStyles.None, out DateTime time))
                t1 = time.TimeOfDay;

            var expireEmails = _appSettings.ExpireReminderEmailDays.Select(x => new MachineInstallationEmail
            {
                OrderId = date.OrderId,
                SerialNo = date.SerialNo,
                InstallDate = date.InstallDate,
                EmailTemplateId = EmailTemplateId.REG_REMIND,
                TargetDate = date.FreeTrainingEndDate.Date.AddDays(-x).Add(t1),
                IsActive = true
            });
            foreach (var email in expireEmails)
            {
                if (DateTime.Today < email.TargetDate)
                    date.MachineInstallationEmail.Add(email);
            }
        }

        private void SendRegistrationEmails(RicohDB db)
        {
            _logger.LogInformation("Executing: SendRegistrationEmails");
            var log = new JobLog { Title = JobLogTitle.SendRegistrationEmail, StartTime = DateTime.Now };

            var dates = db.MachineInstallationDate
                .Include(x => x.EmailRequest)
                .Include(x => x.MachineInstallation)
                .Where(x => x.EmailRequestId == null)
                .Where(x => x.IsFreeTraining)
                .ToList();
            _logger.LogInformation("{0} registration emails should have to be enqueued", dates.Count);

            if (dates.Any())
            {
                DateTime now = DateTime.Now;
                DateTime requestTime = now;
                if (DateTime.TryParseExact(_appSettings.RegistrationEmailSendTime, "HH:mm", Thread.CurrentThread.CurrentCulture, System.Globalization.DateTimeStyles.None, out DateTime time))
                    requestTime = DateTime.Today.Add(new TimeSpan(time.Hour, time.Minute, 0));
                requestTime = requestTime.AddDays(now > requestTime ? 1 : 0);

                UriBuilder builder = new UriBuilder(_appSettings.RegistrationUrl);
                builder.Path = "/invite";

                foreach (var date in dates)
                {
                    var qs = HttpUtility.ParseQueryString("");
                    qs["sn"] = SecurityHelper.Encrypt(RegistrationPassphrase.Register, date.SerialNo);
                    builder.Query = qs.ToString();

                    date.EmailRequest = db.AddEmailRequest(
                        EmailTemplateId.REG_INVITE,
                        new
                        {
                            MODEL_NO = date.MachineInstallation.ModelId,
                            SERIAL_NO = date.SerialNo,
                            REG_URL = builder.Uri.AbsoluteUri,
                            REG_DATE = date.FreeTrainingEndDate.ToString("yyyy/MM/dd")
                        },
                        date.MachineInstallation.Email,
                        date.MachineInstallation.ContactName,
                        "en-US"
                    );
                    date.EmailRequest.RequestTime = requestTime;
                }
            }
            int changes = db.SaveChanges();

            log.EndTime = DateTime.Now;
            log.Message = string.Join("; ", $"{dates.Count} installations retrieved to enqueue sending registration email", $"{changes} record(s) affected");
            db.JobLog.Add(log);

            db.SaveChanges();

            _logger.LogInformation("Executed. ({0})", log.Message);
        }

        private void SendReminderEmails(RicohDB db)
        {
            _logger.LogInformation("Executing: SendReminderEmails");
            var log = new JobLog { Title = JobLogTitle.SendReminderEmail, StartTime = DateTime.Now };

            var today = DateTime.Today;
            var dates = db.MachineInstallationEmail
                .Include(x => x.Order)
                .ThenInclude(x => x.MachineInstallation)
                .Where(x => x.IsActive)
                .Where(x => x.EmailRequestId == null && EF.Functions.DateDiffDay(x.TargetDate, today) >= 0)
                .ToList();

            _logger.LogInformation("{0} reminder emails should have to be enqueued", dates.Count);

            if (dates.Any())
            {
                var orderIds = dates.Select(x => x.OrderId).Distinct().ToList();
                var registrations = db.RegistrationTimeslot.Where(x => orderIds.Contains(x.OrderId)).ToList();

                UriBuilder builder = new UriBuilder(_appSettings.RegistrationUrl);
                builder.Path = "/invite";

                foreach (var date in dates)
                {
                    var reg = registrations.Find(x => x.OrderId == date.OrderId);
                    if (date.EmailTemplateId == EmailTemplateId.REG_REMIND)
                    {
                        if (reg?.Status == RegistrationStatus.Submitted || reg?.Status == RegistrationStatus.Rescheduled)
                        {
                            date.IsActive = false;
                            _logger.LogInformation("#{0} - {1} already registered. Reminder email skipped.", reg.OrderId, reg.SerialNo);
                            continue;
                        }

                        var qs = HttpUtility.ParseQueryString("");
                        qs["sn"] = SecurityHelper.Encrypt(RegistrationPassphrase.Register, date.SerialNo);
                        builder.Query = qs.ToString();
                        date.EmailRequest = db.AddEmailRequest(
                            date.EmailTemplateId,
                            new
                            {
                                MODEL_NO = date.Order.MachineInstallation.ModelId,
                                SERIAL_NO = date.SerialNo,
                                REG_URL = builder.Uri.AbsoluteUri,
                                EXP_DATE = date.Order.FreeTrainingEndDate.ToString("yyyy/MM/dd")
                            },
                            date.Order.MachineInstallation.Email,
                            date.Order.MachineInstallation.ContactName
                        );
                    }
                    else if (date.EmailTemplateId == EmailTemplateId.TRAINING_REMIND)
                    {
                        if (reg?.Status != RegistrationStatus.Submitted && reg?.Status != RegistrationStatus.Rescheduled)
                        {
                            date.IsActive = false;
                            _logger.LogInformation("#{0} - {1} is not registered. Reminder email skipped.", reg?.OrderId ?? date.OrderId, reg?.SerialNo ?? date.SerialNo);
                            continue;
                        }

                        var trainingDate = string.Format("{0:yyyy/MM/dd}", reg.ScheduleDate ?? reg.TrainingDate);
                        var trainingTimeslot = reg.ScheduleTimeslot ?? reg.TrainingTimeslot ?? "";
                        var t = trainingTimeslot.Split('-').FirstOrDefault()?.Trim();
                        if (DateTime.TryParseExact($"{trainingDate} {t}", "yyyy/MM/dd HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime time) && DateTime.Now > time)
                        {
                            date.IsActive = false;
                            _logger.LogInformation("#{0} - {1} training is already started ({2}). Reminder email skipped.", reg.OrderId, reg.SerialNo, t);
                            continue;
                        }

                        date.EmailRequest = db.AddEmailRequest(
                            date.EmailTemplateId,
                            new
                            {
                                CUSTOMER_NAME = reg.CompanyName,
                                CONTACT_NAME = reg.ContactName,
                                PHONE_NO = reg.PhoneNo,
                                MODEL_NO = reg.ModelId,
                                SERIAL_NO = reg.SerialNo,
                                TRAINING_DATE = trainingDate,
                                TRAINING_TIMESLOT = trainingTimeslot,
                                VENUE_ADDRESS = reg.VenueAddress?.Trim()
                            },
                            reg.Email,
                            reg.ContactName
                        );
                        date.EmailRequest.Language = date.Language;
                    }
                    else
                        continue;

                    date.EmailRequest.RequestTime = date.TargetDate;
                }
            }
            int changes = db.SaveChanges();

            log.EndTime = DateTime.Now;
            log.Message = string.Join("; ", $"{dates.Count} installations retrieved to enqueue sending reminder email", $"{changes} record(s) affected");
            db.JobLog.Add(log);

            db.SaveChanges();

            _logger.LogInformation("Executed. ({0})", log.Message);
        }

        private void SendFailEmail(Exception exception)
        {
            System.Net.Mail.MailAddress mailAddress;
            try
            {
                mailAddress = new System.Net.Mail.MailAddress(_appSettings.AdminEmailAddress);
            }
            catch (Exception)
            {
                _logger.LogInformation("Admin email address not configured.");
                return;
            }

            try
            {
                using (var scope = _services.CreateScope())
                {
                    var db = scope.ServiceProvider.GetRequiredService<RicohDB>();
                    var strategy = db.Database.CreateExecutionStrategy();
                    strategy.Execute(() =>
                    {
                        string name = "";
                        switch (progress)
                        {
                            case 0: name = "Sync RICOH database fail"; break;
                            case 1: name = "Enqueue registration emails fail"; break;
                            case 2: name = "Enqueue reminder emails fail"; break;
                        }

                        var request = db.AddEmailRequest(
                            EmailTemplateId.SYSTEM_FAIL,
                            new
                            {
                                DATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                NAME = name,
                                ERROR = exception.Message,
                                TRACE = exception.StackTrace
                            },
                            mailAddress.Address
                        );
                        //request.RequestTime = DateTime.Today.AddDays(1);
                        db.SaveChanges();
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to enqueue system fail email");
            }
        }

        private void ResetExecution()
        {
            attempt = 0;
            progress = 0;
            nextExecuteTime = DateTime.Today.AddDays(1).Add(executeTime);
        }

        async private void HandleEmailRequests(object state)
        {
            if (sending)
                return;

            sending = true;
            _logger.LogTrace("HandleEmailRequests start.");
            try
            {
                using (var scope = _services.CreateScope())
                {
                    var service = scope.ServiceProvider.GetRequiredService<EmailRequestHandler>();
                    await service.ExecuteAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occur while handling email requests.");
            }
            finally
            {
                _logger.LogTrace("HandleEmailRequests end.");
                sending = false;
            }
        }
    }
}
