var Page = function () {

    var $modal, $table, $form;

    var init = function () {

        $modal = $('#modal1');
        $table = $('#dt1');
        $form = $('#frm1');

        handleDatable();
        handleModal();
        handleMoment();
    };

    var handleDatable = function () {
        var dt1 = $table.DataTable({
            ajax: {
                url: $table.attr('data-dt-ajax'),
                data: function (data) { DT.ajaxData(data, $form); }
            },
            columns: [
                {
                    data: 'id',
                    render: function (data) {
                        return '<a href="javascript:;" class="btn-edit">' + data + '</a>';
                    }
                },
                { data: 'name' },
                {
                    data: 'isFree',
                    render: function (data) {
                        return data == true ? '<span class="text-success" title="Entitled free training"><i class="fa fa-fw fa-check-circle"></i></span>' : '<span class="text-danger" title="Withdrawn from free training"><i class="fa fa-fw fa-times-circle"></i></span>';
                    }
                },
                { data: 'openedDays' },
                {
                    data: 'agenda',
                    render: function (data) {
                        return data ? '<a href="' + data.url + '" target="_blank">' + data.title + '</a>' : '';
                    }
                },
                {
                    data: 'retrievedDate',
                    render: function (data) {
                        var m = moment(data);
                        return m.isValid() ? '<span title="' + m.format('YYYY/MM/DD HH:mm') + '">' + m.fromNow() + '</span>' : '';
                    }
                },
                {
                    data: 'updatedDate',
                    render: function (data) {
                        var m = moment(data);
                        return m.isValid() ? '<span title="' + m.format('YYYY/MM/DD HH:mm') + '">' + m.fromNow() + '</span>' : '';
                    }
                },
                {
                    orderable: false,
                    data: function (data) {
                        return '<button class="btn btn-default btn-sm btn-edit"><i class="fa fa-edit"></i> Edit</button>';
                    }
                }
            ]
        });
        DT.form(dt1, $form);

        $form.on('change', ':input', function () { $form.trigger('submit'); });
        $table.on('click', '.btn-edit', function () {
            var tr = $(this).closest('tr');
            var data = $table.DataTable().row(tr[0]).data();
            $('[data-field="id"]', $modal).text(data.id);
            $('[data-field="name"]', $modal).text(data.name);
            $('[name="isFree"]', $modal).prop('checked', data.isFree == true);
            $('[name="OpenedDays"]', $modal).val(data.openedDays);
            $('form', $modal).attr('action', data.editUrl);
            $modal.modal();
        });
    };

    var handleModal = function () {
        $modal.on('change', '.open-days', function () {
            var input = $(this);
            var val = parseInt(input.val());
            val = isNaN(val) ? 0 : val;
            val = Math.min(720, Math.max(0, val));
            input.val(val);
        });

        $modal.on('submit', 'form', function (e) {
            e.preventDefault();
            Ajax.load({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (resp) {
                    Ajax.success(resp);
                    if (resp.success) {
                        $table.DataTable().draw(false);
                        $modal.modal('hide');
                    }
                }
            });
        });
    };

    var handleMoment = function () {
        var elem = $('[data-last-retrieved-date]');
        var val = elem.attr('data-last-retrieved-date');
        var m = moment(val);
        var text = m.isValid() ? m.fromNow() : val;
        elem.text(text);
    };

    return ({
        init: init
    });
}();