var Page = function ($) {

    var _opts = {
        defaultQueryUrl: null,
        timeslots: {
            defaults: [],
            customs: []
        }
    };
    var $tbl1, $tbl2;

    var init = function (opts) {
        opts && $.extend(_opts, opts);
        $tbl1 = $('#dt1');
        $tbl2 = $('#dt2');

        handleControls();
        handleEvents();
        handleTables();
        handleSubmission();
    };

    var handleControls = function () {
        $('.datepicker').attr('autocomplete', 'off').datepicker();
    };

    var handleEvents = function () {
        $('.datepicker,.district').on('change', function () {
            var date = $('.datepicker').val();
            var district = $('.district:checked').val();
            $.get(_opts.defaultQueryUrl, { date, district }, function (data) {
                helper.renderTimeslots($tbl1, data, false);
            });
        });
    };

    var handleTables = function () {
        var dtDefaults = {
            serverSide: false,
            processing: false,
            ajax: null,
            searching: false,
            ordering: false,
            paging: false,
            info: false,
            data: [],
            columns: []
        };

        $tbl1.DataTable($.extend({}, dtDefaults, {
            data: _opts.timeslots.defaults,
            columns: [
                { data: 'timeslot' },
                { data: 'quota' },
                {
                    data: 'registrations',
                    render: function (data) {
                        return data.map(function (value) {
                            return '<a href="' + value.url + '" target="_blank">' + value.label + '</a>';
                        }).join('<br />');
                    }
                }
            ]
        }));

        $tbl2.DataTable($.extend({}, dtDefaults, {
            data: _opts.timeslots.customs,
            columns: [
                {
                    data: 'timeslot',
                    render: function (data, type, row, meta) {
                        var value = data || '';
                        var start = row.startTime || '';
                        var end = row.endTime || '';
                        var name = 'Data.CustomTimeslots.' + meta.row + '.Timeslot';
                        return '<div class="input-group input-group-sm"><div class="input-group-addon"><i class="fa fa-clock-o"></i></div><input type="text" class="form-control timepicker" value="' + start + '" data-ts="start" /><div class="input-group-addon">~</div><input type="text" class="form-control timepicker" value="' + end + '" data-ts="end" /></div><input type="hidden" class="timeslot" name="' + name + '" value="' + value + '" />';
                    }
                },
                {
                    data: 'quota',
                    render: function (data, type, row, meta) {
                        var value = data || '';
                        var name = 'Data.CustomTimeslots.' + meta.row + '.Quota';
                        return '<input type="number" name="' + name + '" value="' + value + '" class="form-control input-sm quota" min="0" max="99" />';
                    }
                },
                {
                    data: 'registrations',
                    render: function (data) {
                        return data.map(function (value) {
                            return '<a href="' + value.url + '" target="_blank">' + value.label + '</a>';
                        }).join('<br />');
                    }
                },
                {
                    data: function (data) {
                        return helper.isRegistered(data) ? '' : '<button type="button" class="btn btn-xs btn-danger btn-remove" title="Remove"><i class="fa fa-times"></i></button>';
                    }
                }
            ],
            createdRow: function (row, data) {
                var tr = $(row);
                helper.isRegistered(data) && tr.find(':input').prop('disabled', true);
            },
            drawCallback: function () {
                var table = $(this.api().table().node());
                $('.timepicker', table).timepicker({
                    showInputs: false,
                    showMeridian: false,
                    defaultTime: false
                });
            }
        }));

        $tbl2.on('click', '.btn-add', helper.addTimeslot);
        $tbl2.on('click', '.btn-add-default', helper.addDefaultTimeslots);
        $tbl2.on('click', '.btn-remove', helper.removeTimeslot);
    };

    var handleSubmission = function () {
        var $frm = $('#frm1');

        $frm.on('submit', function (e) {
            e.preventDefault();

            var i = 0;
            $tbl2.DataTable().rows().every(function (rowIdx, tableLoop, rowLoop) {
                var data = this.data();
                if (helper.isRegistered(data)) return;

                var tr = $(this.node());
                var start = $('[data-ts=start]', tr).val() || '';
                start = (start.length == 4 ? '0' : '') + start;
                var end = $('[data-ts=end]', tr).val() || '';
                end = (end.length == 4 ? '0' : '') + end;
                $('.timeslot', tr).attr('name', 'Data.CustomTimeslots.' + i + '.Timeslot').val(start + ' - ' + end);
                $('.quota', tr).attr('name', 'Data.CustomTimeslots.' + i + '.Quota');
                i++;
            });

            var url = $frm.attr('action');
            var data = $frm.serializeObject();
            Ajax.load({ url, data });
        });
    };

    var helper = {
        addTimeslot: function () {
            var btn = $(this);
            var tbl = btn.closest('table');
            var dt = tbl.DataTable();
            var row = { timeslot: '', quota: '', registrations: [], serialNos: [] };
            dt.row.add(row).draw();
        },
        addDefaultTimeslots: function () {
            var btn = $(this);
            var tbl = btn.closest('table');
            var dt = tbl.DataTable();
            var rows = _opts.timeslots.defaults.map(function (value) {
                var times = value.timeslot.split(' - ');
                return {
                    timeslot: value.timeslot,
                    startTime: times[0],
                    endTime: times[1],
                    quota: value.quota,
                    registrations: [],
                    serialNos: []
                };
            });
            dt.rows.add(rows).draw();
        },
        removeTimeslot: function () {
            var btn = $(this);
            var tr = btn.closest('tr');
            var tbl = tr.closest('table');
            var dt = tbl.DataTable();
            var row = dt.row(tr.get(0));
            var data = row.data();
            !helper.isRegistered(data) && row.remove().draw();
        },
        isRegistered: function (data) {
            return data && data.registrations && data.registrations.length;
        }
    };

    return ({
        init: init
    });
}(jQuery);