var Page = function ($) {

    var _opts = {
        data: null
    };
    var $tbl, $dpkr;

    var init = function (opts) {
        opts && $.extend(_opts, opts);
        $tbl = $('#dt1');

        $dpkr = $('#StartDate').attr('autocomplete', 'off');
        $dpkr.datepicker();

        handleTable();
        handleSave();
    };

    var handleTable = function () {
        var renderEmpty = function () {
            $tbl.find('tr.empty').remove();
            var rows = $tbl.find('tr.data');
            if (!rows.length) {
                var empty = $('<tr class="empty"><td colspan="17" class="text-center">No records yet.</td></tr>');
                $tbl.find('tbody').append(empty);
            }
        };
        var addTimeslot = function (timeslot) {
            var data = timeslot && _opts.data[timeslot] ? _opts.data[timeslot] : [];

            var tr = $('<tr class="data"/>');
            tr.append('<td><div class="input-group input-group-sm"><input type="text" class="form-control text-center timepicker" data-ts="start" /><div class="input-group-addon"><i class="fa fa-clock-o"></i></div></div><div class="text-center">~</div><div class="input-group input-group-sm"><input type="text" class="form-control text-center timepicker" data-ts="end" /><div class="input-group-addon"><i class="fa fa-clock-o"></i></div></div><input type="hidden" class="timeslot" /></td>');

            if (timeslot && typeof (timeslot) === 'string') {
                tr.find('.timeslot').val(timeslot);
                var ts = timeslot.split(' - ');
                tr.find('[data-ts=start]').val(ts[0]);
                tr.find('[data-ts=end]').val(ts[1]);
            }

            $(DAYS_OF_WEEK).each(function () {
                var d = this;
                $(DISTRICTS).each(function () {
                    var t = this;
                    var input = $('<input type="number" class="form-control input-sm quota" maxlength="2" />');
                    input.attr('data-dow', d);
                    input.attr('data-dis', t);

                    var item = data.find(function (value) { return value.dayOfWeek == d && value.district == t; });
                    item && input.val(isNaN(parseInt(item.quota)) ? '' : item.quota);

                    var td = $('<td/>');
                    input.appendTo(td);
                    td.appendTo(tr);
                });
            });
            tr.append('<td><button type="button" class="btn btn-xs btn-danger btn-remove"><i class="fa fa-remove"></i></button></td>');
            $tbl.find('tbody').append(tr);

            tr.find('.timepicker').timepicker({
                showInputs: false,
                showMeridian: false,
                defaultTime: false
            });

            renderEmpty();
        };
        var removeTimeslot = function () {
            $(this).closest('tr').remove();
            renderEmpty();
        };

        $tbl.on('click', '.btn-add', function () { addTimeslot.call(this); });
        $tbl.on('click', '.btn-remove', function () { removeTimeslot.call(this); });

        // init table
        if (_opts.data) {
            var keys = Object.keys(_opts.data);
            keys.forEach(function (timeslot) {
                addTimeslot(timeslot);
            });
        } else {
            renderEmpty();
        }
    };

    var handleSave = function () {
        $tbl.on('change', '.quota', function () {
            var input = $(this);
            var val = parseInt(input.val());
            val = isNaN(val) ? 0 : val;
            val = Math.min(99, Math.max(0, val));
            input.val(val);
        });

        $('.btn-save').on('click', function () {
            doSave();
        });

        var doSave = function (force) {
            var rows = $tbl.find('tbody tr.data');
            var data = {};
            for (var i = 0; i < rows.length; i++) {
                var tr = $(rows.get(i));
                tr.removeClass('danger');

                var list = [];

                var start = $('[data-ts=start]', tr);
                var startVal = start.val();
                if (!startVal || !startVal.match(/^\d{1,2}:\d{2}$/)) {
                    alert('Invalid start time');
                    start.select().focus();
                    return;
                }

                var end = $('[data-ts=end]', tr);
                var endVal = end.val();
                if (!endVal || !endVal.match(/^\d{1,2}:\d{2}$/)) {
                    alert('Invalid end time');
                    end.select().focus();
                    return;
                }

                startVal = (startVal.length < 5 ? '0' : '') + startVal;
                endVal = (endVal.length < 5 ? '0' : '') + endVal;
                var time = startVal + ' - ' + endVal;
                if (time in data) {
                    alert('Duplicated timeslot: ' + time);
                    tr.addClass('danger');
                    return;
                }

                $('[data-dow][data-dis]', tr).each(function () {
                    var dow = parseInt($(this).attr('data-dow'));
                    var dis = $(this).attr('data-dis');
                    var quota = parseInt($(this).val()) || 0;
                    if (DAYS_OF_WEEK.indexOf(dow) >= 0 && DISTRICTS.indexOf(dis) >= 0) {
                        var item = {
                            dayOfWeek: dow,
                            district: dis,
                            quota: quota
                        };
                        list.push(item);
                    }
                });

                data[time] = list;
            }
            //console.debug(data);

            var startDate = $dpkr.val();

            var url = '';
            var formData = $.extend({
                data: data,
                startDate: startDate
            }, __csrf);
            if (force) { url = '?force=1'; }

            Ajax.load({
                url: url,
                data: formData,
                success: function (resp) {
                    if (resp.waitForConfirm) {
                        bootbox.confirm(resp.message, function (result) {
                            result && doSave(true);
                        });
                    } else {
                        Ajax.success.call(this, resp);
                    }
                }
            });
        };
    };

    return ({
        init: init
    });
}(jQuery);