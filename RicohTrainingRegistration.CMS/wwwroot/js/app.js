﻿(function ($) {

    $(document).ready(init);

    function init() {
        ajax();
        moments();
        buttons();
        defaults();

        pageLoaded();
    }

    function ajax() {
        //$(document).on('ajaxStart', function () { Loader.startLoading(); });
        //$(document).on('ajaxComplete', function () { Loader.endLoading(); });

        $('form.ajax-form').each(function () {
            Ajax.form({ form: this });
        });
    }

    function moments() {
        moment && __locale && moment.locale(__locale);
    }

    function buttons() {
        $('body').on('click', '[data-href]', function () {
            var url = $(this).attr('data-href');
            var target = $(this).attr('data-href-target');
            if (target == '_blank') {
                window.open(url);
            } else {
                if ($(this).attr('data-href-replace')) {
                    window.location.replace(url);
                } else {
                    window.location = url;
                }
            }
        });
        $('body').on('click', '[data-btn-del]', function () {
            var url = $(this).attr('data-btn-del');
            var message = $(this).attr('data-btn-del-msg') || 'Confirm to delete?';
            if (bootbox) {
                bootbox.confirm(message, function (result) { result && btnAction(url); });
                return;
            }
            confirm(message) && btnAction(url);
        });

        function btnAction(url) {
            Ajax.load({ url: url, method: 'DELETE' });
        }
    }

    function defaults() {
        $.extend(FormSerializer.patterns, {
            validate: /^[a-z_][a-z0-9_]*(?:\.[a-z0-9_]+)*(?:\[\])?$/i
        });
    }

    function pageLoaded() {
        setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
        $(window).on('beforeunload', function () { $('.page-loader-wrapper').fadeIn(); });
    }
})(jQuery);