﻿var Loader = function () {
    var loading = 0;

    function startLoading() {
        loading++;
        if ($('.app-loader-wrapper').is(':hidden')) {
            $('.app-loader-wrapper').fadeIn();
            return true;
        }
        return false;
    }

    function endLoading(force) {
        loading--;
        if (force == true) {
            loading = 0;
        }
        if (loading <= 0 && $('.app-loader-wrapper').is(':visible')) {
            $('.app-loader-wrapper').fadeOut();
            return true;
        }
        return false;
    }

    return ({
        startLoading: startLoading,
        endLoading: endLoading
    });
}();
