﻿var Ajax = function () {

    function form(opts) {
        var elem = opts.form;
        if (!elem) return;

        var $form = $(elem);
        if (!elem.length) return;

        $form.on('submit', function (e) {
            e.preventDefault();
            var frm = $(this);
            if ($.fn.validate) {
                if (!frm.valid()) {
                    return;
                }
            }

            var url = frm.attr('action');
            var data = frm.serialize();
            typeof (opts.data) === 'function' && opts.data(data);

            load({ url: url, data: data });
        });
    }

    function load(opts) {
        var settings = {
            method: 'post',
            dataType: 'json',
            success: success,
            error: error
        };
        $.extend(settings, opts);

        Loader.startLoading();
        return $.ajax(settings).always(complete);
    }

    function success(resp) {
        var message = resp.message;
        if (message) {
            if (bootbox) {
                bootbox.alert(message, function () { successCallback(resp); });
                return;
            }
            alert(message);
        }
        successCallback(resp);
    }

    function successCallback(resp) {
        var url = resp.redirect;
        if (typeof (url) === 'string') {
            var type = resp.redirectType;
            if (type == 'replace') { window.location.replace(url); }
            else { window.location.assign(url); }
        }
    }

    function error(jqXHR, textStatus, errorThrown) {
        var message = '';
        var text = jqXHR.responseText;
        var type = jqXHR.getResponseHeader('content-type');
        if (type == null) {
            message = 'Server unavailable';
        } else {
            if (type.indexOf('application/json') > -1) {
                var resp = JSON.parse(text);
                message = resp.message;
            }
            if (type.indexOf('html') > -1) {
                console.debug('ajaxError', jqXHR);
                if (status >= 400) {
                    message = jqXHR.statusText;
                } else {
                    message = 'Unexpected response';
                }
            }
        }
        message = message || 'Unknown error';
        alert(message);
    }

    function complete(jqXHR, textStatus) {
        Loader.endLoading();
    }

    return ({
        form: form,
        load: load,
        success: success,
        error: error
    });
}();