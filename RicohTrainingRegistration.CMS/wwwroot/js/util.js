﻿var Util = function () {

    function fileSize(num) {
        var val = parseFloat(num);
        if (isNaN(val)) return '';

        var units = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        var len = units.length;
        var i = 0;
        while (val > 1024 && i < len) {
            val /= 1024.0;
            i++;
        }
        return val.toFixed(2) + units[i];
    }

    function setClipboard(value) {
        var tempInput = document.createElement("input");
        tempInput.style = "position: absolute; left: -1000px; top: -1000px";
        tempInput.value = value;
        document.body.appendChild(tempInput);
        tempInput.select();
        document.execCommand("copy");
        document.body.removeChild(tempInput);
    }

    return ({
        fileSize: fileSize,
        setClipboard: setClipboard
    });
}();