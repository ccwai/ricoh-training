﻿var DT = function () {

    function dtForm(api, selector) {
        var dt = api;
        var form = $(selector);
        if (dt && form.is('form')) {
            form.on('submit', function (e) {
                e.preventDefault();
                dt.draw();
            });
        }
    }

    function ajaxData(data) {
        if (!arguments || !arguments.length) return;

        typeof (__csrf) === 'object' && $.extend(data, __csrf);

        if (arguments.length > 1 && $.fn.serializeObject) {
            var form = $(arguments[1]);
            if (form.is('form')) {
                var extraData = form.serializeObject();
                $.extend(data, { params: extraData });
            }
        }
    }

    function renderEditLink(data, type, row) {
        var title = data;
        var obj = arguments.length < 3 ? type : row;
        var html = title;
        if (obj.actions && obj.actions.length) {
            var edit = obj.actions.find(function (value) { return value.type == 'edit' });
            if (edit && edit.url) {
                var className = row.hasOwnProperty('status') && !row.status ? 'disabled' : '';
                html = '<a href="' + edit.url + '" class="' + className + '" data-action="edit">' + title + '</a>';
            }
        }
        return html;
    }

    function renderActionLink(data) {
        if (!data) return '';

        var url = data.url;
        var title = data.title;
        var text = data.text;
        var icon = data.icon;
        var className = data.className;
        if (text && url) {
            var html = text;
            if (icon) { html = '<i class="' + icon + '"></i> ' + text; }
            return '<a href="' + url + '" title="' + title + '" class="' + className + '" target="_blank">' + html + '</a>';
        }
        return '';
    }

    function momentFromNow(data) {
        return momentFormat(data, 'fromNow');
    }

    function momentDate(data) {
        return momentFormat(data, 'YYYY/MM/DD');
    }

    function momentDateTime(data) {
        return momentFormat(data, 'YYYY/MM/DD H:mm');
    }

    function momentFormat(data, format) {
        if (moment) {
            var t = moment(data);
            if (t.isValid()) {
                if (format == 'fromNow') return t.fromNow();
                return t.format(format);
            }
        }
        return '';
    }

    function renderActions() {
        return {
            orderable: false,
            searchable: false,
            width: '1%',
            className: 'text-nowrap',
            data: 'actions',
            render: function (data) {
                var arr = [];
                $(data).each(function () {
                    var a = $('<a/>');
                    if (this.hasOwnProperty('url') && this.url != null) {
                        a.attr('href', this.url || '');
                    }
                    this.title && a.attr('title', this.title);
                    this.className && a.addClass(this.className);
                    this.icon && a.append('<i class="' + this.icon + '"></i>');
                    this.text && a.append(' ' + this.text);
                    if (this.attributes) {
                        for (var name in this.attributes) {
                            a.attr(name, this.attributes[name]);
                        }
                    }
                    arr.push($('<div/>').append(a).html());
                });
                return arr.join('');
            }
        };
    }

    function renderStatus(data) {
        if (data == true) return '<span class="text-success" title="Active"><i class="fa fa-circle"></i></span>';
        return '<span class="text-danger" title="Disabled"><i class="fa fa-circle-o"></i></span>';
    }

    function renderReorder(name) {
        return {
            orderable: true,
            searchable: false,
            width: '1%',
            className: 'text-nowrap',
            data: name,
            render: function (data) { return '<span class="reorder"><i class="fa fa-fw fa-bars"></i> <sub class="text-muted">' + data + '</sub></span>'; }
        };
    }

    var dataTableDefaults = {
        dom: '<"row"<"col-sm-6"l><"col-sm-6"f>>' +
            '<"row"<"col-sm-12"tr>>' +
            '<"row"<"col-sm-5"i><"col-sm-7"p>>',
        serverSide: true,
        ajax: {
            method: 'post',
            data: ajaxData
        },
        pageLength: 25
    };
    $.fn.dataTable && $.extend($.fn.dataTable.defaults, dataTableDefaults);

    // delete from datatable
    $('body').on('click', '[data-dt-action="delete"][data-dt-action-url]', function (e) {
        e.preventDefault();
        var url = $(this).attr('data-dt-action-url');
        if (!url) return;

        var message = $(this).attr('data-dt-action-message') || 'Confirm to delete?';
        var table = $(this).closest('table');
        bootbox.confirm(message, function (result) {
            if (!result) return;

            var data = {};
            __csrf && $.extend(data, __csrf);
            Ajax.load({
                url: url,
                data: data,
                method: 'DELETE',
                success: function (resp) {
                    if (resp.success) {
                        setTimeout(function () {
                            var redraw = function () {
                                var dt = table.DataTable();
                                dt && dt.draw && dt.draw(false);
                            };
                            if (resp.message) {
                                bootbox.alert(resp.message, redraw);
                            } else {
                                redraw();
                            }
                        }, 500);
                        return;
                    }
                    Ajax.success(resp);
                }
            });
        });
    });

    return ({
        form: dtForm,
        ajaxData: ajaxData,
        editLink: renderEditLink,
        actionLink: renderActionLink,
        momentFormat: momentFormat,
        momentFromNow: momentFromNow,
        momentDate: momentDate,
        momentDateTime: momentDateTime,
        actions: renderActions,
        status: renderStatus,
        reorder: renderReorder
    });
}();