﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NPOI.XSSF.UserModel;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Report
{
    public class IndexModel : PageModel
    {
        private readonly RicohDB db;

        public IndexModel(RicohDB db)
        {
            this.db = db;
        }

        [BindProperty]
        public ReportModel Report { get; set; }

        public List<SelectListItem> ReportSelectList => new List<SelectListItem>
        {
            new SelectListItem { Value = "AVAILABILITY", Text = "Training availability" },
            new SelectListItem { Value = "REGISTERED", Text = "Registered training sessions" },
            new SelectListItem { Value = "PROVIDED", Text = "Provided training sessions" }
        };

        public void OnGet()
        {
            var today = DateTime.Today;

            Report = new ReportModel();
            Report.From = new DateTime(today.Year, today.Month, 1);
            Report.To = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var id = Report.Id;
                var from = Report.From.Value;
                var to = Report.To.Value;
                var districts = db.GetDistrictList();
                byte[] content = null;

                if (id == "AVAILABILITY")
                {
                    var header = new string[] { "Date", "Time slot", "Area", "Quota Left" };
                    var data = db.ScheduledSession
                        .Where(x => EF.Functions.DateDiffDay(from, x.Date) >= 0 && EF.Functions.DateDiffDay(x.Date, to) >= 0)
                        .ToList()
                        .GroupBy(x => new { x.Date, x.Timeslot, x.District })
                        .OrderBy(x => x.Key.Date)
                        .ThenBy(x => x.Key.Timeslot)
                        .ThenBy(x => x.Key.District)
                        .Select(x => new string[]
                        {
                            x.Key.Date.ToString("yyyy-MM-dd"),
                            x.Key.Timeslot,
                            districts.FirstOrDefault(d => d.Id.Equals(x.Key.District))?.Label,
                            x.Count(c => c.IsCustom == c.IsUseCustom && !c.RegistrationId.HasValue).ToString()
                        })
                        .ToList();
                    content = Export(header, data);
                }
                else if (id == "REGISTERED")
                {
                    var attendees = db.GetAttendeeList();
                    var os = db.GetOperatingSystemList();
                    var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
                    var header = new string[] { "Date", "Time slot", "Area", "No. Of Attendees", "Operating System", "Company Name", "Venue", "First Name", "Last Name", "Phone Number", "Email", "Remarks", "Model", "Serial No.", "Item No.", "Order No." };
                    var data = db.RegistrationTimeslot
                        .Where(x => EF.Functions.DateDiffDay(from, x.ScheduleDate) >= 0 && EF.Functions.DateDiffDay(x.ScheduleDate, to) >= 0)
                        .Where(x => status.Contains(x.Status))
                        .ToList()
                        .OrderBy(x => x.ScheduleDate ?? x.TrainingDate)
                        .ThenBy(x => x.ScheduleTimeslot ?? x.TrainingTimeslot)
                        .ThenBy(x => x.ScheduleDistrict ?? x.District)
                        .Select(x => new string[]
                        {
                            string.Format("{0:yyyy-MM-dd}", x.ScheduleDate ?? x.TrainingDate),
                            x.ScheduleTimeslot ?? x.TrainingTimeslot,
                            districts.FirstOrDefault(d => d.Id.Equals(x.ScheduleDistrict ?? x.District))?.Label,
                            attendees.FirstOrDefault(a => a.Id == x.Attendees)?.Label,
                            os.FirstOrDefault(o => o.Id == x.OperatingSystem)?.Label,
                            x.CompanyName,
                            x.VenueAddress,
                            x.FirstName,
                            x.LastName,
                            x.PhoneNo,
                            x.Email,
                            x.Remarks,
                            x.ModelId,
                            x.SerialNo,
                            x.ItemNo,
                            x.OrderId
                        })
                        .ToList();
                    content = Export(header, data);
                }
                else if (id == "PROVIDED")
                {
                    var problemCodes = db.GetDefaultSettings().CompletedTrainingProblemCodes;
                    var attendees = db.GetAttendeeList();
                    var os = db.GetOperatingSystemList();
                    var header = new string[] { "Date", "Time slot", "Area", "No. Of Attendees", "Operating System", "Company Name", "Venue", "First Name", "Last Name", "Phone Number", "Email", "Remarks", "Model", "Serial No.", "Item No.", "Order No." };
                    var installs = db.MachineInstallation
                        .Where(x => EF.Functions.DateDiffDay(from, x.LastChangeTime) >= 0 && EF.Functions.DateDiffDay(x.LastChangeTime, to) >= 0)
                        .Where(x => problemCodes.Contains(x.ProblemCode))
                        //.Select(x => x.OrderId);
                        .Select(x => new { x.SerialNo, x.ItemNo });
                    var serialNos = installs.Select(x => x.SerialNo);
                    var data = db.RegistrationTimeslot
                        //.Where(x => installs.Contains(x.OrderId))
                        .Where(x => serialNos.Contains(x.SerialNo))
                        .ToList()
                        //.Where(x => installs.Any(i => i.SerialNo == x.SerialNo && i.ItemNo == x.ItemNo))
                        .OrderBy(x => x.ScheduleDate ?? x.TrainingDate)
                        .ThenBy(x => x.ScheduleTimeslot ?? x.TrainingTimeslot)
                        .ThenBy(x => x.ScheduleDistrict ?? x.District)
                        .Select(x => new string[]
                        {
                            string.Format("{0:yyyy-MM-dd}", x.ScheduleDate ?? x.TrainingDate),
                            x.ScheduleTimeslot ?? x.TrainingTimeslot,
                            districts.FirstOrDefault(d => d.Id.Equals(x.ScheduleDistrict ?? x.District))?.Label,
                            attendees.FirstOrDefault(a => a.Id == x.Attendees)?.Label,
                            os.FirstOrDefault(o => o.Id == x.OperatingSystem)?.Label,
                            x.CompanyName,
                            x.VenueAddress,
                            x.FirstName,
                            x.LastName,
                            x.PhoneNo,
                            x.Email,
                            x.Remarks,
                            x.ModelId,
                            x.SerialNo,
                            x.ItemNo,
                            x.OrderId
                        })
                        .ToList();
                    content = Export(header, data);
                }

                if (content != null)
                {
                    string fileName = GetFileName();
                    return File(content, MimeKit.MimeTypes.GetMimeType(fileName), fileName);
                }

            }
            return RedirectToPage("/Report/Index");
        }

        private string GetFileName()
        {
            var label = ReportSelectList.FirstOrDefault(x => x.Value == Report.Id).Text;
            var fileName = string.Format("{0:yyyyMMddHHmm}_{1}_{2:yyyy-MM-dd}-{3:yyyy-MM-dd}.xlsx", DateTime.Now, label, Report.From, Report.To);
            return fileName;
        }

        private byte[] Export(string[] header, List<string[]> data)
        {
            using (var stream = new MemoryStream())
            {
                var wb = new XSSFWorkbook();
                var sheet = wb.CreateSheet();
                int r = 0, c = 0;
                var row = sheet.CreateRow(r);
                foreach (var h in header)
                    row.CreateCell(c++).SetCellValue(h);

                foreach (var line in data)
                {
                    r++;
                    c = 0;
                    row = sheet.CreateRow(r);
                    foreach (var val in line)
                        row.CreateCell(c++).SetCellValue(val ?? "");
                }

                c = 0;
                foreach (var col in header)
                    sheet.AutoSizeColumn(c++);

                wb.Write(stream);
                return stream.ToArray();
            }
        }
    }
}