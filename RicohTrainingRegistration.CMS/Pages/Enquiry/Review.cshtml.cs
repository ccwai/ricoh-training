﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Enquiry
{
    public class ReviewModel : PageModel
    {
        private readonly RicohDB db;

        public ReviewModel(RicohDB db)
        {
            this.db = db;
        }

        [BindProperty]
        public EnquiryFormEditModel Enquiry { get; set; }

        public EnquiryForm Form { get; private set; }
        public MachineInstallation Install => Form?.MachineInstallation;
        public TrainingModel TrainingModel => Install?.TrainingModel;

        public IActionResult OnGet(int id)
        {
            Form = db.EnquiryForm.Include(x => x.MachineInstallation).ThenInclude(x => x.TrainingModel).SingleOrDefault(x => x.Id == id);
            if (Form == null)
                return NotFound();

            Enquiry = Form.Populate(new EnquiryFormEditModel());
            return Page();
        }

        public IActionResult OnPost(int id)
        {
            if (!ModelState.IsValid)
                return this.Fail(ModelState.FirstErrorMessage());

            Form = db.EnquiryForm.Include(x => x.MachineInstallation).ThenInclude(x => x.TrainingModel).SingleOrDefault(x => x.Id == id);
            if (Form == null)
                return this.Fail("Record not found.");

            Enquiry.Populate(Form);
            db.SaveChanges(JobLogTitle.SaveEnquiry);

            return this.Ok(
                message: "Save success.",
                redirect: Url.Page("/Enquiry/Review", new { id = Form.Id })
            );
        }
    }
}