﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Registration
{
    public class IndexModel : PageModel
    {
        private readonly RicohDB db;

        public IndexModel(RicohDB db)
        {
            this.db = db;
        }

        public List<SelectListItem> DistrictList { get; protected set; }

        public List<SelectListItem> AttendeeList { get; protected set; }

        public List<SelectListItem> OperatingSystemList { get; protected set; }
        
        public void OnGet()
        {
            DistrictList = db.GetDistrictList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();
            AttendeeList = db.GetAttendeeList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();
            OperatingSystemList = db.GetOperatingSystemList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();
        }
    }
}