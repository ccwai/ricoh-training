﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Registration
{
    public class EditModel : PageModel
    {
        private readonly AppSettings appSettings;
        private readonly RicohDB db;

        public EditModel(AppSettings appSettings, RicohDB db)
        {
            this.appSettings = appSettings;
            this.db = db;
        }

        [BindProperty]
        public RegistrationEditModel Registration { get; set; }

        public string ModelId { get; set; }

        public string ModelName { get; set; }

        [BindProperty]
        public string FormAction { get; set; }

        [BindProperty(Name = "fr", SupportsGet = true)]
        public string From { get; set; }

        public List<SelectListItem> DistrictList { get; protected set; }

        public List<SelectListItem> AttendeeList { get; protected set; }

        public List<SelectListItem> OperatingSystemList { get; protected set; }

        public IActionResult OnGet(int? id = null, int? slot = null)
        {
            Registration = new RegistrationEditModel();

            if (id.HasValue)
            {
                var reg = db.RegistrationTimeslot.Find(id);
                if (reg == null)
                    return NotFound();

                reg.PopulateExcept(Registration, "District", "TrainingDate", "TrainingTimeslot");
                Registration.District = reg.ScheduleDistrict ?? reg.District;
                Registration.TrainingDate = reg.ScheduleDate ?? reg.TrainingDate;
                Registration.TrainingTimeslot = reg.ScheduleTimeslot ?? reg.TrainingTimeslot;
                ModelId = reg.ModelId;
                ModelName = reg.ModelName;
            }
            else if (slot != null)
            {
                var timeslot = db.ScheduleItemSlot.Include(x => x.ScheduleItem).SingleOrDefault(x => x.Id == slot);
                if (timeslot != null)
                {
                    Registration.District = timeslot.ScheduleItem.District;
                    Registration.TrainingDate = timeslot.ScheduleItem.Date;
                    Registration.TrainingTimeslot = timeslot.Timeslot;
                }
            }

            DistrictList = db.GetDistrictList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();
            AttendeeList = db.GetAttendeeList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();
            OperatingSystemList = db.GetOperatingSystemList().Select(x => new SelectListItem { Value = x.Id, Text = x.Label }).ToList();

            return Page();
        }

        public IActionResult OnPost(int? id = null)
        {
            if (ModelState.IsValid)
            {
                var serialNo = Registration.SerialNo;
                var install = db.MachineInstallation.Include(x => x.MachineInstallationDate).LatestOrDefault(serialNo);

                if (install == null)
                    return this.Fail("Serial No. not found.");

                if (install.MachineInstallationDate?.IsFreeTraining != true)
                {
                    // CR - Allow to create registration record if problem code is MCT
                    var entitled = false;
                    if (db.GetDefaultSettings().CompletedTrainingProblemCodes.Contains(install.ProblemCode))
                    {
                        var modelId = install.ModelId;
                        entitled = db.TrainingModel.Find(modelId)?.IsFreeTraining == true;
                    }

                    if (!entitled)
                        return this.Fail("This model is not entitled.");
                }

                var orderId = install.OrderId;
                Database.Registration reg = null;
                if (id.HasValue)
                {
                    reg = db.Registration.Find(id);
                    if (reg == null)
                        return this.Ok(redirect: Url.Page("/Registration/Edit", new { id }));
                }
                else
                {
                    reg = db.Registration.SingleOrDefault(x => x.OrderId == orderId);

                    if (reg == null)
                        reg = db.Registration.Add(new Database.Registration
                        {
                            OrderId = install.OrderId,
                            SerialNo = install.SerialNo,
                            Status = RegistrationStatus.Pending
                        }).Entity;
                }

                var trainingRemind = false;
                if ("submit".EqualsIgnoreCase(FormAction) || "refresh".EqualsIgnoreCase(FormAction))
                {
                    var district = Registration.District;
                    var date = Registration.TrainingDate;
                    var time = Registration.TrainingTimeslot;
                    //var items = db.GetTodayScheduleItems(install.ModelId, district, out DateTime startDate, out DateTime endDate);

                    /*if (date.HasValue && date.Value.DayOfWeek != DayOfWeek.Sunday && date.Value.DayOfWeek != DayOfWeek.Saturday && !(date >= startDate && date <= endDate))
                        return this.Fail("Training date beyond it's registration deadline");*/

                    var today = DateTime.Today;
                    var items = db.ScheduleItem.Include(x => x.ScheduleItemSlot).ThenInclude(x => x.Registration).Where(x => x.Date >= today && x.District == district);
                    var slot = items.SelectMany(x => x.ScheduleItemSlot).FirstOrDefault(x =>
                        x.ScheduleItem.District == district
                        && x.ScheduleItem.Date == date
                        && x.Timeslot == time
                        && x.IsCustom == x.ScheduleItem.IsUseCustom
                        && (x.RegistrationId == null || x.Registration.OrderId == orderId)
                    );

                    if (slot == null && Registration.TrainingDatePreferable != true)
                        return this.Fail("Timeslot not found or invalid");

                    db.ScheduleItemSlot.Where(x => x.Registration.OrderId == orderId).ToList()
                        .ForEach(x => x.RegistrationId = null);

                    if (slot != null)
                    {
                        slot.RegistrationId = reg.Id;
                        reg.Status = "refresh".EqualsIgnoreCase(FormAction) ? RegistrationStatus.Rescheduled : RegistrationStatus.Submitted;
                        trainingRemind = true;
                    }
                }
                else if ("cancel".EqualsIgnoreCase(FormAction))
                {
                    db.ScheduleItemSlot.Where(x => x.Registration.OrderId == orderId).ToList()
                        .ForEach(x => x.RegistrationId = null);

                    reg.Status = RegistrationStatus.Cancelled;
                }

                Registration.PopulateExcept(reg, "OrderId", "SerialNo", "Status");

                var entry = db.ChangeTracker.Entries<Database.Registration>().Single();
                var modified = entry.Properties.Any(x => x.IsModified);

                db.SaveChanges(JobLogTitle.SaveRegistration);

                if (modified)
                {
                    // enqueue reminder email
                    if (trainingRemind)
                    {
                        var regId = reg.Id;
                        var r = db.RegistrationTimeslot.Find(regId);
                        db.EnqueueTraningReminder(r, true);
                        db.SaveChanges();
                    }

                    // send email
                    SendEmail(reg.Id, FormAction);
                    db.SaveChanges();
                }

                return this.Ok(
                    message: "Save success.",
                    redirect: Url.Page("/Registration/Edit", new { id = reg.Id })
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }

        private EmailRequest SendEmail(int id, string formAction)
        {
            if (new[] { "submit", "refresh", "cancel" }.Any(x => x.EqualsIgnoreCase(formAction)))
            {
                var r = db.RegistrationTimeslot.Find(id);
                var sn = SecurityHelper.Encrypt(RegistrationPassphrase.Register, r.SerialNo);

                var builder = new UriBuilder(appSettings.RegistrationUrl);
                var qs = HttpUtility.ParseQueryString("");

                if ("submit".EqualsIgnoreCase(formAction))
                {
                    builder.Path = "/invite/reschedule";
                    qs["sn"] = SecurityHelper.Encrypt(RegistrationPassphrase.Reschedule, r.SerialNo);
                    builder.Query = qs.ToString();

                    return db.AddEmailRequest(
                        EmailTemplateId.REG_CONFIRM,
                        new
                        {
                            CUSTOMER_NAME = r.CompanyName,
                            CONTACT_NAME = r.ContactName,
                            PHONE_NO = r.PhoneNo,
                            MODEL_NO = r.ModelId,
                            SERIAL_NO = r.SerialNo,
                            TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", r.ScheduleDate ?? r.TrainingDate),
                            TRAINING_TIMESLOT = r.ScheduleTimeslot ?? r.TrainingTimeslot,
                            VENUE_ADDRESS = r.VenueAddress?.Trim(),
                            LOGIN_URL = builder.Uri.AbsoluteUri,
                            DISTRICT = db.GetDistrictList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.ScheduleDistrict ?? r.District))?.Label,
                            NO_SUITABLE_CHOICE = r.TrainingDatePreferable == true ? "Y" : "N",
                            NO_SUITABLE_CHOICE_REMARK = string.IsNullOrWhiteSpace(r.TrainingDateRemarks) ? "-" : r.TrainingDateRemarks,
                            ATTENDEES = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Label,
                            ATTENDEES_NOTE = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Note,
                            OS = db.GetOperatingSystemList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.OperatingSystem))?.Label,
                            COMPANY_NAME = r.CompanyName,
                            EMAIL = r.Email,
                            REMARKS = string.IsNullOrWhiteSpace(r.Remarks) ? "-" : r.Remarks
                        },
                        r.Email,
                        r.ContactName
                    );
                }

                if ("refresh".EqualsIgnoreCase(formAction))
                {
                    builder.Path = "/invite/reschedule";
                    qs["sn"] = SecurityHelper.Encrypt(RegistrationPassphrase.Reschedule, r.SerialNo);
                    builder.Query = qs.ToString();

                    return db.AddEmailRequest(
                        EmailTemplateId.TRAINING_RESCHEDULE,
                        new
                        {
                            CUSTOMER_NAME = r.CompanyName,
                            CONTACT_NAME = r.ContactName,
                            PHONE_NO = r.PhoneNo,
                            MODEL_NO = r.ModelId,
                            SERIAL_NO = r.SerialNo,
                            TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", r.ScheduleDate ?? r.TrainingDate),
                            TRAINING_TIMESLOT = r.ScheduleTimeslot ?? r.TrainingTimeslot,
                            VENUE_ADDRESS = r.VenueAddress?.Trim(),
                            LOGIN_URL = builder.Uri.AbsoluteUri,
                            DISTRICT = db.GetDistrictList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.ScheduleDistrict ?? r.District))?.Label,
                            NO_SUITABLE_CHOICE = r.TrainingDatePreferable == true ? "Y" : "N",
                            NO_SUITABLE_CHOICE_REMARK = string.IsNullOrWhiteSpace(r.TrainingDateRemarks) ? "-" : r.TrainingDateRemarks,
                            ATTENDEES = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Label,
                            ATTENDEES_NOTE = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Note,
                            OS = db.GetOperatingSystemList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.OperatingSystem))?.Label,
                            COMPANY_NAME = r.CompanyName,
                            EMAIL = r.Email,
                            REMARKS = string.IsNullOrWhiteSpace(r.Remarks) ? "-" : r.Remarks
                        },
                        r.Email,
                        r.ContactName
                    );
                }

                if ("cancel".EqualsIgnoreCase(formAction))
                {
                    builder.Path = "/invite";
                    qs["sn"] = SecurityHelper.Encrypt(RegistrationPassphrase.Register, r.SerialNo);
                    builder.Query = qs.ToString();
                    var orderId = r.OrderId;
                    var regDate = db.MachineInstallationDate.FirstOrDefault(x => x.OrderId == orderId);

                    return db.AddEmailRequest(
                        EmailTemplateId.TRAINING_CANCEL,
                        new
                        {
                            CUSTOMER_NAME = r.CompanyName,
                            CONTACT_NAME = r.ContactName,
                            PHONE_NO = r.PhoneNo,
                            MODEL_NO = r.ModelId,
                            SERIAL_NO = r.SerialNo,
                            TRAINING_DATE = string.Format("{0:yyyy/MM/dd}", r.ScheduleDate ?? r.TrainingDate),
                            TRAINING_TIMESLOT = r.ScheduleTimeslot ?? r.TrainingTimeslot,
                            VENUE_ADDRESS = r.VenueAddress?.Trim(),
                            LOGIN_URL = builder.Uri.AbsoluteUri,
                            REG_DATE = string.Format("{0:yyyy/MM/dd}", regDate?.FreeTrainingEndDate),
                            DISTRICT = db.GetDistrictList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.ScheduleDistrict ?? r.District))?.Label,
                            NO_SUITABLE_CHOICE = r.TrainingDatePreferable == true ? "Y" : "N",
                            NO_SUITABLE_CHOICE_REMARK = string.IsNullOrWhiteSpace(r.TrainingDateRemarks) ? "-" : r.TrainingDateRemarks,
                            ATTENDEES = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Label,
                            ATTENDEES_NOTE = db.GetAttendeeList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.Attendees))?.Note,
                            OS = db.GetOperatingSystemList().FirstOrDefault(x => x.Id.EqualsIgnoreCase(r.OperatingSystem))?.Label,
                            COMPANY_NAME = r.CompanyName,
                            EMAIL = r.Email,
                            REMARKS = string.IsNullOrWhiteSpace(r.Remarks) ? "-" : r.Remarks
                        },
                        r.Email,
                        r.ContactName
                    );
                }
            }
            return null;
        }
    }
}