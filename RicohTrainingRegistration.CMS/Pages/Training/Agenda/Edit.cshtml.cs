﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Agenda
{
    public class EditModel : PageModel
    {
        private readonly RicohDB DB;
        private readonly IStorageProvider Storage;

        public EditModel(RicohDB db, IStorageProvider storage)
        {
            DB = db;
            Storage = storage;
        }

        [BindProperty]
        public TrainingAgendaEditModel Agenda { get; set; }

        public List<SelectListItem> ModelList { get; internal set; }
        public List<MediaModel> MediaList { get; internal set; }

        public IActionResult OnGet(int? id)
        {
            Agenda = new TrainingAgendaEditModel();
            if (id > 0)
            {
                var agenda = DB.TrainingAgenda.Include(x => x.TrainingModel).SingleOrDefault(x => x.Id == id);
                if (agenda != null)
                {
                    agenda.Populate(Agenda);
                    Agenda.Models = agenda.TrainingModel.Select(x => x.ModelId).ToList();

                }
                else
                    return NotFound();
            }

            var agendaId = Agenda.Id;

            // model drop down list
            var models = DB.TrainingModel.ToList();
            ModelList = models.Select(x => new SelectListItem
            {
                Text = $"{x.ModelId} - {x.ModelName}",
                Value = x.ModelId,
                Disabled = x.TrainingAgendaId.HasValue && x.TrainingAgendaId != agendaId
            }).ToList();

            // media list
            if (agendaId > 0)
                MediaList = DB.TrainingAgendaMedia.Include(x => x.Media)
                    .Where(x => x.TrainingAgendaId == agendaId)
                    .ToList()
                    .Select(x => new MediaModel
                    {
                        Id = x.Media.Id,
                        FileName = x.Media.FileName,
                        Url = Url.Action("Get", "Media", new { x.Media.Id }),
                        ContentType = x.Media.ContentType,
                        ContentLength = x.Media.ContentLength,
                        Language = x.Language
                    }).ToList();
            else
                MediaList = new List<MediaModel>();

            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (ModelState.IsValid)
            {
                TrainingAgenda agenda;
                if (id > 0)
                {
                    agenda = DB.TrainingAgenda.Include(x => x.TrainingAgendaMedia).ThenInclude(x => x.Media).SingleOrDefault(x => x.Id == id);
                    if (agenda == null)
                        return this.Fail("Record not found.");
                }
                else
                    agenda = DB.TrainingAgenda.Add(new TrainingAgenda()).Entity;

                var agendaId = agenda.Id;

                // training models
                var models = Agenda.Models ?? new List<string>();
                var modelList = DB.TrainingModel.Where(x => x.TrainingAgendaId == null || x.TrainingAgendaId == agendaId).ToList();
                var trainingModels = modelList.Where(x => models.Contains(x.ModelId)).ToList();
                var removeTrainingModels = modelList.Where(x => !models.Contains(x.ModelId)).ToList();

                // attachments
                List<TrainingAgendaMedia> attachments = Enumerable.Empty<TrainingAgendaMedia>()
                    .Concat(CreateAttachments(agenda, Agenda.AttachmentsEng, TrainingAgendaMedia.English))
                    .Concat(CreateAttachments(agenda, Agenda.AttachmentsChi, TrainingAgendaMedia.Chinese))
                    .ToList();

                // remove attachments
                var removeAttachments = agenda.TrainingAgendaMedia.Where(x => Agenda.RemoveAttachments?.Contains(x.MediaId) == true
                    //|| (x.Language == TrainingAgendaMedia.English && attachments.Any(a => a.Language == TrainingAgendaMedia.English))
                    //|| (x.Language == TrainingAgendaMedia.Chinese && attachments.Any(a => a.Language == TrainingAgendaMedia.Chinese))
                ).ToList();

                // update db records
                Agenda.PopulateExcept(agenda, "Id");
                trainingModels.ForEach(x => x.TrainingAgenda = agenda);
                removeTrainingModels.ForEach(x => x.TrainingAgenda = null);
                DB.TrainingAgendaMedia.AddRange(attachments);
                DB.TrainingAgendaMedia.RemoveRange(removeAttachments);
                DB.Media.RemoveRange(removeAttachments.Select(x => x.Media));
                DB.SaveChanges(JobLogTitle.SaveTrainingAgenda);

                return this.Ok(
                    message: "Save success.",
                    redirect: Url.Page("/Training/Agenda/Edit", new { id = agenda.Id })
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }

        private IEnumerable<TrainingAgendaMedia> CreateAttachments(TrainingAgenda agenda, IEnumerable<IFormFile> files, string language)
        {
            files = files ?? Enumerable.Empty<IFormFile>();
            int i = 1;
            foreach (var file in files)
            {
                var media = new Media
                {
                    FileName = file.FileName,
                    Path = string.Join("/", "agenda", Path.GetRandomFileName()),
                    ContentLength = (int)file.Length,
                    ContentType = file.ContentType
                };
                if (Storage.Create(media.Path, file.OpenReadStream()))
                    yield return new TrainingAgendaMedia
                    {
                        TrainingAgenda = agenda,
                        Media = media,
                        Language = language,
                        DisplayOrder = i++
                    };
            }
        }
    }
}