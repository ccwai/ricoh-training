﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Holiday
{
    public class EditModel : PageModel
    {
        private readonly RicohDB DB;

        public EditModel(RicohDB db)
        {
            DB = db;
        }

        [BindProperty]
        public HolidayEditModel Holiday { get; set; }

        public IActionResult OnGet(int? id)
        {
            Holiday = new HolidayEditModel();
            if (id > 0)
            {
                var holiday = DB.Holiday.Find(id);
                if (holiday != null)
                {
                    Holiday.Id = holiday.Id;
                    Holiday.Name = holiday.Name;
                    Holiday.StartDate = holiday.StartDate;
                    Holiday.EndDate = holiday.EndDate;
                    Holiday.Type = holiday.Type;
                }
                else
                    return NotFound();
            }
            return Page();
        }

        public IActionResult OnPost(int? id)
        {
            if (ModelState.IsValid)
            {
                if (Holiday.StartDate > Holiday.EndDate)
                    return this.Fail("Date is not valid.");

                Database.Holiday model;
                if (id > 0)
                {
                    model = DB.Holiday.Find(id);
                    if (model == null)
                        return this.Fail("Record not found.");
                }
                else
                    model = DB.Holiday.Add(new Database.Holiday { Type = HolidayType.Manual }).Entity;

                model.Name = Holiday.Name;
                model.StartDate = Holiday.StartDate.Value;
                model.EndDate = Holiday.EndDate.Value;

                DB.SaveChanges(JobLogTitle.SaveHoliday);
                return this.Ok(
                    message: "Save Success",
                    redirect: Url.Page($"/Training/Holiday/Edit", new { id = model.Id })
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }
    }
}