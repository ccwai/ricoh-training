﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Schedule
{
    public class DefaultModel : PageModel
    {
        private readonly RicohDB db;
        private readonly ILogger<DefaultModel> logger;

        public DefaultModel(RicohDB db, ILogger<DefaultModel> logger)
        {
            this.db = db;
            this.logger = logger;
        }

        public List<ScheduleDefault> ScheduleDefaults { get; private set; }

        public void OnGet()
        {
            ScheduleDefaults = db.ScheduleDefault.OrderBy(x => x.DayOfWeek).ThenBy(x => x.Timeslot).ToList();

            var status = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
            var reg = db.RegistrationTimeslot.OrderByDescending(x => x.ScheduleDate).FirstOrDefault(x => status.Contains(x.Status));
            var startDate = reg?.ScheduleDate;
            if (startDate.HasValue)
                StartDate = startDate.Value.AddDays(1);
        }

        [BindProperty]
        public Dictionary<string, List<ScheduleDefaultEditModel>> Data { get; set; }

        [BindProperty]
        public DateTime? StartDate { get; set; }

        public IActionResult OnPost()
        {
            if (Data == null)
                return this.Fail("No data submitted.");

            if (Data.Keys.Select(x => new ScheduleItemSlot { Timeslot = x }).Any(x => !(x.EndTime > x.StartTime)))
                return this.Fail("There are errors in submitted data.");

            var slots = db.ScheduledSession.Where(x => EF.Functions.DateDiffDay(StartDate, x.Date) >= 0).ToList();

            var hasStartDate = StartDate.HasValue;
            // check registered schedule
            if (slots.Any(x => x.RegistrationId.HasValue))
                return this.Fail("There are registered schedules on or after the selected date. Please select another date.");

            // check if have any custom schedule training
            var hasCustom = slots.Any(x => x.IsCustom);
            var force = Request.Query["force"].Any(x => x == "1");
            if (!force && hasStartDate && hasCustom)
                return new JsonResult(new
                {
                    waitForConfirm = true,
                    message = string.Format("There are custommized schedules after the selected date. Confirm the overwrite?")
                });

            logger.LogInformation("START Update schedule default from {0:yyyy/MM/dd}", StartDate);
            var startTime = DateTime.Now;
            var list = new List<ScheduleDefault>();
            foreach (var key in Data.Keys)
            {
                var items = Data[key].GroupBy(r => (DayOfWeek)r.DayOfWeek);
                foreach (var group in items)
                {
                    var dayOfWeek = group.Key;
                    var entity = new ScheduleDefault
                    {
                        Timeslot = key,
                        DayOfWeek = (int)dayOfWeek
                    };
                    entity.Hk = group.FirstOrDefault(g => g.District.EqualsIgnoreCase(District.Hk))?.Quota ?? 0;
                    entity.Kln = group.FirstOrDefault(g => g.District.EqualsIgnoreCase(District.Kln))?.Quota ?? 0;
                    entity.Nt = group.FirstOrDefault(g => g.District.EqualsIgnoreCase(District.Nt))?.Quota ?? 0;
                    list.Add(entity);
                }
            }

            var data = db.ScheduleDefault.ToList();
            db.ScheduleDefault.RemoveRange(data.Where(r => !list.Any(x => x.DayOfWeek == r.DayOfWeek && x.Timeslot == r.Timeslot)));

            foreach (var item in list)
            {
                var entity = data.Find(r => r.DayOfWeek == item.DayOfWeek && r.Timeslot == item.Timeslot);
                if (entity == null)
                    entity = db.ScheduleDefault.Add(item).Entity;

                entity.Hk = item.Hk;
                entity.Kln = item.Kln;
                entity.Nt = item.Nt;
            }

            // remove all generated schedule
            var generatedItems = new List<ScheduleItem>();
            if (hasStartDate)
            {
                var itemsId = slots.Select(x => x.ItemId).ToList();
                var items = db.ScheduleItem.Include(x => x.ScheduleItemSlot).Where(x => itemsId.Contains(x.Id)).ToList();
                db.ScheduleItem.RemoveRange(items);

                // regenerate existing scheduled month
                var year = StartDate.Value.Year;
                var month = StartDate.Value.Month;
                var holidays = db.Holiday.ToList();
                var masters = db.ScheduleMaster.Where(x => (x.Year > year) || (x.Year == year && x.Month >= month)).ToList();
                foreach (var master in masters)
                {
                    var newItems = list.GenerateScheduleItems(master, holidays).ToList();
                    generatedItems.AddRange(newItems.Where(x => x.Date >= StartDate));
                }

                db.ScheduleItem.AddRange(generatedItems);
            }

            // log
            var log = db.JobLog.Add(new JobLog
            {
                Title = "Update Traning Schedule Default",
                StartTime = startTime
            }).Entity;

            db.Database.SetCommandTimeout(TimeSpan.FromMinutes(15));
            int changes = db.SaveChanges();

            logger.LogInformation("END Update schedule default from {0:yyyy/MM/dd} ({1}s elapsed, {2} records affected)", StartDate, (DateTime.Now - startTime).TotalSeconds, changes);

            // log result
            StringBuilder sb = new StringBuilder();
            sb.Append($"Selected start date: {StartDate?.ToString("yyyy/MM/dd") ?? "--EMPTY--"}");
            sb.Append($"; {generatedItems.Count} date schedules updated");
            sb.Append($"; {changes - 1} record(s) affected");
            log.EndTime = DateTime.Now;
            log.Message = sb.ToString();
            db.SaveChanges();

            // response
            sb.Clear();
            sb.AppendLine("Save success.");

            if (generatedItems.Any())
                sb.AppendLine("(Existing schedules have been updated.)");

            return this.Ok(
                message: sb.ToString(),
                redirect: Url.Page("/Training/Schedule/Default")
            );
        }
    }
}