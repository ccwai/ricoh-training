﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Schedule.Custom
{
    public class EditModel : PageModel
    {
        private readonly RicohDB db;
        private readonly AppSettings appSettings;

        public EditModel(RicohDB db, AppSettings appSettings)
        {
            this.db = db;
            this.appSettings = appSettings;
        }

        [BindProperty]
        public CustomScheduleEditModel Data { get; set; }

        [BindProperty(Name = "fr", SupportsGet = true)]
        public string From { get; set; }

        public IActionResult OnGet(int? id = null, string district = null, DateTime? date = null)
        {
            Data = new CustomScheduleEditModel();

            if (id > 0)
            {
                var model = db.ScheduleItem.Include(x => x.ScheduleItemSlot).ThenInclude(x => x.Registration).FirstOrDefault(x => x.Id == id);
                if (model == null)
                    return NotFound();

                Data.Id = model.Id;
                Data.Date = model.Date;
                Data.District = model.District;
                Data.IsUseCustom = model.IsUseCustom;
                Data.Remark = model.Remark;
                Data.DefaultTimeslots = model.ScheduleItemSlot.Defaults()
                    .GroupBy(x => x.Timeslot)
                    .Select(x => new CustomScheduleSlotEditModel
                    {
                        Timeslot = x.Key,
                        Quota = x.Count(),
                        Registrations = x.Where(r => r.Registration != null).Select(r => new DtRowAction
                        {
                            Label = r.Registration.SerialNo,
                            Url = Url.Page("/Registration/Edit", new { id = r.Registration.Id })
                        }).ToList()
                    })
                    .OrderBy(x => x.Timeslot)
                    .ToList();
                Data.CustomTimeslots = model.ScheduleItemSlot.Customs()
                    .GroupBy(x => x.Timeslot)
                    .Select(x => new CustomScheduleSlotEditModel
                    {
                        Timeslot = x.Key,
                        Quota = x.Count(),
                        Registrations = x.Where(r => r.Registration != null).Select(r => new DtRowAction
                        {
                            Label = r.Registration.SerialNo,
                            Url = Url.Page("/Registration/Edit", new { id = r.Registration.Id })
                        }).ToList()
                    })
                    .OrderBy(x => x.Timeslot)
                    .ToList();
            }
            else
            {
                Data.District = appSettings.Districts.FirstOrDefault(x => x.EqualsIgnoreCase(district)) ?? District.Hk;
                Data.Date = date;
            }

            return Page();
        }

        public IActionResult OnPost(int? id = null)
        {
            if (!ModelState.IsValid)
                return this.Fail(ModelState.FirstErrorMessage());

            ScheduleItem model;
            if (id > 0)
            {
                model = db.ScheduleItem.Include(x => x.ScheduleItemSlot).FirstOrDefault(x => x.Id == id);
                if (model == null)
                    return this.Fail("Record not found", 404);
            }
            else
            {
                var date = Data.Date.Value;
                var district = Data.District;
                model = db.ScheduleItem.FirstOrDefault(x => x.Date == date && x.District == district);
                if (model == null)
                {
                    var year = date.Year;
                    var month = date.Month;
                    var master = db.ScheduleMaster.FirstOrDefault(x => x.Year == year && x.Month == month);
                    master = master ?? db.Add(new ScheduleMaster { Year = year, Month = month }).Entity;
                    model = db.Add(new ScheduleItem
                    {
                        ScheduleMaster = master,
                        Date = date,
                        Day = date.Day,
                        District = district
                    }).Entity;
                }
            }

            if (Data.IsUseCustom != true && model.IsUseCustom && model.ScheduleItemSlot.Any(x => x.RegistrationId.HasValue && x.IsCustom == model.IsUseCustom))
                return this.Fail("Cannot cancel the use of custom timeslots.");

            model.IsUseCustom = Data.IsUseCustom == true;

            // rebuild custom timeslots
            db.RemoveRange(model.ScheduleItemSlot.Where(x => !x.RegistrationId.HasValue).Customs());
            if (Data.CustomTimeslots?.Any() == true)
            {
                foreach (var slot in Data.CustomTimeslots)
                {
                    if (slot.Quota < 0)
                        continue;

                    if (model.ScheduleItemSlot.Any(x => x.Timeslot == slot.Timeslot && x.IsCustom && x.RegistrationId.HasValue))
                        continue;

                    var items = Enumerable.Range(1, slot.Quota).Select(seq => new ScheduleItemSlot
                    {
                        ScheduleItem = model,
                        Timeslot = slot.Timeslot,
                        Seq = seq,
                        IsCustom = true
                    }).Where(x => x.EndTime > x.StartTime);
                    db.AddRange(items);
                }
            }

            db.SaveChanges(JobLogTitle.SaveCustomTimeslot);

            return this.Ok(message: "Save success.", redirect: Url.Page("/Training/Schedule/Custom/Edit", new { id = model.Id }));
        }
    }
}