﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Schedule
{
    public class IndexModel : PageModel
    {
        private readonly RicohDB db;
        private readonly ILogger<IndexModel> logger;

        public IndexModel(RicohDB db, ILogger<IndexModel> logger)
        {
            this.db = db;
            this.logger = logger;
            this.Today = DateTime.Today;
        }

        public DateTime Today { get; }
        public DateTime CurrentMonth { get; private set; }
        public ScheduleMaster Data { get; private set; }
        public List<Database.Holiday> Holidays { get; private set; }

        public void OnGet()
        {
            int year, month;
            BindCurrentMonth(out year, out month);

            Data = db.ScheduleMaster.Include(x => x.ScheduledSessions)
                .FirstOrDefault(x => x.Year == year && x.Month == month);

            Holidays = db.Holiday.Where(x => EF.Functions.DateDiffMonth(x.StartDate, CurrentMonth) == 0 && EF.Functions.DateDiffMonth(CurrentMonth, x.EndDate) == 0).ToList();
        }

        public IActionResult OnPost()
        {
            int year, month;
            BindCurrentMonth(out year, out month);
            string redirect = Url.Page("/Training/Schedule/Index", new { year, month });

            if (db.ScheduleMaster.Any(x => x.Year == year && x.Month == month))
                return this.Ok(redirect: redirect);

            logger.LogInformation("START Generate schedule {0}/{1}", year, month);
            var startTime = DateTime.Now;
            var defaults = db.ScheduleDefault.ToList();
            var holidays = db.Holiday.Where(x => EF.Functions.DateDiffMonth(x.StartDate, CurrentMonth) == 0 && EF.Functions.DateDiffMonth(CurrentMonth, x.EndDate) == 0).ToList();
            var master = db.Add(new ScheduleMaster { Year = year, Month = month }).Entity;
            var items = defaults.GenerateScheduleItems(master, holidays);
            db.AddRange(items);
            db.Database.SetCommandTimeout(TimeSpan.FromMinutes(15));
            var results = db.SaveChanges();
            logger.LogInformation("END Generat schedule {0}/{1} ({2}s elapsed, {3} records affected)", year, month, (DateTime.Now - startTime).TotalSeconds, results);

            return this.Ok(
                message: "Generate success.",
                redirect: redirect
            );
        }

        private void BindCurrentMonth(out int year, out int month)
        {
            var y = Request.Query["year"].ToString();
            var m = Request.Query["month"].ToString();
            if (!int.TryParse(y, out year) || !int.TryParse(m, out month))
            {
                year = Today.Year;
                month = Today.Month;
            }
            CurrentMonth = new DateTime(year, month, 1);
        }
    }
}