﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Training.Model
{
    public class IndexModel : PageModel
    {
        private readonly RicohDB db;

        public IndexModel(RicohDB db)
        {
            this.db = db;
        }

        public DateTime? LastRetrievedDate { get; private set; }

        public void OnGet()
        {
            var title = JobLogTitle.RetrieveModel;
            var log = db.JobLog.Where(x => x.Title == title).OrderByDescending(x => x.EndTime).FirstOrDefault();
            LastRetrievedDate = log?.EndTime;
        }
    }
}