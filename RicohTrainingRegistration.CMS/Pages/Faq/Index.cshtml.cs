﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Faq
{
    public class IndexModel : PageModel
    {
        private readonly RicohDB DB;
        private readonly IStorageProvider Storage;

        public IndexModel(RicohDB db, IStorageProvider storage)
        {
            DB = db;
            Storage = storage;
        }

        [BindProperty]
        public List<IFormFile> DocumentsEng { get; set; }
        
        [BindProperty]
        public List<IFormFile> DocumentsChi { get; set; }
        
        [BindProperty]
        public List<int> RemoveDocuments { get; set; }

        public List<MediaModel> MediaList { get; internal set; }

        public void OnGet()
        {
            MediaList = DB.FaqDocument.Include(x => x.Media)
                .ToList()
                .Select(x => new MediaModel
                {
                    Id = x.Media.Id,
                    FileName = x.Media.FileName,
                    Url = Url.Action("Get", "Media", new { x.Media.Id }),
                    ContentType = x.Media.ContentType,
                    ContentLength = x.Media.ContentLength,
                    Language = x.Language
                }).ToList();
        }

        public IActionResult OnPost()
        {
            var documents = DB.FaqDocument.Include(x => x.Media).ToList();

            // add documents
            var addDocuments = Enumerable.Empty<FaqDocument>()
                .Concat(CreateDocuments(DocumentsEng, "en-us"))
                .Concat(CreateDocuments(DocumentsChi, "zh-hk"))
                .ToList();

            // remove attachments
            var removeDocuments = documents.Where(x => RemoveDocuments?.Contains(x.MediaId) == true || addDocuments.Any(a => a.Language == x.Language)).ToList();
            
            DB.FaqDocument.AddRange(addDocuments);
            DB.FaqDocument.RemoveRange(removeDocuments);
            DB.Media.RemoveRange(removeDocuments.Select(x => x.Media));
            DB.SaveChanges(JobLogTitle.SaveFaq);

            return this.Ok(
                message: "Save success.",
                redirect: Url.Page("/Faq/Index")
            ); ;
        }

        private IEnumerable<FaqDocument> CreateDocuments(IEnumerable<IFormFile> files, string language)
        {
            foreach (var file in files)
            {
                var media = new Media
                {
                    FileName = file.FileName,
                    Path = string.Join("/", "faq", Path.GetRandomFileName()),
                    ContentLength = (int)file.Length,
                    ContentType = file.ContentType
                };
                if (Storage.Create(media.Path, file.OpenReadStream()))
                    yield return new FaqDocument
                    {
                        UploadDate = DateTime.Now,
                        Media = media,
                        Language = language
                    };
            }
        }
    }
}