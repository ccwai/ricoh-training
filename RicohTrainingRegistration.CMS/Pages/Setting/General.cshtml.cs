﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Setting
{
    public class GeneralModel : PageModel
    {
        private readonly RicohDB db;
        private readonly IStorageProvider storage;
        private readonly AppSettings appSettings;

        public GeneralModel(RicohDB db, IStorageProvider storage, AppSettings appSettings)
        {
            this.db = db;
            this.storage = storage;
            this.appSettings = appSettings;
        }

        [BindProperty]
        public SettingEditModel Setting { get; set; }

        [BindProperty]
        public List<IFormFile> HintImage { get; set; }

        [BindProperty]
        public bool? RemoveHintImage { get; set; }

        public MediaModel HintMedia { get; internal set; }

        public void OnGet()
        {
            var settings = db.GetDefaultSettings();
            Setting = settings.PopulateComplex(new SettingEditModel());

            var path = SystemSettings.SerialNoHintImagePath;
            var media = db.Media.FirstOrDefault(x => x.Path == path);
            if (media != null)
                HintMedia = new MediaModel
                {
                    Id = media.Id,
                    FileName = media.FileName,
                    Url = Url.Action("Get", "Media", new { media.Id }),
                    ContentType = media.ContentType,
                    ContentLength = media.ContentLength
                };

            // footer links
            var max = appSettings.MaxFooterLinks;
            Setting.FooterLinks = (Setting.FooterLinks ?? new List<FooterLink>())
                .Concat(Enumerable.Range(0, max).Select(x => new FooterLink()))
                .Take(max)
                .ToList();
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var settings = Setting.PopulateComplex(new SystemSettings());

                var media = db.GetSerialNoHintMedia();
                if (HintImage?.Any() == true)
                {
                    var file = HintImage.First();
                    var path = SystemSettings.SerialNoHintImagePath;
                    if (storage.Create(path, file.OpenReadStream()))
                    {
                        if (media == null)
                            media = db.Media.Add(new Media()).Entity;

                        media.FileName = file.FileName;
                        media.Path = path;
                        media.ContentLength = (int)file.Length;
                        media.ContentType = file.ContentType;
                    }
                }
                else if (RemoveHintImage == true)
                {
                    if (media != null)
                        db.Media.Remove(media);
                }

                db.UpdateSystemSettings(settings);
                db.SaveChanges(JobLogTitle.SaveGeneralSettings);

                return this.Ok(
                    message: "Save Success",
                    redirect: Url.Page($"/Setting/General")
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }
    }
}