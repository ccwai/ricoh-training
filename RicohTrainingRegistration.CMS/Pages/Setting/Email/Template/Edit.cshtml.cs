﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Setting.Email.Template
{
    public class EditModel : PageModel
    {
        private readonly ILogger<EditModel> logger;
        private readonly RicohDB db;

        public EditModel(ILogger<EditModel> logger, RicohDB db)
        {
            this.logger = logger;
            this.db = db;
        }

        [BindProperty]
        public EmailTemplateEditModel Template { get; set; }

        public IActionResult OnGet(string id)
        {
            var template = db.EmailTemplate.Find(id);
            if (template == null)
                return NotFound();

            Template = template.Populate(new EmailTemplateEditModel());
            return Page();
        }

        public IActionResult OnPost(string id)
        {
            if (ModelState.IsValid)
            {
                var template = db.EmailTemplate.Find(id);
                if (template == null)
                    return this.Fail("Record not found.");

                Template.PopulateExcept(template, "Id");
                db.SaveChanges(JobLogTitle.SaveEmailTemplate);

                return this.Ok(
                    message: "Save Success",
                    redirect: Url.Page("/Setting/Email/Template/Edit", new { id })
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }
    }
}