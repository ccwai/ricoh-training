﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Setting.Email.Template
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> logger;
        private readonly RicohDB db;

        public IndexModel(ILogger<IndexModel> logger, RicohDB db)
        {
            this.logger = logger;
            this.db = db;
        }

        public object Templates { get; internal set; }

        public void OnGet()
        {
            Templates = db.EmailTemplate.OrderBy(x => x.DisplayOrder).ToList()
                .Select(x => new
                {
                    id = x.Id,
                    name = x.Name,
                    sendToTrainers = x.SendToTrainers,
                    lastUpdated = x.UpdatedDate,
                    actions = DtRowAction.Build(
                        DtRowAction.Edit(Url.Page("/Setting/Email/Template/Edit", new { id = x.Id }))
                    )
                }).ToList();
        }
    }
}