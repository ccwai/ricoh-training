﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using DotNetLib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages.Setting.Email
{
    public class AddressModel : PageModel
    {
        private readonly RicohDB DB;

        public AddressModel(RicohDB db)
        {
            this.DB = db;
        }

        [BindProperty]
        public EmailSettingEditModel Setting { get; set; }

        public void OnGet()
        {
            var list = DB.SystemEmail.ToList();
            Setting = new EmailSettingEditModel();
            Setting.TrainerAddresses = list.Where(x => x.Type == SystemEmailType.Trainer).Select(x => x.EmailAddress).ToList();
            Setting.FromAddress = list.FirstOrDefault(x => x.Type == SystemEmailType.Sender)?.EmailAddress;
            Setting.From = list.FirstOrDefault(x => x.Type == SystemEmailType.Sender)?.DisplayName;
            Setting.ReplyToAddresses = list.Where(x => x.Type == SystemEmailType.ReplyTo).Select(x => x.EmailAddress).ToList();
            Setting.ReturnAddress = list.FirstOrDefault(x => x.Type == SystemEmailType.ReturnPath)?.EmailAddress;
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                var trainers = Setting.TrainerAddresses?.Select(x => GetSystemEmail(SystemEmailType.Trainer, x)).ToList();
                var fromAddress = new[] { GetSystemEmail(SystemEmailType.Sender, Setting.FromAddress, Setting.From) }.ToList();
                var replyTos = Setting.ReplyToAddresses?.Select(x => GetSystemEmail(SystemEmailType.ReplyTo, x)).ToList();
                var returnPath = new[] { GetSystemEmail(SystemEmailType.ReturnPath, Setting.ReturnAddress) }.ToList();

                var input = trainers.Concat(fromAddress).Concat(replyTos).Concat(returnPath).Where(x => x != null).ToList();
                var types = new[] { SystemEmailType.Trainer, SystemEmailType.Sender, SystemEmailType.ReplyTo, SystemEmailType.ReturnPath };
                var list = DB.SystemEmail.ToList();
                foreach (var type in types)
                    Merge(type, list, input);

                DB.SaveChanges(JobLogTitle.SaveEmailAddresses);
                return this.Ok(
                    message: "Save Success",
                    redirect: Url.Page($"/Setting/Email")
                );
            }
            return this.Fail(ModelState.FirstErrorMessage());
        }

        private void Merge(string type, List<SystemEmail> source, List<SystemEmail> target)
        {
            var list = source.Where(x => x.Type == type).ToList();
            var input = target.Where(x => x?.Type == type).ToList();
            
            if (list.Count < input.Count)
            {
                var items = input.Skip(list.Count).ToList();
                DB.SystemEmail.AddRange(items);
                list.AddRange(items);
            }
            else if (list.Count > input.Count)
                DB.SystemEmail.RemoveRange(list.Skip(input.Count).ToList());

            for (int i = 0; i < input.Count; i++)
            {
                var a = list[i];
                var b = input[i];
                b.Populate(a, "EmailAddress", "DisplayName");
            }
        }

        private static SystemEmail GetSystemEmail(string type, string address, string display = null)
        {
            var mailAddress = GetMailAddress(address);
            if (mailAddress != null)
                return new SystemEmail { Type = type, EmailAddress = mailAddress.Address, DisplayName = display };

            return null;
        }

        private static MailAddress GetMailAddress(string address)
        {
            try
            {
                if (address != null)
                    return new MailAddress(address);
            }
            catch (Exception)
            {
            }
            return null;
        }
    }
}