﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly RicohDB db;

        public IndexModel(ILogger<IndexModel> logger, RicohDB db)
        {
            _logger = logger;
            this.db = db;
        }

        public DashboardModel ThisMonth { get; set; }
        public DashboardModel NextMonth { get; set; }
        public DashboardModel LastMonth { get; set; }

        public void OnGet()
        {
            var today = DateTime.Today;
            var nextMonth = today.AddMonths(1);
            var lastMonth = today.AddMonths(-1);

            var timeslots = db.ScheduledSession
                .Where(x => EF.Functions.DateDiffMonth(today, x.Date) >= -1)
                .Where(x => EF.Functions.DateDiffMonth(today, x.Date) <= 1)
                .Where(x => x.IsCustom == x.IsUseCustom || x.RegistrationId > 0)
                .ToList();

            var installs = db.MachineInstallation
                .Include(x => x.MachineInstallationDate)
                .Where(x => EF.Functions.DateDiffMonth(today, x.LastChangeTime) >= -1)
                .Where(x => EF.Functions.DateDiffMonth(today, x.LastChangeTime) <= 0)
                .ToList();

            var invitationEmail = EmailTemplateId.REG_INVITE;
            var sent = (int)EmailRequestStatus.Sent;
            var emails = db.EmailRequest
                .Where(x => x.TemplateId == invitationEmail)
                .Where(x => EF.Functions.DateDiffMonth(today, x.SentTime) >= -1)
                .Where(x => EF.Functions.DateDiffMonth(today, x.SentTime) <= 0)
                .Where(x => x.Status == sent)
                .ToList();

            var regStatus = new[] { RegistrationStatus.Submitted, RegistrationStatus.Rescheduled };
            var registrations = db.RegistrationTimeslot
                .Where(x => EF.Functions.DateDiffMonth(today, x.SubmissionDate) >= -1)
                .Where(x => EF.Functions.DateDiffMonth(today, x.SubmissionDate) <= 1)
                .Where(x => regStatus.Contains(x.Status))
                .ToList();

            var settings = db.GetDefaultSettings();
            var entitled = settings.CompletedInstallationProblemCodes;
            var provided = settings.CompletedTrainingProblemCodes;

            var maps = new Dictionary<DateTime, DashboardModel>
            {
                { today, ThisMonth = new DashboardModel() },
                { lastMonth, LastMonth = new DashboardModel() },
                { nextMonth, NextMonth = new DashboardModel() }
            };
            foreach (var date in maps.Keys)
            {
                var model = maps[date];
                var year = date.Year;
                var month = date.Month;
                var isToday = date == today;
                model.Date = date;
                
                model.AvailableTimeslot = timeslots.Count(x => x.Date.Year == year && x.Date.Month == month && (!isToday || x.Date <= today));
                
                model.InstallationEntitled = installs.Where(x => x.LastChangeTime.Year == year && x.LastChangeTime.Month == month)
                    .Count(x => entitled.Contains(x.ProblemCode, StringComparer.OrdinalIgnoreCase) && x.MachineInstallationDate?.IsFreeTraining == true);
                
                model.SentInvitation = emails.Count(x => x.SentTime?.Year == year && x.SentTime?.Month == month);
                
                model.RegistrationPreferred = registrations.Where(x => x.SubmissionDate?.Year == year && x.SubmissionDate?.Month == month)
                    .Count(x => x.ScheduleTimeslot == null);
                
                model.RegistrationScheduled = registrations.Where(x => x.SubmissionDate?.Year == year && x.SubmissionDate?.Month == month)
                    .Count(x => x.ScheduleTimeslot != null);
                
                model.AvailableTimeslotLeft = timeslots.Where(x => x.Date.Year == year && x.Date.Month == month && (!isToday || x.Date <= today))
                    .Count(x => !x.RegistrationId.HasValue);
                
                model.NotYetRegistered = installs.Where(x => x.LastChangeTime.Year == year && x.LastChangeTime.Month == month)
                    .Count(x => !registrations.Any(r => r.SerialNo == x.SerialNo && r.ItemNo == x.ItemNo) && x.MachineInstallationDate?.IsFreeTraining == true);
                
                model.TrainingProvided = installs.Where(x => x.LastChangeTime.Year == year && x.LastChangeTime.Month == month)
                    .Count(x => provided.Contains(x.ProblemCode));
            }
        }
    }
}
