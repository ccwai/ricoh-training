﻿using DotNetLib.Email;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.DI
{
    static class EmailProviderFactory
    {
        public static IEmailClient GetInstance(IServiceProvider services)
        {
            var configuration = services.GetRequiredService<IConfiguration>();
            var section = configuration.GetSection("Email");
            var provider = section?.GetValue("Provider", "Default");

            IEmailClient instance = null;
            if ("default".Equals(provider, StringComparison.OrdinalIgnoreCase))
                instance = GetEmailClient(services, section);

            if (instance == null)
                throw new Exception("Cannot resolve IEmailClient. Please check the configurations.");

            return instance;
        }

        private static EmailClient GetEmailClient(IServiceProvider services, IConfiguration configuration)
        {
            var config = new EmailClientConfig();
            configuration.Bind(config);
            return new EmailClient(config);
        }
    }
}
