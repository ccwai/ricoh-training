﻿using DotNetLib.Storage;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.DI
{
    static class StorageProviderFactory
    {
        public static IStorageProvider GetInstance(IServiceProvider services)
        {
            var configuration = services.GetRequiredService<IConfiguration>();
            var section = configuration.GetSection("Storage");
            var provider = section?.GetValue("Provider", "Physical");

            IStorageProvider instance = null;
            if ("physical".Equals(provider, StringComparison.OrdinalIgnoreCase))
                instance = GetPhysicalStorageProvider(services, section);
            else if ("azure".Equals(provider, StringComparison.OrdinalIgnoreCase))
                instance = GetAzureStorageProvider(services, section);

            if (instance == null)
                throw new Exception("Cannot resolve IStorageProvider. Please check the configurations.");

            return instance;
        }

        private static PhysicalStorageProvider GetPhysicalStorageProvider(IServiceProvider services, IConfigurationSection config)
        {
            var container = config.GetValue<string>("Container");
            if (container != null)
            {
                string basePath = container;
                if (!Path.IsPathFullyQualified(basePath))
                {
                    if (!Path.IsPathRooted(basePath))
                    {
                        basePath = Regex.Replace(basePath, @"^~[\/\\]", "");
                        var env = services.GetRequiredService<IWebHostEnvironment>();
                        var contentPath = env.ContentRootPath;
                        basePath = Path.GetFullPath(basePath, contentPath);
                    }
                    else
                        basePath = Path.GetFullPath(basePath);

                }
                return new PhysicalStorageProvider(basePath);
            }
            return null;
        }

        private static AzureStorageProvider GetAzureStorageProvider(IServiceProvider services, IConfigurationSection config)
        {
            var logger = services.GetRequiredService<Microsoft.Extensions.Logging.ILogger<AzureStorageProvider>>();
            var connectionString = config.GetValue<string>("ConnectionString");
            var container = config.GetValue<string>("Container");
            return new AzureStorageProvider(logger, connectionString, container);
        }
    }
}
