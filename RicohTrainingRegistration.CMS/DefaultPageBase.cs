﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS
{
    public abstract class DefaultPageBase : PageBase
    {
        private ViewDataDictionary _ViewData => PageContext.ViewData;

        public IEnumerable<string> ActiveMenus(params string[] menus)
        {
            var list = menus?.AsEnumerable();
            _ViewData["ActiveMenus"] = list;
            return list;
        }

        public void PageTitle(string title) => _ViewData["PageTitle"] = title;
        
        public void PageHeading(string heading, string subheading = null)
        {
            _ViewData["PageHeading"] = heading;
            _ViewData["PageSubheading"] = subheading;
        }

        public void Breadcrumbs(params Breadcrumb[] breadcrumbs)
        {
            _ViewData["Breadcrumbs"] = breadcrumbs;
        }

        public void UseDatatable() => _ViewData["UseDatatable"] = true;
        
        public void UseJqueryValidate() => _ViewData["UseJqueryValidate"] = true;

        public void UseDatePicker() => _ViewData["UseDatePicker"] = true;

        public void UseTimePicker() => _ViewData["UseTimePicker"] = true;

        public void UseSelect2() => _ViewData["UseSelect2"] = true;
    }
}
