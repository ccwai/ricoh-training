﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS
{
    public static class MvcHelper
    {
        public static IHtmlContent BsValidationMessageFor<TModel, TResult>(this IHtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TResult>> expression)
        {
            return htmlHelper.ValidationMessageFor(expression, null, new { @class = "text-danger" });
        }

        public static IHtmlContent BsValidationSummary(this IHtmlHelper htmlHelper, bool excludePropertyErrors = false)
        {
            return htmlHelper.ValidationSummary(excludePropertyErrors, null, new { @class = "alert alert-danger" });
        }

        public static string FirstErrorMessage(this ModelStateDictionary modelState)
        {
            return modelState.Values.SelectMany(r => r.Errors).Select(r => r.ErrorMessage).FirstOrDefault(r => r != null);
        }

        public static IActionResult Ok(this PageModel page, bool success = true, string message = null, object data = null, string redirect = null, string redirectType = "replace", int? statusCode = null)
        {
            var result = new { success, message, data, redirect, redirectType };
            return new JsonResult(result);
        }

        public static IActionResult Fail(this PageModel page, string message, int statusCode = 400)
        {
            return page.Ok(false, message, statusCode: statusCode);
        }

        public static string Checked(this IHtmlHelper htmlHelper, bool? isChecked)
        {
            return isChecked == true ? "checked" : "";
        }

        public static string Selected(this IHtmlHelper htmlHelper, bool? isSelected)
        {
            return isSelected == true ? "selected" : "";
        }
    }
}
