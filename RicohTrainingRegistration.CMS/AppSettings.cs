﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS
{
    public class AppSettings
    {
        public AppSettings(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IConfigurationSection Settings => Configuration.GetSection("AppSettings");

        public DayOfWeek[] DayOfWeeks => Settings?.GetValue("DayOfWeeks", new DayOfWeek[] { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday });

        public string[] Districts => Settings?.GetValue("Districts", new string[] { District.Hk, District.Kln, District.Nt });

        public string DailyJobTime => Settings?.GetValue("DailyJobTime", "04:00");
        
        public int MaxFooterLinks => Settings?.GetValue("MaxFooterLinks", 5) ?? 5;

        public int SendEmailRetry => Settings?.GetValue("SendEmailRetry", 3) ?? 3;
        
        public int SendEmailRetryInterval => Settings?.GetValue("SendEmailRetryInterval", 180) ?? 180;

        public int DailyJobRetry => Settings?.GetValue("DailyJobRetry", 5) ?? 5;

        public int DailyJobRetryInterval => Settings?.GetValue("DailyJobRetryInterval", 600) ?? 600;

        public string RegistrationEmailSendTime => Settings?.GetValue("RegistrationEmailSendTime", "");

        public string RegistrationUrl => Settings?.GetValue("RegistrationUrl", "");
        
        public string AdminEmailAddress => Settings?.GetValue("AdminEmailAddress", "");


        public string ExpireReminderEmailTime => "09:00";
        public int[] ExpireReminderEmailDays => new[] { 83, 30, 10 };

        public string TrainingReminderEmailTime => "09:00";
        public int[] TrainingReminderEmailDays => new[] { 1 };
    }
}
