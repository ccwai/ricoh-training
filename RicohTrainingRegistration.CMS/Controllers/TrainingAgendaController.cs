﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class TrainingAgendaController : ControllerBase
    {
        private readonly RicohDB db;

        public TrainingAgendaController(RicohDB db)
        {
            this.db = db;
        }

        [Route("training/agenda/list")]
        public IActionResult List(DtRequest model)
        {
            var query = db.TrainingAgenda.Include(x => x.TrainingModel).AsQueryable();
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(r => r.Title.Contains(search)
                    || r.ContentEng.Contains(search)
                    || r.ContentChi.Contains(search)
                    || r.TrainingModel.Any(m => m.ModelId.Contains(search) || m.ModelName.Contains(search))
                );
            }

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .GetResult(recordsTotal);

            return Ok(
                result.As(x => new
                {
                    x.Id,
                    x.Title,
                    x.ContentEng,
                    x.ContentChi,
                    x.UrlLabelEng,
                    x.UrlEng,
                    x.UrlLabelChi,
                    x.UrlChi,
                    Models = x.TrainingModel.Select(m => new { Id = m.ModelId, Name = m.ModelName }).OrderBy(m => m.Id),
                    x.UpdatedDate,
                    Actions = DtRowAction.Build(
                        DtRowAction.Edit(Url.Page("/Training/Agenda/Edit", new { id = x.Id })),
                        DtRowAction.Delete(Url.Action("Delete", "TrainingAgenda", new { id = x.Id }))
                    )
                })
            );
        }

        [HttpDelete]
        [Route("training/model/{modelId}")]
        public IActionResult Delete(string modelId)
        {
            var model = db.TrainingModel.Find(modelId);
            if (model != null)
            {
                db.TrainingModel.Remove(model);
                db.SaveChanges();
                return Ok(new { success = true, message = "Item deleted." });
            }
            return BadRequest(new { message = "Item not found" });
        }
    }
}