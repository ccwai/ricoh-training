﻿using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class RegistrationController : ControllerBase
    {
        private readonly RicohDB db;

        public RegistrationController(RicohDB db)
        {
            this.db = db;
        }

        [Route("registration/list")]
        public IActionResult List(DtRequest<RegistrationFilter> model)
        {
            var query = db.RegistrationTimeslot.AsQueryable();
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(x =>
                    x.SerialNo.Contains(search) ||
                    x.OrderId.Contains(search) ||
                    x.CompanyName.Contains(search) || x.VenueAddress.Contains(search) ||
                    x.FirstName.Contains(search) || x.LastName.Contains(search) ||
                    x.PhoneNo.Contains(search) || x.Email.Contains(search) ||
                    x.ModelId.Contains(search) || x.ModelName.Contains(search)
                );
            }

            query = ApplyFilter(query, model.Params);

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .GetResult(recordsTotal);

            var districtList = db.GetDistrictList();
            var attendeeList = db.GetAttendeeList();
            var osList = db.GetOperatingSystemList();

            return Ok(
                result.As(x => new
                {
                    x.SerialNo,
                    x.OrderId,
                    x.VenueAddress,
                    District = districtList.FirstOrDefault(d => d.Id.EqualsIgnoreCase(x.District)),
                    x.TrainingDate,
                    x.TrainingTimeslot,
                    x.TrainingDatePreferable,
                    x.TrainingDateRemarks,
                    Attendees = attendeeList.FirstOrDefault(a => a.Id == x.Attendees),
                    OperatingSystem = osList.FirstOrDefault(s => s.Id == x.OperatingSystem),
                    x.CompanyName,
                    x.FirstName,
                    x.LastName,
                    x.PhoneNo,
                    x.Email,
                    x.Remarks,
                    x.Status,
                    x.SubmissionDate,
                    x.ModelId,
                    x.ModelName,
                    x.IsFreeTraining,
                    ScheduleDistrict = x.ScheduleDistrict?.ToUpperInvariant(),
                    x.ScheduleDate,
                    x.ScheduleTimeslot,
                    Actions = DtRowAction.Build(
                        DtRowAction.Edit(Url.Page("/Registration/Edit", new { id = x.Id })),
                        DtRowAction.Delete(Url.Action("Delete", new { id = x.Id }))
                    )
                })
            );
        }

        [Route("registration/export")]
        public IActionResult Export(RegistrationFilter model)
        {
            if (model != null)
                model.Status = Request.Query["Status[]"].ToArray();

            var query = db.RegistrationTimeslot.AsQueryable();
            query = ApplyFilter(query, model);

            var districtList = db.GetDistrictList();
            var attendeeList = db.GetAttendeeList();
            var osList = db.GetOperatingSystemList();

            var records = query.OrderByDescending(x => x.SubmissionDate).ToList()
                .Select(x => new
                {
                    x.SerialNo,
                    x.OrderId,
                    x.VenueAddress,
                    District = districtList.FirstOrDefault(d => d.Id.EqualsIgnoreCase(x.ScheduleDistrict))?.Label,
                    TrainingDate = x.ScheduleDate,
                    TrainingTimeSlot = x.ScheduleTimeslot,
                    x.TrainingDatePreferable,
                    x.TrainingDateRemarks,
                    Attendees = attendeeList.FirstOrDefault(a => a.Id == x.Attendees)?.Label,
                    OperatingSystem = osList.FirstOrDefault(s => s.Id == x.OperatingSystem)?.Label,
                    x.CompanyName,
                    x.FirstName,
                    x.LastName,
                    x.PhoneNo,
                    x.Email,
                    x.Remarks,
                    x.Status,
                    x.SubmissionDate,
                    x.ModelId,
                    x.ModelName,
                    x.IsFreeTraining
                })
                .ToList();

            string text;
            using (var sw = new System.IO.StringWriter())
            {
                using (var csv = new CsvHelper.CsvWriter(sw, true))
                {
                    csv.WriteRecords(records);
                    text = sw.ToString();
                }
            }

            var content = System.Text.Encoding.UTF8.GetBytes(text);
            var fileName = string.Format("enquiry-export-{0:yyyyMMddHHmmss}.csv", DateTime.Now);
            return File(content, "text/csv", fileName);
        }

        [HttpDelete]
        [Route("registration/{id}")]
        public IActionResult Delete(int id)
        {
            var model = db.Registration.Find(id);
            if (model != null)
            {
                db.Registration.Remove(model);
                db.SaveChanges();
                return Ok(new { success = true, message = "Item deleted." });
            }
            return BadRequest(new { message = "Item not found" });
        }

        [Route("registration/lookup")]
        public IActionResult GetInstallation(string input, string type)
        {
            MachineInstallation install;
            if ("order".EqualsIgnoreCase(type))
                install = db.MachineInstallation.Find(input);
            else
                install = db.MachineInstallation.LatestOrDefault(input);

            return Ok(new
            {
                success = true,
                data = install?.Populate(new MachineInstallation(), "OrderId", "SerialNo", "ModelId", "ModelDesc", "CompanyName", "InstallAddress", "InstallDistrict", "FirstName", "FirstName", "LastName", "PhoneNo", "Email")
            });
        }

        private IQueryable<RegistrationTimeslot> ApplyFilter(IQueryable<RegistrationTimeslot> query, RegistrationFilter filter)
        {
            var p = filter ?? new RegistrationFilter();
            if (p.DateFrom.HasValue)
            {
                var dateFrom = p.DateFrom;
                query = query.Where(x => x.ScheduleDate >= dateFrom);
            }
            if (p.DateTo.HasValue)
            {
                var dateTo = p.DateTo;
                query = query.Where(x => x.ScheduleDate <= dateTo);
            }
            if (!string.IsNullOrWhiteSpace(p.VenueAddress))
            {
                var address = p.VenueAddress;
                query = query.Where(x => x.VenueAddress.Contains(address));
            }
            if (!string.IsNullOrWhiteSpace(p.District))
            {
                var district = p.District;
                query = query.Where(x => x.ScheduleDistrict == district);
            }
            if (!string.IsNullOrWhiteSpace(p.SerialNo))
            {
                var sn = p.SerialNo.Trim();
                query = query.Where(x => x.SerialNo.Contains(sn));
            }
            if (!string.IsNullOrWhiteSpace(p.ModelNo))
            {
                var modelNo = p.ModelNo.Trim();
                query = query.Where(x => x.ModelId.Contains(modelNo));
            }
            if (!string.IsNullOrEmpty(p.Attendees))
            {
                var attendees = p.Attendees;
                query = query.Where(x => x.Attendees == attendees);
            }
            if (!string.IsNullOrEmpty(p.OperatingSystem))
            {
                var os = p.OperatingSystem;
                query = query.Where(x => x.OperatingSystem == os);
            }
            if (!string.IsNullOrWhiteSpace(p.Company))
            {
                var company = p.Company.Trim();
                query = query.Where(x => x.CompanyName.Contains(company));
            }
            if (!string.IsNullOrWhiteSpace(p.Name))
            {
                var name = p.Name.Replace(" ", "");
                query = query.Where(x => x.FirstName.Contains(name) || x.LastName.Contains(name) || (x.FirstName + x.LastName).Contains(name) || (x.LastName + x.FirstName).Contains(name));
            }
            if (!string.IsNullOrWhiteSpace(p.PhoneNo))
            {
                var phoneNo = p.PhoneNo.Trim();
                query = query.Where(x => x.PhoneNo.Contains(phoneNo));
            }
            if (!string.IsNullOrWhiteSpace(p.Email))
            {
                var email = p.Email.Trim();
                query = query.Where(x => x.Email.Contains(email));
            }
            if (p.Status?.Any() == true)
            {
                var status = p.Status;
                query = query.Where(x => status.Contains(x.Status));
            }
            return query;
        }
    }
}
