﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Datatable;
using DotNetLib.Storage;
using Microsoft.AspNetCore.Mvc;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class MediaController : ControllerBase
    {
        private readonly RicohDB db;
        private readonly IStorageProvider storage;

        public MediaController(RicohDB db, IStorageProvider storage)
        {
            this.db = db;
            this.storage = storage;
        }

        [Route("media/{id}")]
        public IActionResult Get(int id, [FromQuery]int dl = 0)
        {
            var media = db.Media.Find(id);
            if (media != null)
            {
                var stream = new MemoryStream();
                if (storage.Read(media.Path, stream))
                {
                    if (dl == 1)
                        return File(stream, media.ContentType, media.FileName);
                    return File(stream, media.ContentType);
                }
            }
            return new StatusCodeResult(404);
        }

        [IgnoreAntiforgeryToken]
        [HttpPost]
        [Route("media/remove")]
        public IActionResult Remove()
        {
            return Ok(new { });
        }
    }
}