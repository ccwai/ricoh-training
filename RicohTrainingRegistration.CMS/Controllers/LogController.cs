﻿using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class LogController : ControllerBase
    {
        private readonly RicohDB db;

        public LogController(RicohDB db)
        {
            this.db = db;
        }

        [Route("log/list")]
        public IActionResult List(DtRequest model)
        {
            var query = db.JobLog.AsQueryable();
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(x =>
                    x.Title.Contains(search) ||
                    x.Message.Contains(search) ||
                    x.Result.Contains(search)
                );
            }

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .GetResult(recordsTotal);

            return Ok(
                result
            );
        }
    }
}
