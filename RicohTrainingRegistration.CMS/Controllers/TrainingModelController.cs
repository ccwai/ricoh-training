﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class TrainingModelController : ControllerBase
    {
        private readonly RicohDB db;

        public TrainingModelController(RicohDB db)
        {
            this.db = db;
        }

        [Route("training/model/list")]
        public IActionResult List(DtRequest<TrainingModelFilter> model)
        {
            var query = db.TrainingModel.Include(x => x.TrainingAgenda).AsQueryable();
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(x => x.ModelId.Contains(search) || x.ModelName.Contains(search));
            }

            if (model.Params?.FreeTraining.Any() == true)
            {
                var display = model.Params.FreeTraining.Select(x => "Entitled".EqualsIgnoreCase(x)).ToArray();
                query = query.Where(x => display.Contains(x.IsFreeTraining));
            }

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .Order("id", x => x.ModelId)
                .Order("name", x => x.ModelName)
                .Order("agenda", x => x.TrainingAgenda.Title)
                .Order("isFree", x => !x.IsFreeTraining)
                .GetResult(recordsTotal);

            return Ok(
                result.As(x => new
                {
                    id = x.ModelId,
                    name = x.ModelName,
                    isFree = x.IsFreeTraining,
                    openedDays = x.OpenedDays,
                    agenda = x.TrainingAgenda != null ? new { title = x.TrainingAgenda.Title, url = Url.Page("/Training/Agenda/Edit", new { id = x.TrainingAgenda.Id }) } : null,
                    retrievedDate = x.RetrievedDate,
                    updatedDate = x.UpdatedDate,
                    editUrl = Url.Action("Edit", new { modelId = x.ModelId })
                })
            );
        }

        [HttpPost]
        [Route("training/model")]
        public IActionResult Edit(string modelId, TrainingModelEditModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = db.TrainingModel.Find(modelId);
                if (entity != null)
                {
                    entity.IsFreeTraining = model.IsFree == true;
                    entity.OpenedDays = model.OpenedDays.Value;
                    db.SaveChanges(JobLogTitle.SaveTrainingModel);
                    return Ok(new { success = true, message = "Item saved." });
                }
                return BadRequest(new { message = "Item not found" });
            }
            return BadRequest(new { message = ModelState.FirstErrorMessage() });
        }

        [HttpDelete]
        [Route("training/model/{modelId}")]
        public IActionResult Delete(string modelId)
        {
            var model = db.TrainingModel.Find(modelId);
            if (model != null)
            {
                db.TrainingModel.Remove(model);
                db.SaveChanges(JobLogTitle.DeleteTrainingModel);
                return Ok(new { success = true, message = "Item deleted." });
            }
            return BadRequest(new { message = "Item not found" });
        }
    }
}