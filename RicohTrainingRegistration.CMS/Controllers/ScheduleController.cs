﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.Database;

namespace RicohTrainingRegistration.CMS.Controllers
{
    public class ScheduleController : ControllerBase
    {
        private readonly RicohDB db;

        public ScheduleController(RicohDB db)
        {
            this.db = db;
        }

        [Route("training/schedule/default/query")]
        public IActionResult DefaultQuery(DateTime? date, string district)
        {
            return null;
        }

        [Route("training/schedule/custom/list")]
        public IActionResult CustomList(DtRequest model)
        {
            var query = db.ScheduleItem
                .Include(x => x.ScheduleMaster)
                .Include(x => x.ScheduleItemSlot)
                .Where(x => x.IsUseCustom);
            var recordsTotal = query.Count();

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .GetResult(recordsTotal);

            var resp = result.As(x => new
            {
                x.Id,
                x.Date,
                x.District,
                Timeslots = x.ScheduleItemSlot.Customs().GroupBy(s => s.Timeslot).Select(s => new
                {
                    Timeslot = string.Format(@"{0:hh\:mm}", s.Key),
                    Count = s.Count()
                }).OrderBy(s => s.Timeslot),
                x.IsUseCustom,
                x.Remark,
                Actions = DtRowAction.Build(
                    DtRowAction.Edit(Url.Page("/Training/Schedule/Custom/Edit", new { id = x.Id })),
                    new DtRowAction
                    {
                        Label = "Undo",
                        Title = "Undo",
                        Icon = "fa fa-fw fa-refresh",
                        Url = Url.Action("Undo", "Schedule", new { id = x.Id }),
                        Attributes = new Dictionary<string, object>
                        {
                            { "class", "text-danger" },
                            { "data-dt-action", "delete" },
                            { "data-dt-action-message", "Confirm to restore timeslots to default?" },
                            { "data-dt-action-url", Url.Action("Undo", "Schedule", new { id = x.Id }) }
                        }
                    }
                )
            });
            return Ok(resp);
        }

        [Route("api/training/timeslot")]
        public IActionResult TrainingTimeslot([FromQuery]string district, [FromQuery]string date)
        {
            if (!new[] { "hk", "kln", "nt" }.Contains(district?.ToLower())
                || !DateTime.TryParseExact(date, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime trainingDate))
                return new JsonResult(Enumerable.Empty<object>());

            var slots = db.ScheduleItemSlot
                .Where(x => x.ScheduleItem.District == district && x.ScheduleItem.Date == trainingDate)
                .Where(x => x.IsCustom == x.ScheduleItem.IsUseCustom)
                .ToList();
            var data = slots.GroupBy(x => x.Timeslot).Select(x => new
            {
                id = x.Key,
                text = x.Key,
                isEnabled = x.Any(s => s.RegistrationId == null)
            }).OrderBy(x => x.text).ToList();

            return new JsonResult(data);
        }
    }
}