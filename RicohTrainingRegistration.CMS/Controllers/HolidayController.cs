﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using RicohTrainingRegistration.Database;
using RicohTrainingRegistration.Integration;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class HolidayController : ControllerBase
    {
        private readonly RicohDB db;
        private readonly HolidayApi api;

        public HolidayController(RicohDB db, HolidayApi api)
        {
            this.db = db;
            this.api = api;
        }

        [HttpPost]
        [Route("training/holiday/import/hk-holiday")]
        public async Task<IActionResult> ImportHkHoliday()
        {
            try
            {
                var hkHolidays = await api.GetHkHolidays();

                if (hkHolidays.Any())
                {
                    var type = HolidayType.HkHoliday;
                    db.Holiday.RemoveRange(db.Holiday.Where(x => x.Type == type).ToList());

                    var list = hkHolidays.Select(x => new Holiday
                    {
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Name = x.Name,
                        Type = type
                    });
                    db.Holiday.AddRange(list);
                    db.SaveChanges();
                }
                return Ok(new
                {
                    message = "Import success",
                    redirect = "",
                    redirectType = "replace"
                });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { error = ex.Message });
            }
        }

        [Route("training/holiday/list")]
        public IActionResult List(DtRequest model)
        {
            var query = db.Holiday.AsQueryable();
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(r => r.Name.Contains(search));
            }

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .Order("date", x => x.StartDate)
                .Order("lastUpdated", x => x.UpdatedDate ?? x.CreatedDate)
                .GetResult(recordsTotal);

            return Ok(
                result.As(x => new
                {
                    x.Id,
                    x.Name,
                    x.StartDate,
                    x.EndDate,
                    x.Type,
                    x.LastUpdated,
                    Actions = DtRowAction.Build(
                        DtRowAction.Edit(Url.Page("/Training/Holiday/Edit", new { id = x.Id })),
                        DtRowAction.Delete(Url.Action("Delete", "Holiday", new { id = x.Id }))
                    )
                })
            );
        }

        [HttpDelete]
        [Route("training/holiday/{id}")]
        public IActionResult Delete(int id)
        {
            var model = db.Holiday.Find(id);
            if (model != null)
            {
                db.Holiday.Remove(model);
                db.SaveChanges();
                return Ok(new { success = true, message = "Item deleted." });
            }
            return BadRequest(new { message = "Item not found" });
        }
    }
}