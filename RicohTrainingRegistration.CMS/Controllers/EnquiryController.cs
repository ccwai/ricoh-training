﻿using DotNetLib;
using DotNetLib.Datatable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RicohTrainingRegistration.CMS.Models;
using RicohTrainingRegistration.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RicohTrainingRegistration.CMS.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class EnquiryController : ControllerBase
    {
        private readonly RicohDB db;

        public EnquiryController(RicohDB db)
        {
            this.db = db;
        }

        [Route("enquiry/list")]
        public IActionResult List(DtRequest<EnquiryFormFilter> model)
        {
            var query = db.EnquiryForm.Include(x => x.MachineInstallation).ThenInclude(x => x.TrainingModel).Where(x => x.MachineInstallation != null);
            var recordsTotal = query.Count();

            if (model.SearchValue.IsNotNullAndWhiteSpace())
            {
                var search = model.SearchValue;
                query = query.Where(x =>
                    x.SerialNo.Contains(search) ||
                    x.CompanyName.Contains(search) ||
                    x.FirstName.Contains(search) || x.LastName.Contains(search) ||
                    x.PhoneNo.Contains(search) || x.Email.Contains(search) ||
                    x.MachineInstallation.ModelId.Contains(search) ||
                    x.MachineInstallation.ModelDesc.Contains(search) || x.MachineInstallation.TrainingModel.ModelName.Contains(search)
                );
            }

            query = ApplyFilter(query, model.Params);

            var result = model.GetOrderable(query)
                .OrderDefaults()
                .Order("modelId", x => x.MachineInstallation.ModelId)
                .Order("modelName", x => x.MachineInstallation.TrainingModel.ModelName)
                .GetResult(recordsTotal);

            return Ok(
                result.As(x => new
                {
                    x.Id,
                    x.OrderId,
                    x.SerialNo,
                    x.CompanyName,
                    x.FirstName,
                    x.LastName,
                    x.PhoneNo,
                    x.Email,
                    x.Remarks,
                    x.RequestTime,
                    x.MachineInstallation.ModelId,
                    x.MachineInstallation.TrainingModel.ModelName,
                    x.IsRead,
                    Actions = DtRowAction.Build(
                        DtRowAction.Edit(Url.Page("/Enquiry/Review", new { id = x.Id }))
                    )
                })
            );
        }

        [Route("enquiry/export")]
        public IActionResult Export(EnquiryFormFilter model)
        {
            if (model != null)
                model.IsRead = Request.Query["IsRead[]"].ToArray();

            var query = db.EnquiryForm.Include(x => x.MachineInstallation).ThenInclude(x => x.TrainingModel).AsQueryable();
            query = ApplyFilter(query, model);

            var records = query.OrderByDescending(x => x.RequestTime).ToList()
                .Select(x => new
                {
                    RequestTime = x.RequestTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    x.OrderId,
                    x.SerialNo,
                    x.CompanyName,
                    x.FirstName,
                    x.LastName,
                    x.PhoneNo,
                    x.Email,
                    x.Remarks,
                    x.MachineInstallation.ModelId,
                    x.MachineInstallation.TrainingModel.ModelName,
                    IsRead = x.IsRead ? 1 : 0,
                })
                .ToList();

            string text;
            using (var sw = new System.IO.StringWriter())
            {
                using (var csv = new CsvHelper.CsvWriter(sw, true))
                {
                    csv.WriteRecords(records);
                    text = sw.ToString();
                }
            }

            var content = System.Text.Encoding.UTF8.GetBytes(text);
            var fileName = string.Format("enquiry-export-{0:yyyyMMddHHmmss}.csv", DateTime.Now);
            return File(content, "text/csv", fileName);
        }

        private IQueryable<EnquiryForm> ApplyFilter(IQueryable<EnquiryForm> query, EnquiryFormFilter filter)
        {
            var p = filter ?? new EnquiryFormFilter();
            if (p.DateFrom.HasValue)
            {
                var dateFrom = p.DateFrom;
                query = query.Where(x => x.RequestTime >= dateFrom);
            }
            if (p.DateTo.HasValue)
            {
                var dateTo = p.DateTo;
                query = query.Where(x => x.RequestTime <= dateTo);
            }
            if (!string.IsNullOrWhiteSpace(p.SerialNo))
            {
                var sn = p.SerialNo.Trim();
                query = query.Where(x => x.SerialNo.Contains(sn));
            }
            if (!string.IsNullOrWhiteSpace(p.ModelNo))
            {
                var modelNo = p.ModelNo.Trim();
                query = query.Where(x => x.MachineInstallation.ModelId.Contains(modelNo));
            }
            if (!string.IsNullOrWhiteSpace(p.Company))
            {
                var company = p.Company.Trim();
                query = query.Where(x => x.CompanyName.Contains(company));
            }
            if (!string.IsNullOrWhiteSpace(p.Name))
            {
                var name = p.Name.Replace(" ", "");
                query = query.Where(x => x.FirstName.Contains(name) || x.LastName.Contains(name) || (x.FirstName + x.LastName).Contains(name) || (x.LastName + x.FirstName).Contains(name));
            }
            if (!string.IsNullOrWhiteSpace(p.PhoneNo))
            {
                var phoneNo = p.PhoneNo.Trim();
                query = query.Where(x => x.PhoneNo.Contains(phoneNo));
            }
            if (!string.IsNullOrWhiteSpace(p.Email))
            {
                var email = p.Email.Trim();
                query = query.Where(x => x.Email.Contains(email));
            }
            if (p.IsRead?.Any() == true)
            {
                var display = p.IsRead.Select(x => "Read".EqualsIgnoreCase(x)).ToArray();
                query = query.Where(x => display.Contains(x.IsRead));
            }
            return query;
        }
    }
}