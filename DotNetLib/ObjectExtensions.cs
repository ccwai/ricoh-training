﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DotNetLib
{
    public static class ObjectExtensions
    {
        public static TResult Populate<TSource, TResult>(this TSource source, params string[] propertyNames)
            where TSource : class
            where TResult : class
        {
            return source.Populate(default(TResult), propertyNames);
        }

        public static TResult Populate<TSource, TResult>(this TSource source, TResult target, params string[] propertyNames)
            where TSource : class
            where TResult : class
        {
            return (TResult)Populate(source, typeof(TSource), target, typeof(TResult), false, propertyNames, null);
        }

        public static TResult PopulateExcept<TSource, TResult>(this TSource source, params string[] excludedPropertyNames)
            where TSource : class
            where TResult : class
        {
            return source.PopulateExcept(default(TResult), excludedPropertyNames);
        }

        public static TResult PopulateExcept<TSource, TResult>(this TSource source, TResult target, params string[] excludedPropertyNames)
            where TSource : class
            where TResult : class
        {
            return (TResult)Populate(source, typeof(TSource), target, typeof(TResult), false, null, excludedPropertyNames);
        }

        public static TResult PopulateComplex<TSource, TResult>(this TSource source, params string[] propertyNames)
            where TSource : class
            where TResult : class
        {
            return source.PopulateComplex(default(TResult), propertyNames);
        }

        public static TResult PopulateComplex<TSource, TResult>(this TSource source, TResult target, params string[] propertyNames)
            where TSource : class
            where TResult : class
        {
            return (TResult)Populate(source, typeof(TSource), target, typeof(TResult), true, propertyNames, null);
        }

        public static TResult PopulateComplexExcept<TSource, TResult>(this TSource source, params string[] excludedPropertyNames)
            where TSource : class
            where TResult : class
        {
            return source.PopulateComplexExcept(default(TResult), excludedPropertyNames);
        }

        public static TResult PopulateComplexExcept<TSource, TResult>(this TSource source, TResult target, params string[] excludedPropertyNames)
            where TSource : class
            where TResult : class
        {
            return (TResult)Populate(source, typeof(TSource), target, typeof(TResult), true, null, excludedPropertyNames);
        }

        private static object Populate(object source, Type sourceType, object target, Type targetType, bool complex, string[] propertyNames, string[] excludedPropertyNames, Dictionary<object, object> visited = null)
        {
            if (source == null)
                return target;

            if (sourceType.IsValueType() && targetType.IsValueType())
                return GetValueTypeValue(source, sourceType, targetType);

            if (sourceType.IsIEnumerable())
                throw new NotSupportedException("Operation does not support IEnumerable source type");

            visited = visited ?? new Dictionary<object, object>();
            if (visited.ContainsKey(source))
                return visited[source];

            target = target ?? Activator.CreateInstance(targetType);
            visited.Add(source, target);

            bool isInclude = propertyNames?.Any() == true;
            bool isExclude = excludedPropertyNames?.Any() == true;

            var sourceProps = sourceType.GetProperties()
                .Where(x => x.CanPopulate())
                .Where(x => (!isInclude || propertyNames.Contains(x.Name)) && (!isExclude || !excludedPropertyNames.Contains(x.Name)))
                .Where(x => complex || x.PropertyType.IsValueType())
                .ToList();

            var targetProps = targetType.GetProperties()
                .Where(x => x.CanBePopulated())
                .Where(x => sourceProps.Any(s => s.Name == x.Name))
                .ToList();

            targetProps.ForEach(p =>
            {
                var s = sourceProps.Find(x => x.Name == p.Name);
                var value = s.GetValue(source);

                if (value == null)
                {
                    p.SetValue(target, null);
                    return;
                }

                var st = s.PropertyType;
                var tt = p.PropertyType;

                if (st.IsValueType() && tt.IsValueType())
                {
                    var val = GetValueTypeValue(value, st, tt);
                    p.SetValue(target, val);
                    return;
                }

                if (st.IsIEnumerable() && tt.IsIEnumerable())
                {
                    var t1 = st.GetGenericArguments()[0];
                    var t2 = tt.GetGenericArguments()[0];
                    var listType = typeof(List<>).MakeGenericType(t2);
                    var list1 = (IEnumerable)value;
                    var list2 = Activator.CreateInstance(listType);

                    foreach (var item in list1)
                    {
                        var val = Populate(item, t1, null, t2, complex, null, null, visited);
                        listType.GetMethod("Add").Invoke(list2, new[] { val });
                    }
                    p.SetValue(target, list2);
                    return;
                }

                if (!st.IsValueType() && !tt.IsValueType())
                {
                    var val = Populate(value, st, null, tt, complex, null, null, visited);
                    p.SetValue(target, val);
                }
            });

            return target;
        }

        private static bool IsValueType(this Type type)
        {
            if (type == typeof(string)) return true;
            return type.IsValueType;
        }

        private static object GetValueTypeValue(object value, Type sourceType, Type targetType)
        {
            TryGetValueTypeValue(value, sourceType, targetType, out object output);
            return output;
        }

        private static bool TryGetValueTypeValue(object value, Type sourceType, Type targetType, out object output)
        {
            if (value != null)
            {
                var val = value;
                var st = Nullable.GetUnderlyingType(sourceType) ?? sourceType;
                var tt = Nullable.GetUnderlyingType(targetType) ?? targetType;
                if (st != tt)
                {
                    try { val = Convert.ChangeType(value, targetType); }
                    catch
                    {
                        output = Activator.CreateInstance(targetType);
                        return false;
                    }
                }
                output = val;
                return true;
            }
            output = value;
            return true;
        }

        private static bool IsIEnumerable(this Type type)
        {
            return typeof(IEnumerable).IsAssignableFrom(type);
        }

        private static bool CanPopulate(this PropertyInfo propertyInfo)
        {
            return propertyInfo.CanRead && propertyInfo.PropertyType.IsPublic;
        }

        private static bool CanBePopulated(this PropertyInfo propertyInfo)
        {
            return propertyInfo.CanWrite && propertyInfo.PropertyType.IsPublic;
        }
    }
}
