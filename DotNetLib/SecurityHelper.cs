﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DotNetLib
{
    public static class SecurityHelper
    {
        public static string Md5(this string input)
        {
            using (var md5 = MD5.Create())
            {
                var bytes = Encoding.UTF8.GetBytes(input);
                var values = md5.ComputeHash(bytes);
                return string.Join("", values.Select(b => b.ToString("x2")));
            }
        }

        private static readonly byte[] DefaultSalt = Encoding.ASCII.GetBytes("ThisIsSalt");
        private static readonly int DefaultSaltSize = DefaultSalt.Length;

        public static string Encrypt(string passphrase, object input)
        {
            return Encrypt(passphrase, input != null ? Convert.ToString(input) : null);
        }

        public static string Encrypt(string passphrase, string plainText)
        {
            if (plainText == null)
                return null;

            using (RijndaelManaged aes = new RijndaelManaged())
            {
                aes.KeySize = 256;
                aes.BlockSize = 128;

                byte[] salt;
                using (var key = new Rfc2898DeriveBytes(passphrase, DefaultSaltSize, 1000))
                {
                    salt = key.Salt;

                    key.Salt = DefaultSalt;
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);
                }
                aes.Mode = CipherMode.CBC;

                byte[] input, output;
                using (MemoryStream ms = new MemoryStream())
                {
                    input = salt.Concat(Encoding.UTF8.GetBytes(plainText)).ToArray();
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(input, 0, input.Length);
                        cs.Close();
                    }
                    output = ms.ToArray();
                }
                return Convert.ToBase64String(output);
            }
        }

        public static string Decrypt(string passphrase, string cipherText)
        {
            if (cipherText == null)
                return null;

            using (RijndaelManaged aes = new RijndaelManaged())
            {
                aes.KeySize = 256;
                aes.BlockSize = 128;

                using (var key = new Rfc2898DeriveBytes(passphrase, DefaultSalt, 1000))
                {
                    aes.Key = key.GetBytes(aes.KeySize / 8);
                    aes.IV = key.GetBytes(aes.BlockSize / 8);
                }
                aes.Mode = CipherMode.CBC;

                byte[] input, output;
                using (MemoryStream ms = new MemoryStream())
                {
                    input = Convert.FromBase64String(cipherText);
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(input, 0, input.Length);
                    }
                    output = ms.ToArray().Skip(DefaultSaltSize).ToArray();
                }
                return Encoding.UTF8.GetString(output);
            }
        }
    }
}
