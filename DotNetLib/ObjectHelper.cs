﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLib
{
    public static class ObjectHelper
    {
        public static bool TryGetPropertyValue<T>(object obj, string propertyName, out T value) where T : struct
        {
            var pi = obj.GetType().GetProperty(propertyName);
            if (pi != null && (pi.PropertyType == typeof(T) || pi.PropertyType == typeof(Nullable<T>)))
            {
                var val = pi.GetValue(obj);
                if (val != null)
                {
                    value = (T)val;
                    return true;
                }
            }
            value = default(T);
            return false;
        }

        public static bool TrySetPropertyValue<T>(object obj, string propertyName, T value) where T : struct
        {
            var pi = obj.GetType().GetProperty(propertyName);
            if (pi != null && (pi.PropertyType == typeof(T) || pi.PropertyType == typeof(Nullable<T>)))
            {
                pi.SetValue(obj, value);
                return true;
            }
            return false;
        }

        public static bool TryGetPropertyValue(object obj, string propertyName, out string value)
        {
            var pi = obj.GetType().GetProperty(propertyName);
            if (pi != null && (pi.PropertyType == typeof(string)))
            {
                var val = pi.GetValue(obj);
                if (val != null)
                {
                    value = (string)val;
                    return true;
                }
            }
            value = default(string);
            return false;
        }

        public static bool TrySetPropertyValue(object obj, string propertyName, string value)
        {
            var pi = obj.GetType().GetProperty(propertyName);
            if (pi != null && (pi.PropertyType == typeof(string)))
            {
                pi.SetValue(obj, value);
                return true;
            }
            return false;
        }
    }
}
